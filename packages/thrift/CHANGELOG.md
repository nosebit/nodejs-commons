# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.9](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.1.8...@nosebit/thrift@1.1.9) (2022-04-26)

**Note:** Version bump only for package @nosebit/thrift





## [1.1.8](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.1.7...@nosebit/thrift@1.1.8) (2022-02-09)

**Note:** Version bump only for package @nosebit/thrift





## [1.1.7](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.1.6...@nosebit/thrift@1.1.7) (2022-01-07)

**Note:** Version bump only for package @nosebit/thrift





## [1.1.6](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.1.5...@nosebit/thrift@1.1.6) (2021-11-14)


### Bug Fixes

* **thrift:** fix reconnect ([482f9ae](https://github.com/nosebit/nodejs-commons/commit/482f9ae83be3046858efcf9b3f34c93597e2fa85))





## [1.1.5](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.1.4...@nosebit/thrift@1.1.5) (2021-10-09)

**Note:** Version bump only for package @nosebit/thrift





## [1.1.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.1.2...@nosebit/thrift@1.1.4) (2021-10-09)

**Note:** Version bump only for package @nosebit/thrift





## [1.1.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.1.2...@nosebit/thrift@1.1.3) (2021-10-09)

**Note:** Version bump only for package @nosebit/thrift





## [1.1.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.1.1...@nosebit/thrift@1.1.2) (2021-10-07)

**Note:** Version bump only for package @nosebit/thrift





## [1.1.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.1.0...@nosebit/thrift@1.1.1) (2021-10-07)

**Note:** Version bump only for package @nosebit/thrift





# [1.1.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.0.16...@nosebit/thrift@1.1.0) (2021-10-07)


### Features

* **thrift:** use tracer instead of logger ([4c8fe4c](https://github.com/nosebit/nodejs-commons/commit/4c8fe4c763f64c80e4dae0726c89b4bd2ad2a7d3))





## [1.0.16](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.0.15...@nosebit/thrift@1.0.16) (2021-09-12)

**Note:** Version bump only for package @nosebit/thrift





## [1.0.15](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.0.14...@nosebit/thrift@1.0.15) (2021-09-11)

**Note:** Version bump only for package @nosebit/thrift





## [1.0.14](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.0.13...@nosebit/thrift@1.0.14) (2021-07-14)


### Bug Fixes

* **thrift:** better logging with thrift service name ([542edcb](https://github.com/nosebit/nodejs-commons/commit/542edcb60c42685708e5f5a6c7fb1cbcee8b38f3))





## [1.0.13](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.0.12...@nosebit/thrift@1.0.13) (2021-07-08)


### Bug Fixes

* **thrift:** fix build ([c6e794a](https://github.com/nosebit/nodejs-commons/commit/c6e794a6969fed494bf8e288f5f6379ca7355e1a))





## [1.0.12](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.0.11...@nosebit/thrift@1.0.12) (2021-06-12)

**Note:** Version bump only for package @nosebit/thrift





## [1.0.11](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.0.10...@nosebit/thrift@1.0.11) (2021-06-09)

**Note:** Version bump only for package @nosebit/thrift





## [1.0.10](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.0.9...@nosebit/thrift@1.0.10) (2020-10-26)

**Note:** Version bump only for package @nosebit/thrift





## [1.0.9](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.0.8...@nosebit/thrift@1.0.9) (2020-10-15)

**Note:** Version bump only for package @nosebit/thrift





## [1.0.8](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.0.7...@nosebit/thrift@1.0.8) (2020-10-15)

**Note:** Version bump only for package @nosebit/thrift





## [1.0.7](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.0.6...@nosebit/thrift@1.0.7) (2020-10-14)

**Note:** Version bump only for package @nosebit/thrift





## [1.0.6](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.0.5...@nosebit/thrift@1.0.6) (2020-10-12)

**Note:** Version bump only for package @nosebit/thrift





## [1.0.5](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.0.4...@nosebit/thrift@1.0.5) (2020-10-12)

**Note:** Version bump only for package @nosebit/thrift





## [1.0.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.0.3...@nosebit/thrift@1.0.4) (2020-10-10)

**Note:** Version bump only for package @nosebit/thrift





## [1.0.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.0.2...@nosebit/thrift@1.0.3) (2020-09-24)

**Note:** Version bump only for package @nosebit/thrift





## [1.0.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.0.1...@nosebit/thrift@1.0.2) (2020-09-12)

**Note:** Version bump only for package @nosebit/thrift





## [1.0.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@1.0.0...@nosebit/thrift@1.0.1) (2020-05-13)

**Note:** Version bump only for package @nosebit/thrift





# [1.0.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift@0.1.0...@nosebit/thrift@1.0.0) (2020-05-12)


### Features

* **@nosebit/thrift:** Require first param of ThriftService class constructor to be an identifier for the service. ([8bfd222](https://github.com/nosebit/nodejs-commons/commit/8bfd222d53d16334b1100a4a092999bbd02dcae7))


### BREAKING CHANGES

* **@nosebit/thrift:** This changes the way thrift services are created.

  To migrate the code follow the example below:

  Before:

  ```
    const srv = new ThriftService(Spec);
  ```

  After:

  ```
    const srv = new ThriftService("myService", Spec);
  ```





# 0.1.0 (2020-04-19)


### Features

* New npm package names removing nodejs-* prefix and adding mongo and thrift mock packages. ([461df10](https://github.com/nosebit/nodejs-commons/commit/461df1099b71f838b046a60f2d232e891a65f666))
