"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = void 0;
//#####################################################
// Imports
//#####################################################
const tracer_1 = require("@nosebit/tracer");
const utils_1 = require("@nosebit/utils");
const lodash_1 = __importDefault(require("lodash"));
const thrift_1 = __importDefault(require("thrift"));
//#####################################################
// Main class
//#####################################################
/**
 * This class create a base interface for all services.
 */
class ThriftService {
    /**
     * This function creates a new instance of this class.
     *
     * @param name - Service name for logging and reference.
     * @param ClientConstructor - A class to construct an instance of client.
     */
    constructor(name, ClientConstructor) {
        /**
         * Flag indicating we are disconnected.
         */
        this._isDisconnected = false;
        this._name = name;
        this._ClientConstructor = ClientConstructor;
    }
    /**
     * Client getter.
     */
    get client() {
        return this._client;
    }
    /**
     * This function connects this service to a remote service instance.
     *
     * @param url - Service url.
     * @param opts - Config options.
     * @param opts.debug - Flag to enable debug logs on thrift connection.
     * @param opts.retryMaxAttemps - Maximum number of retry attemps before giving up.
     * @param opts.retryMaxDelay - Delay in milliseconds between consecutive retry attemps.
     * @param $ctx - Trace context.
     */
    connect(url, opts = {}, $ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const name = this._name;
            /* eslint-disable @typescript-eslint/no-magic-numbers */
            const thriftOpts = {
                connect_timeout: lodash_1.default.defaultTo(opts.retryMaxAttemps, 1000),
                debug: lodash_1.default.defaultTo(opts.debug, true),
                max_attempts: lodash_1.default.defaultTo(opts.retryMaxAttemps, 1000),
                retry_max_delay: lodash_1.default.defaultTo(opts.retryMaxDelay, 5000),
            };
            /* eslint-enable @typescript-eslint/no-magic-numbers */
            return new Promise((resolve) => {
                /**
                 * This function create a new connection.
                 *
                 * @param urlParts - The url object of target service.
                 */
                const createConnection = (urlParts) => {
                    this._connection = thrift_1.default.createConnection(urlParts.hostname, urlParts.port, thriftOpts);
                    const parts = urlParts.hostname.split(".");
                    const serviceName = name || (parts.length > 0 ? parts[0] : null);
                    this._client = thrift_1.default.createClient(this._ClientConstructor, this._connection);
                    /**
                     * BUG : Right now thrift checks for "close" event when
                     * a net.connection is down but the event that gets called
                     * is "end".
                     */
                    this._connection.connection.on("end", () => {
                        if (!this._isDisconnected) {
                            $ctx.logger.warn(`service "${serviceName}" got disconnected`);
                            /**
                             * Thrift way to do retry logic is not working properly
                             * via the function `this._connection.connection_gone()`
                             * so we are going to implement our own logic which is
                             * basically recreate the connection after some time.
                             */
                            setTimeout(() => createConnection(urlParts), thriftOpts.retry_max_delay);
                        }
                    });
                    this._connection.on("connect", () => {
                        if (serviceName) {
                            $ctx.logger.debug(`service "${serviceName}" connected`);
                        }
                        resolve();
                    });
                    // On error, log it.
                    this._connection.on("error", (error) => {
                        /**
                         * Server is down.
                         */
                        if (error.code === "ECONNREFUSED") {
                            $ctx.logger.warn(`service "${serviceName}" is not available yet`);
                            this._connection.end();
                            setTimeout(() => createConnection(urlParts), thriftOpts.retry_max_delay);
                            return;
                        }
                        $ctx.logger.error(`service "${serviceName}" connection error`, error);
                    });
                };
                const originalUrlParts = (0, utils_1.getUrlParts)(url);
                // If no port was provided, then let's try to get it.
                if (originalUrlParts.port) {
                    createConnection(originalUrlParts);
                }
                else {
                    (0, utils_1.resolveUrl)(originalUrlParts.hostname).then((newUrlParts) => {
                        $ctx.logger.debug(`service address resolved : ${url}`, newUrlParts.port);
                        createConnection(newUrlParts);
                    }).catch((error) => {
                        $ctx.logger.error("could not resolve url", error);
                    });
                }
            });
        });
    }
    /**
     * This function disconnects this service from a remote
     * service instance.
     */
    disconnect() {
        if (this._connection) {
            this._isDisconnected = true;
            this._connection.end();
        }
    }
}
__decorate([
    (0, tracer_1.trace)()
], ThriftService.prototype, "connect", null);
__decorate([
    (0, tracer_1.trace)()
], ThriftService.prototype, "disconnect", null);
exports.default = ThriftService;
//# sourceMappingURL=service.js.map