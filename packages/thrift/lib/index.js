"use strict";
/* eslint-disable import/prefer-default-export */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ThriftService = void 0;
var service_1 = require("./service");
Object.defineProperty(exports, "ThriftService", { enumerable: true, get: function () { return __importDefault(service_1).default; } });
//# sourceMappingURL=index.js.map