import { TraceContext } from "@nosebit/tracer";
import thrift from "thrift";
declare type TClientConstructor<TClient> = thrift.TClientConstructor<TClient>;
/**
 * This class create a base interface for all services.
 */
declare class ThriftService<TClient> {
    /**
     * Service name for logging and reference.
     */
    private readonly _name;
    /**
     * The client used to communicate with service instance.
     */
    private _client;
    /**
     * The service constructor.
     */
    private readonly _ClientConstructor;
    /**
     * The service connection.
     */
    private _connection;
    /**
     * Flag indicating we are disconnected.
     */
    private _isDisconnected;
    /**
     * Client getter.
     */
    get client(): TClient;
    /**
     * This function creates a new instance of this class.
     *
     * @param name - Service name for logging and reference.
     * @param ClientConstructor - A class to construct an instance of client.
     */
    constructor(name: string, ClientConstructor?: TClientConstructor<TClient>);
    /**
     * This function connects this service to a remote service instance.
     *
     * @param url - Service url.
     * @param opts - Config options.
     * @param opts.debug - Flag to enable debug logs on thrift connection.
     * @param opts.retryMaxAttemps - Maximum number of retry attemps before giving up.
     * @param opts.retryMaxDelay - Delay in milliseconds between consecutive retry attemps.
     * @param $ctx - Trace context.
     */
    connect(url: string, opts?: {
        debug?: boolean;
        retryMaxAttemps?: number;
        retryMaxDelay?: number;
    }, $ctx?: TraceContext): Promise<void>;
    /**
     * This function disconnects this service from a remote
     * service instance.
     */
    disconnect(): void;
}
export { ThriftService as default, };
