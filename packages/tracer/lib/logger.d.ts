import { ILogger, ILoggerConfig, TLogLevel } from "./types";
/**
 * This class implements a logger which going to use an
 * underlying logger driver to log statements to console,
 * file or logstash/elasticsearch.
 */
export default class Logger implements ILogger {
    /**
     * Global log level for all loggers.
     */
    static level: TLogLevel;
    /**
     * The trace context which owns this logger and from
     * which we going to grab some information about the
     * function/service we are focusing in.
     */
    private readonly _traceCtx;
    /**
     * This is the log driver we use to actually do logging.
     */
    private readonly _driver;
    /**
     * Instance log level.
     */
    private readonly _level;
    /**
     * Scope of logging to be printed together with message.
     */
    private readonly _scope;
    /**
     * Creates a new instance of logger.
     *
     * @param config - Set of config options.
     * @param config.traceCtx - The trace context which owne this logger.
     */
    constructor(config?: ILoggerConfig);
    /**
     * This function implements a generic log function which is
     * the base to all other methods.
     *
     * @param level - The log level (info, error, etc).
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of metadata.
     */
    private _log;
    /**
     * This function creates a debug log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    debug(message: string, ...data: any[]): void;
    /**
     * This function creates a verbose log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    verbose(message: string, ...data: any[]): void;
    /**
     * This function creates an info log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    info(message: string, ...data: any[]): void;
    /**
     * This function creates a warning log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    warn(message: string, ...data: any[]): void;
    /**
     * This function creates an error log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    error(message: string, ...data: any[]): void;
    /**
     * This function emits a critical error log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     */
    critical(message: string, ...data: any[]): void;
    /**
     * This function creates a new logger based on this logger.
     *
     * @param scope - The new scope.
     */
    fork(scope: string): Logger;
}
