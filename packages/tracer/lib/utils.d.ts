import { ITraceHashData } from "./types";
declare const TRACE_HASH_PREFIX = "$TRACE_HASH_";
/**
 * This function going to encode trace context data into
 * a hash.
 *
 * @param hashData - The context data to be encoded to hash.
 */
declare function hashEncode(hashData: ITraceHashData): string;
/**
 * This function going to decode a hash into it's data.
 *
 * @param hash - The hash to be decoded.
 */
declare function hashDecode(hash: string): ITraceHashData;
export { TRACE_HASH_PREFIX, hashDecode, hashEncode, };
