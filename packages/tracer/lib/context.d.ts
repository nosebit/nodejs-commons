import { ICallStackNamesChainItem, ILogDriver, ILogger, ITraceContext, ITraceContextConfig, ITraceContextUser, ITraceContextSharedInitConfig, ITraceDriver, ITraceDriverSpan, SpanStatus, TraceContextInjectMode } from "./types";
/**
 * The `traceFn` is a function that going to wrap a function that
 * clients going to call and it going to provide tracing functionalities
 * for the wrapped function. In order to allow injecting trace context
 * we enforce the called function to receive all it's arguments as a
 * single object instead of using positional parameters. This have two
 * main advantages:
 *
 *   - Positional arguments can be difficult to read when looking at code.
 *     Think of a function call like `doSomething(true)` and
 *     `doSomething({ shouldCallApi: true })`. Since the second call is
 *     more verbose it make easy to understand what is the meaning of the
 *     passed arguments looking only at the code which calls the function.
 *
 *   - Positional arguments can be tricky because we can have optional
 *     arguments and different call signatures for a function. This way we
 *     can end up injecting trace context in the position of an optional
 *     argument that was not provided by the caller. In the past we used a
 *     argIdx prop in the traceFn config object that let user to manually
 *     inform at which position we want to inject the trace context but this
 *     is not elegant :)
 */
export declare type TTracedFnArgs<TArgs extends Object = {}> = TArgs & {
    $ctx?: TraceContext;
    $traceHash?: string;
};
/**
 * Configs we can pass to traceFn.
 */
export interface ITraceFnConfig extends ITraceContextConfig {
    /**
     * Function argument names.
     */
    fnArgNames?: string[];
    /**
     * Option to control the context injection mode.
     */
    injectMode?: TraceContextInjectMode;
    /**
     * Log passed in arguments automatically.
     */
    logArgs?: boolean | string[];
    /**
     * Flag to log start end of all traced functions.
     */
    shouldLogStartEnd?: boolean;
    /**
     * This is the original function we should use as source of
     * truth to collect function arg names.
     */
    originalFn?: Function;
    /**
     * Color associated with the context which will be created.
     */
    serviceColor?: string;
    /**
     * Flag to force record of tracing using the driver.
     */
    shouldRecordTracing?: boolean;
    /**
     * Argument path (in lodash get format) to get the trace hash.
     */
    traceHashArgPath?: string;
}
export declare type TTracedFn<TReturn = void> = (...args: any[]) => TReturn | Promise<TReturn>;
/**
 * The trace namespace map.
 */
export interface ITraceNSMap {
    [key: string]: TTracedFn;
}
/**
 * This class implements a context for all tracing
 * functionality.
 */
declare class TraceContext<TData = any> implements ITraceContext<TData> {
    /**
     * Name of the service under tracing.
     */
    private static _serviceName;
    /**
     * Color to be used when logging the service name
     * associated with this stuff.
     */
    private static _serviceColor;
    /**
     * Trace driver to be used.
     */
    private static _logLevel;
    /**
     * Trace driver to be used.
     */
    private static _shouldLogStartEnd;
    /**
     * Trace driver to be used.
     */
    private static _traceDriver;
    /**
     * Flag to force record of tracing using the driver.
     */
    private static _shouldRecordTracing;
    /**
     * Log driver to be used when creating loggers.
     */
    private static _logDriver;
    /**
     * Shared trace context.
     */
    private static _shared;
    /**
     * Current user.
     */
    private static _user;
    /**
     * Inject mode to drive how we inject context into
     * traced function.
     */
    private static _injectMode;
    /**
     * Getter for shared trace context.
     */
    static get shared(): TraceContext<any>;
    /**
     * Getter for trace driver.
     */
    static get traceDriver(): ITraceDriver;
    /**
     * Getter for traceDriverRecord.
     */
    static get shouldRecordTracing(): boolean;
    /**
     * Getter for logStartEnd.
     */
    static get shouldLogStartEnd(): boolean;
    /**
     * Getter for log driver.
     */
    static get logDriver(): ILogDriver;
    /**
     * Getter for the log level of all contexts.
     */
    static get logLevel(): "error" | "warn" | "info" | "debug" | "verbose";
    /**
     * Getter for global logger
     */
    static get logger(): ILogger;
    /**
     * Getter for global service color.
     */
    static get serviceColor(): string;
    /**
     * Getter for global service name.
     */
    static get serviceName(): string;
    /**
     * Setter for global service color.
     */
    static set user(user: ITraceContextUser);
    /**
     * Setter for global service color.
     */
    static get user(): ITraceContextUser;
    /**
     * Getter for inject mode.
     */
    static get injectMode(): TraceContextInjectMode;
    /**
     * This function going to initialized a shared trace
     * context.
     *
     * @param config - A set of config options.
     */
    static sharedInit(config?: ITraceContextSharedInitConfig): TraceContext<any>;
    /**
     * This function checks if shared instance already exists
     * and therefore if global context was initialized.
     */
    static sharedExists(): boolean;
    /**
     * This functions only return data if we are in environments other
     * than production.
     *
     * @param data - The secret data to log if we are allowed to (dev like envs).
     */
    static secret<TData = any>(data: TData): TData;
    /**
     * This function going to create a trace context from a trace
     * hash.
     *
     * @param hash - The hash representing the parent context.
     */
    static fromHash(hash: string): TraceContext<any>;
    /**
     * Trace context going to hold information about the function
     * we are tracing like fileName, fnName, etc.
     */
    /**
     * The class name this context is associated with.
     */
    private readonly _callStackNamesChain;
    /**
     * The class name this context is associated with.
     */
    private readonly _className;
    /**
     * Unique hash id identifying this context.
     */
    private readonly _id;
    /**
     * Flag for enabling auto tracing which going to automatically
     * create a function level span which can then be used inside
     * the function to trace down other parts of the function and
     * to pass it over to other function automatically.
     */
    private readonly _autoTracing;
    /**
     * Function arguments.
     */
    private readonly _fnArgs;
    /**
     * Lits of function args to log with args name in the logging
     * output.
     */
    private readonly _fnArgsToLog;
    /**
     * List of function results to log.
     */
    private readonly _fnResultsToLog;
    /**
     * Name of the function we are tracing.
     */
    private readonly _fnName;
    /**
     * Path of the file where the function lives.
     */
    private readonly _filePath;
    /**
     * A logger instance so we can log stuff related to this
     * context.
     */
    private readonly _logger;
    /**
     * Context associated with a function that called the creation
     * of this context. This way we going to get a stack of contexts.
     */
    private readonly _parent;
    /**
     * Color to be used for all logs produced by this context logger.
     */
    private readonly _serviceColor;
    /**
     * Name of the service associated with this context.
     */
    private readonly _serviceName;
    /**
     * Hash of the span representing the span.
     */
    private readonly _spanHash;
    /**
     * Trace span associated with this context.
     */
    span: ITraceDriverSpan;
    /**
     * Public data carried over by this context.
     */
    data: TData;
    /**
     * Id getter.
     */
    get id(): string;
    /**
     * Getter for auto tracing flag.
     */
    get autoTracing(): boolean;
    /**
     * This function get the name/ids of all context stack
     * iterating of content ancestors.
     */
    get callStackName(): string;
    /**
     * This getter is going to retrieve _callStackNamesChain.
     */
    get callStackNamesChain(): ICallStackNamesChainItem[];
    /**
     * Getter for className.
     */
    get className(): string;
    /**
     * Function args getter.
     */
    get fnArgs(): Object;
    /**
     * Function args to log getter.
     */
    get fnArgsToLog(): string[];
    /**
     * Function args to log getter.
     */
    get fnName(): string;
    /**
     * Function full name including class name if present.
     */
    get fnFullName(): string;
    /**
     * Function args to log getter.
     */
    get fnResultsToLog(): boolean | string[];
    /**
     * File path getter.
     */
    get filePath(): string;
    /**
     * Flag indicating if this context is the shared one.
     */
    get isShared(): boolean;
    /**
     * Logger getter.
     */
    get logger(): ILogger;
    /**
     * Parent getter.
     */
    get parent(): ITraceContext<any>;
    /**
     * Getter for service color.
     */
    get serviceColor(): string;
    /**
     * Getter for service name.
     */
    get serviceName(): string;
    /**
     * Getter for span hash.
     */
    get spanHash(): string;
    /**
     * Creates a new instance of trace context.
     *
     * @param config - Set of config options.
     */
    constructor(config?: ITraceContextConfig);
    /**
     * This function get the name/ids of all context stack
     * iterating of content ancestors.
     */
    private _evalCallStackNamesChain;
    /**
     * This function going to call another function and inject
     * the context to it's args.
     *
     * @param fn - Function to be called.
     * @param args - The function arguments.
     */
    call<TArgs = any, TReturn = void>(fn: (args: TArgs) => TReturn, args: TArgs): TReturn;
    /**
     * This function going to bind context into a set of arguments.
     *
     * @param args - The function arguments.
     */
    bind<TArgs extends Object = any>(args: TArgs): TTracedFnArgs<TArgs>;
    /**
     * This function going to use the client to sent an error
     * notification to Sentry.
     *
     * @param error - The error to notify.
     */
    errorNotify(error: Error): void;
    /**
     * This function going to encode this context into a hash
     * we going to use to trace down requests.
     */
    toHash(): string;
    /**
     * This function going to start tracing associated with this
     * context.
     */
    traceStart(): void;
    /**
     * This function going to finish tracing associated with this
     * context.
     *
     * @param status - Finish status.
     * @param _data - Any data we want to associate with span.
     */
    traceFinish(status?: SpanStatus, _data?: any): void;
}
declare function traceFn<TFn extends Function>(fnToTrace: TFn): TFn;
declare function traceFn<TFn extends Function>(fnNameOrConfig: string | ITraceFnConfig, fnToTrace: TFn): TFn;
declare function traceFn<TFn extends Function>(fnName: string, config: ITraceFnConfig, fnToTrace: TFn): TFn;
/**
 * This function going to create a trace namespace so we
 * can trace standalone functions (i.e., not class methods).
 *
 * @param nsMap -
 *   A map of the functions to trace.
 */
declare const traceNs: <TMap extends ITraceNSMap>(nsMap: TMap) => TMap;
/**
 * This function going to trace class methods.
 *
 * @param config - Trace context config options.
 */
declare function trace(config?: ITraceFnConfig): (...decArgs: any[]) => void;
export { trace, TraceContext as default, traceFn, traceNs, };
