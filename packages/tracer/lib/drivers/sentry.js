"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = void 0;
const Tracing = __importStar(require("@sentry/tracing"));
const types_1 = require("../types");
//#####################################################
// Auxiliary Classes
//#####################################################
/**
 * This class implements the ITraceDriverSpan interface
 * for a sentry span.
 */
class SentryTraceDriverSpan {
    /**
     * Creates a new span instance.
     *
     * @param span - Sentry span instance.
     */
    constructor(span) {
        this._span = span;
    }
    /**
     * This function going to convert our span status to
     * their span status.
     *
     * @param status - Our span status.
     */
    static ourSpanStatusToTheir(status) {
        if (!status) {
            return null;
        }
        switch (status) {
            case types_1.SpanStatus.ABORTED:
                return Tracing.SpanStatus.Aborted;
            case types_1.SpanStatus.ALREADY_EXISTS:
                return Tracing.SpanStatus.AlreadyExists;
            case types_1.SpanStatus.CANCELLED:
                return Tracing.SpanStatus.Cancelled;
            case types_1.SpanStatus.DATA_LOSS:
                return Tracing.SpanStatus.DataLoss;
            case types_1.SpanStatus.DEADLINE_EXCEEDED:
                return Tracing.SpanStatus.DeadlineExceeded;
            case types_1.SpanStatus.FAILED_PRECONDITION:
                return Tracing.SpanStatus.FailedPrecondition;
            case types_1.SpanStatus.INTERNAL_ERROR:
                return Tracing.SpanStatus.InternalError;
            case types_1.SpanStatus.INVALID_ARGUMENT:
                return Tracing.SpanStatus.InvalidArgument;
            case types_1.SpanStatus.NOT_FOUND:
                return Tracing.SpanStatus.NotFound;
            case types_1.SpanStatus.OK:
                return Tracing.SpanStatus.Ok;
            case types_1.SpanStatus.OUT_OF_RANGE:
                return Tracing.SpanStatus.OutOfRange;
            case types_1.SpanStatus.PERMISSION_DENIED:
                return Tracing.SpanStatus.PermissionDenied;
            case types_1.SpanStatus.RESOURCE_EXHAUSTED:
                return Tracing.SpanStatus.ResourceExhausted;
            case types_1.SpanStatus.UNAUTHENTICATED:
                return Tracing.SpanStatus.Unauthenticated;
            case types_1.SpanStatus.UNAVAILABLE:
                return Tracing.SpanStatus.Unavailable;
            case types_1.SpanStatus.UNIMPLEMENTED:
                return Tracing.SpanStatus.Unimplemented;
            case types_1.SpanStatus.UNKNOWN_ERROR:
                return Tracing.SpanStatus.UnknownError;
            default:
                return null;
        }
    }
    /**
     * This function going to convert our span status to
     * their span status.
     *
     * @param status - Our span status.
     */
    static theirSpanStatusToOur(status) {
        if (!status) {
            return null;
        }
        switch (status) {
            case Tracing.SpanStatus.Aborted:
                return types_1.SpanStatus.ABORTED;
            case Tracing.SpanStatus.AlreadyExists:
                return types_1.SpanStatus.ALREADY_EXISTS;
            case Tracing.SpanStatus.Cancelled:
                return types_1.SpanStatus.CANCELLED;
            case Tracing.SpanStatus.DataLoss:
                return types_1.SpanStatus.DATA_LOSS;
            case Tracing.SpanStatus.DeadlineExceeded:
                return types_1.SpanStatus.DEADLINE_EXCEEDED;
            case Tracing.SpanStatus.FailedPrecondition:
                return types_1.SpanStatus.FAILED_PRECONDITION;
            case Tracing.SpanStatus.InternalError:
                return types_1.SpanStatus.INTERNAL_ERROR;
            case Tracing.SpanStatus.InvalidArgument:
                return types_1.SpanStatus.INVALID_ARGUMENT;
            case Tracing.SpanStatus.NotFound:
                return types_1.SpanStatus.NOT_FOUND;
            case Tracing.SpanStatus.Ok:
                return types_1.SpanStatus.OK;
            case Tracing.SpanStatus.OutOfRange:
                return types_1.SpanStatus.OUT_OF_RANGE;
            case Tracing.SpanStatus.PermissionDenied:
                return types_1.SpanStatus.PERMISSION_DENIED;
            case Tracing.SpanStatus.ResourceExhausted:
                return types_1.SpanStatus.RESOURCE_EXHAUSTED;
            case Tracing.SpanStatus.Unauthenticated:
                return types_1.SpanStatus.UNAUTHENTICATED;
            case Tracing.SpanStatus.Unavailable:
                return types_1.SpanStatus.UNAVAILABLE;
            case Tracing.SpanStatus.Unimplemented:
                return types_1.SpanStatus.UNIMPLEMENTED;
            case Tracing.SpanStatus.UnknownError:
                return types_1.SpanStatus.UNKNOWN_ERROR;
            default:
                return null;
        }
    }
    /**
     * Gets the span id.
     */
    get id() {
        return this._span.spanId;
    }
    /**
     * Gets the trace id.
     */
    get traceId() {
        return this._span.traceId;
    }
    /**
     * Getter for span status.
     */
    get status() {
        return SentryTraceDriverSpan.theirSpanStatusToOur(this._span.status);
    }
    /**
     * Setter for span status.
     */
    set status(status) {
        this._span.status = SentryTraceDriverSpan.ourSpanStatusToTheir(status);
    }
    /**
     * This function going to create a child span of this span.
     *
     * @param opName -
     *  The operation name associated with the span to be created.
     * @param config -
     *  Set of config options to drive the span creation.
     */
    childStart(opName, config) {
        return new SentryTraceDriverSpan(this._span.startChild({
            data: config.data,
            op: opName,
            status: SentryTraceDriverSpan.ourSpanStatusToTheir(config.status),
            tags: config.tags,
        }));
    }
    /**
     * This function going to finish the underlying span.
     */
    finish() {
        this._span.finish();
    }
    /**
     * This function going to encode this span in a hash string.
     */
    toHash() {
        /**
         * Function toTraceparent is defined here in the following
         * source file. Sentry encodes the span in a string form and
         * send it over from frontend to backend in the sentry-trace
         * request header.
         *
         * https://github.com/getsentry/sentry-javascript/blob/a6f8dc26a4c7ae2146ae64995a2018c8578896a6/packages/tracing/src/span.ts#L241
         */
        return this._span.toTraceparent();
    }
}
//#####################################################
// Main Class
//#####################################################
/**
 * This class implements an abstract blueprint for
 * sentry trace driver. Any sentry trace driver needs to
 * inherit from this.
 */
class SentryTraceDriver {
    /**
     * This function going to initialize this driver.
     */
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this._client) {
                throw new Error("sentry client not set");
            }
            this._client.init(this._config);
        });
    }
    /**
     * This function going to set the current authenticated user
     * and associate all trace requests to this user.
     *
     * @param user - The user info.
     */
    setUser(user) {
        this._client.setUser(user);
    }
    /**
     * This function going to start a new span.
     *
     * @param opName -
     *  The operation name associated with the span to be created.
     * @param config -
     *  Set of config options to drive the span creation.
     */
    spanStart(opName, config = {}) {
        if (!config.parent) {
            let parentData = {};
            if (config.parentHash) {
                /**
                 * The function `extractTraceparentData` is defined in the
                 * following source file:
                 *
                 * https://github.com/getsentry/sentry-javascript/blob/dcdb1130586e3a4d42b3611b41952f329a8da94e/packages/tracing/src/utils.ts#L35
                 *
                 * and it's used in the following source file to start a new
                 * sentry transaction with traceparent id request header:
                 *
                 * https://github.com/getsentry/sentry-javascript/blob/dcdb1130586e3a4d42b3611b41952f329a8da94e/packages/node/src/handlers.ts#L46
                 */
                parentData = Tracing.extractTraceparentData(config.parentHash);
            }
            // Start a root span as a transaction.
            return new SentryTraceDriverSpan(this._client.startTransaction(Object.assign({ data: config.data, name: opName, op: opName, status: SentryTraceDriverSpan.ourSpanStatusToTheir(config.status), tags: config.tags }, parentData)));
        }
        return config.parent.childStart(opName, config);
    }
    /**
     * This function going to use the client to sent an error
     * notification to Sentry.
     *
     * @param error - The error to notify.
     */
    errorNotify(error) {
        this._client.captureException(error);
    }
}
exports.default = SentryTraceDriver;
//# sourceMappingURL=sentry.js.map