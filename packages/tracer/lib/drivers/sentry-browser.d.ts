import * as Sentry from "@sentry/browser";
import SentryTraceDriver from "./sentry";
/**
 * This is the main class.
 */
declare class SentryBrowserTraceDriver extends SentryTraceDriver {
    /**
     * This function going to create a new sentry trace driver
     * instance using sentry browser client under the hood.
     *
     * @param config - Set of config options.
     */
    constructor(config: Sentry.BrowserOptions);
}
export { SentryBrowserTraceDriver as default, };
