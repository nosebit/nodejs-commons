import * as Sentry from "@sentry/node";
import SentryTraceDriver from "./sentry";
/**
 * This is the main class.
 */
declare class SentryNodeJsTraceDriver extends SentryTraceDriver {
    /**
     * This function going to create a new sentry trace driver
     * instance using sentry browser client under the hood.
     *
     * @param config - Set of config options.
     */
    constructor(config: Sentry.NodeOptions);
}
export { SentryNodeJsTraceDriver as default, };
