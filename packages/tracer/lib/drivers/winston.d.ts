import { ILogDriver, ILogDriverConfig, ILogDriverLogOpts } from "../types";
/**
 * Winston driver config options.
 */
interface IWinstonLogDriverConfig extends ILogDriverConfig {
}
/**
 * This class implements a log driver which uses winston
 * logger under the hood.
 */
declare class WinstonLogDriver implements ILogDriver {
    /**
     * Underlying logger to use.
     */
    private _logger;
    /**
     * The log file path if we want to log to file.
     */
    private readonly _logFilePath;
    /**
     * Url to a logstash server.
     */
    private readonly _logstashUrl;
    /**
     * Boolean indicating if we shoudl log to console.
     */
    private readonly _logToConsole;
    /**
     * This function going to create a new instance of
     * this driver.
     *
     * @param config - Set of config options.
     */
    constructor(config?: IWinstonLogDriverConfig);
    /**
     * This function going to initialize the driver.
     */
    init(): void;
    /**
     * This function implements the main logging function.
     *
     * @param level - The log level.
     * @param message - A main message to be logged.
     * @param data - Extra data to be logged with the message.
     * @param opts - Extra options to be used.
     */
    log(level: string, message: string, data?: any, opts?: ILogDriverLogOpts): void;
}
export { WinstonLogDriver as default, };
