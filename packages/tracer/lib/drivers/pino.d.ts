import { ILogDriver, ILogDriverLogOpts, TLogLevel } from "../types";
/**
 * This class implements a log driver which uses pino
 * logger under the hood.
 */
declare class PinoLogDriver implements ILogDriver {
    /**
     * Underlying logger to use.
     */
    private _logger;
    /**
     * This function going to initialize the driver.
     */
    init(): void;
    /**
     * This function implements the main logging function.
     *
     * @param level - The log level.
     * @param message - A main message to be logged.
     * @param data - Extra data to be logged with the message.
     * @param opts - Extra options to be used.
     */
    log(level: TLogLevel, message: string, data?: any, opts?: ILogDriverLogOpts): void;
}
export { PinoLogDriver as default, };
