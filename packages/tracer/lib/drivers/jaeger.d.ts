import { ITraceContextUser, ITraceDriver, ITraceDriverSpan, ITraceDriverSpanConfig } from "../types";
interface IJaegerTraceDriverConfig {
    host?: string;
}
/**
 * This class implements a jaeger tracer.
 */
declare class JaegerTraceDriver implements ITraceDriver {
    /**
     * The jaeger driver (or client).
     */
    private _tracer;
    /**
     * Jaeger server host.
     */
    private readonly _host;
    /**
     * This function creates a new jaeger tracer instance.
     *
     * @param config - The config options.
     */
    constructor(config: IJaegerTraceDriverConfig);
    /**
     * This function going to initialize the driver.
     */
    init(): void;
    /**
     * This function going to set the current authenticated user
     * and associate all trace requests to this user.
     *
     * @param _user - The user info.
     */
    setUser(_user: ITraceContextUser): void;
    /**
     * This function starts a new span.
     *
     * @param opName - Name of the operation to be traced.
     * @param config - A set of config options.
     */
    spanStart(opName: string, config?: ITraceDriverSpanConfig): ITraceDriverSpan;
    /**
     * This function going to use the client to sent an error
     * notification to Sentry.
     *
     * @param _error - The error to notify.
     */
    errorNotify(_error: Error): void;
}
export { JaegerTraceDriver as default, };
