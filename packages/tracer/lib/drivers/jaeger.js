"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = void 0;
const jaeger_client_1 = require("jaeger-client");
const uuid_1 = require("uuid");
const context_1 = __importDefault(require("../context"));
const types_1 = require("../types");
//#####################################################
// Constants
//#####################################################
// Tracer key id
const TRACE_ID = "uber-trace-id";
//#####################################################
// Auxiliary Classes
//#####################################################
/**
 * This class implements the ITraceDriverSpan interface
 * for a sentry span.
 */
class JaegerTraceDriverSpan {
    /**
     * Creates a new span instance.
     *
     * @param tracer - The tracer instance.
     * @param span - Sentry span instance.
     */
    constructor(tracer, span) {
        this._tracer = tracer;
        this._span = span;
    }
    /**
     * Gets the span id.
     */
    get id() {
        return "cole";
    }
    /**
     * Gets the trace id.
     */
    get traceId() {
        return "cole";
    }
    /**
     * Getter for status.
     */
    get status() {
        return types_1.SpanStatus.OK;
    }
    /**
     * This function going to create a child span of this span.
     *
     * @param opName -
     *  The operation name associated with the span to be created.
     * @param _config -
     *  Set of config options to drive the span creation.
     */
    childStart(opName, _config) {
        return new JaegerTraceDriverSpan(this._tracer, this._tracer.startSpan(opName, {
            childOf: {
                id: this.id,
            },
        }));
    }
    /**
     * This function going to finish the underlying span.
     */
    finish() {
        this._span.finish();
    }
    /**
     * This function going to encode this span in a hash string.
     */
    toHash() {
        if (!this._span) {
            return (0, uuid_1.v4)();
        }
        const headers = {
            [TRACE_ID]: "",
        };
        this._tracer.inject(this._span, "http_headers", headers);
        return headers[TRACE_ID];
    }
}
//#####################################################
// Main class
//#####################################################
/**
 * This class implements a jaeger tracer.
 */
class JaegerTraceDriver {
    /**
     * This function creates a new jaeger tracer instance.
     *
     * @param config - The config options.
     */
    constructor(config) {
        this._host = config.host || "localhost";
    }
    /**
     * This function going to initialize the driver.
     */
    init() {
        this._tracer = (0, jaeger_client_1.initTracer)({
            reporter: {
                agentHost: this._host,
            },
            sampler: {
                host: this._host,
                param: 1,
                type: "const",
            },
            serviceName: context_1.default.serviceName,
        }, {
            logger: context_1.default.logger,
        });
    }
    /**
     * This function going to set the current authenticated user
     * and associate all trace requests to this user.
     *
     * @param _user - The user info.
     */
    setUser(_user) {
        // Do nothing
    }
    /**
     * This function starts a new span.
     *
     * @param opName - Name of the operation to be traced.
     * @param config - A set of config options.
     */
    spanStart(opName, config) {
        if (config.parentHash) {
            const span = this._tracer.extract("http_headers", {
                [TRACE_ID]: config.parentHash,
            });
            return new JaegerTraceDriverSpan(this._tracer, span);
        }
        return new JaegerTraceDriverSpan(this._tracer, this._tracer.startSpan(opName, config));
    }
    /**
     * This function going to use the client to sent an error
     * notification to Sentry.
     *
     * @param _error - The error to notify.
     */
    errorNotify(_error) {
        // Do nothing.
    }
}
exports.default = JaegerTraceDriver;
//# sourceMappingURL=jaeger.js.map