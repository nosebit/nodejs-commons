import * as Tracing from "@sentry/tracing";
import { ITraceContextUser, ITraceDriver, ITraceDriverSpan, ITraceDriverSpanConfig } from "../types";
interface ISentrySpanContext {
    data?: {
        [key: string]: any;
    };
    op?: string;
    status?: Tracing.SpanStatus;
    tags?: {
        [key: string]: string | boolean | number;
    };
}
export interface ISentryTransactionContext extends ISentrySpanContext {
    name: string;
}
interface ISentrySpan {
    spanId: string;
    traceId: string;
    status?: string;
    startChild: (ctx: ISentrySpanContext) => ISentrySpan;
    toTraceparent: () => string;
    finish: () => void;
}
interface ISentryTransaction extends ISentrySpan {
}
interface ISentryUser {
    id?: string;
    username?: string;
    email?: string;
    ip_address?: string;
}
interface ISentryClient {
    captureException: (error: Error) => void;
    init: (config: any) => void;
    setUser: <TUser extends ISentryUser>(user: TUser) => void;
    startTransaction: (ctx: ISentryTransactionContext, samplingCtx?: any) => ISentryTransaction;
}
/**
 * This class implements an abstract blueprint for
 * sentry trace driver. Any sentry trace driver needs to
 * inherit from this.
 */
declare abstract class SentryTraceDriver implements ITraceDriver {
    /**
     * The main sentry client to use.
     */
    protected _client: ISentryClient;
    /**
     * The config options we should use to initialize sentry
     * client.
     */
    protected _config: any;
    /**
     * This function going to initialize this driver.
     */
    init(): Promise<void>;
    /**
     * This function going to set the current authenticated user
     * and associate all trace requests to this user.
     *
     * @param user - The user info.
     */
    setUser(user: ITraceContextUser): void;
    /**
     * This function going to start a new span.
     *
     * @param opName -
     *  The operation name associated with the span to be created.
     * @param config -
     *  Set of config options to drive the span creation.
     */
    spanStart(opName: string, config?: ITraceDriverSpanConfig): ITraceDriverSpan;
    /**
     * This function going to use the client to sent an error
     * notification to Sentry.
     *
     * @param error - The error to notify.
     */
    errorNotify(error: Error): void;
}
export { SentryTraceDriver as default, };
