"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = void 0;
const utils_1 = require("@nosebit/utils");
const chalk_1 = __importDefault(require("chalk"));
const dayjs_1 = __importDefault(require("dayjs"));
const lodash_1 = __importDefault(require("lodash"));
const winston = __importStar(require("winston"));
const winston_logstash_transport_1 = require("winston-logstash-transport");
const context_1 = __importDefault(require("../context"));
//#####################################################
// Main Class
//#####################################################
/**
 * This class implements a log driver which uses winston
 * logger under the hood.
 */
class WinstonLogDriver {
    /**
     * This function going to create a new instance of
     * this driver.
     *
     * @param config - Set of config options.
     */
    constructor(config = {}) {
        this._logFilePath = config.logFilePath;
        this._logstashUrl = config.logstashUrl;
        this._logToConsole = lodash_1.default.defaultTo(config.logToConsole, true);
    }
    /**
     * This function going to initialize the driver.
     */
    init() {
        // Create the winston logger.
        this._logger = winston.createLogger({
            format: winston.format.json(),
            /**
             * We going to handle level in our logger instead of in
             * winston so that we can have a winston singleton driver.
             */
            level: "silly",
        });
        // Setup the driver.
        const { combine, timestamp, printf, colorize, } = winston.format;
        const ignorePrivate = winston.format((info) => {
            if (info.private) {
                return false;
            }
            return info;
        });
        if (this._logToConsole) {
            this._logger.add(new winston.transports.Console({
                format: combine(colorize(), ignorePrivate(), timestamp(), printf((info) => {
                    const traceCtx = info.traceCtx;
                    let dataStr = "";
                    /**
                     * Transform data to string so we can log it better.
                     */
                    if (info.data) {
                        try {
                            dataStr = JSON.stringify(info.data);
                        }
                        catch (error) {
                            dataStr = info.data;
                        }
                        if (dataStr && dataStr.length) {
                            dataStr = chalk_1.default.gray(dataStr);
                        }
                    }
                    /**
                     * As prefix we going to show the timestamp followed
                     * by the service name (if provided) followed by the
                     * caller name.
                     */
                    let prefix = (0, dayjs_1.default)(info.timestamp).toISOString();
                    if (context_1.default.serviceName) {
                        const textColor = context_1.default.serviceColor || "#999999";
                        const serviceNamePrefix = chalk_1.default.bold.hex(textColor)(` [${context_1.default.serviceName}]`);
                        prefix = `${prefix}${serviceNamePrefix}`;
                    }
                    /**
                     * Call stack info.
                     */
                    let callStackNamesStr;
                    if (traceCtx) {
                        callStackNamesStr = traceCtx.callStackName;
                        if (callStackNamesStr.length) {
                            callStackNamesStr = chalk_1.default.bold.cyan(`by ${callStackNamesStr}`);
                        }
                    }
                    const parts = [
                        prefix,
                        info.level,
                        info.message,
                        dataStr,
                        callStackNamesStr,
                    ].filter(Boolean);
                    return parts.join(" ");
                })),
            }));
        }
        if (this._logFilePath) {
            this._logger.add(new winston.transports.File({
                filename: this._logFilePath,
            }));
        }
        if (this._logstashUrl) {
            const urlParts = (0, utils_1.getUrlParts)(this._logstashUrl);
            const logstashOpts = {
                host: urlParts.hostname,
                node_name: "logstash",
                port: urlParts.port,
            };
            this._logger.add(new winston_logstash_transport_1.LogstashTransport(logstashOpts));
        }
    }
    /**
     * This function implements the main logging function.
     *
     * @param level - The log level.
     * @param message - A main message to be logged.
     * @param data - Extra data to be logged with the message.
     * @param opts - Extra options to be used.
     */
    log(level, message, data, opts) {
        this._logger.log(level, message, Object.assign({ data }, opts));
    }
}
exports.default = WinstonLogDriver;
//# sourceMappingURL=winston.js.map