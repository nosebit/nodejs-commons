"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = void 0;
const dayjs_1 = __importDefault(require("dayjs"));
const pino_1 = __importDefault(require("pino"));
const context_1 = __importDefault(require("../context"));
//#####################################################
// Constants
//#####################################################
const browserLevelColors = {
    critical: "red",
    debug: "blue",
    error: "red",
    info: "green",
    verbose: "gray",
    warn: "yellow",
};
//#####################################################
// Main Class
//#####################################################
/**
 * This class implements a log driver which uses pino
 * logger under the hood.
 */
class PinoLogDriver {
    /**
     * This function going to initialize the driver.
     */
    init() {
        let transport;
        /**
         * If we are running in NodeJS then lets create the transport.
         */
        if (typeof window === "undefined") {
            transport = pino_1.default.transport({
                targets: [
                    {
                        level: "debug",
                        options: {},
                        target: "@nosebit/tracer/lib/drivers/pino-console-transport",
                    },
                ],
            });
        }
        this._logger = (0, pino_1.default)({
            browser: {
                asObject: true,
                serialize: true,
                write: (logObj) => {
                    // eslint-disable-next-line
                    const { data, levelName, msg, time, } = logObj;
                    // eslint-disable-next-line prefer-destructuring
                    const traceCtx = logObj.traceCtx;
                    const timeWithStyles = [
                        `%c${(0, dayjs_1.default)(time).toISOString()}`,
                        "color: original",
                    ];
                    let levelNameWithStyles;
                    if (levelName) {
                        const levelColor = browserLevelColors[levelName] || "gray";
                        levelNameWithStyles = [
                            `%c${levelName}`,
                            `color: ${levelColor}; font-weight: bold`,
                        ];
                    }
                    /**
                     * Function full name.
                     */
                    let fnFullNameWithStyles;
                    let callStackNameWithStyles;
                    if (traceCtx) {
                        fnFullNameWithStyles = [
                            `%c${traceCtx.fnFullName}`,
                            "color: cyan; font-weigh: bold;",
                        ];
                        const { callStackName } = traceCtx;
                        if (callStackName) {
                            callStackNameWithStyles = [
                                `%cby ${callStackName}`,
                                "color: magenta; font-weigh: bold;",
                            ];
                        }
                    }
                    let msgWithStyles;
                    if (msg) {
                        msgWithStyles = [
                            `%c${msg}`,
                            "color: original",
                        ];
                    }
                    let dataWithStyles;
                    if (data) {
                        try {
                            const dataStr = JSON.stringify(data);
                            if (dataStr && dataStr.length) {
                                dataWithStyles = [
                                    `%c${dataStr}`,
                                    "color: gray",
                                ];
                            }
                        }
                        catch (error) {
                            // Fail
                        }
                    }
                    const parts = [
                        timeWithStyles,
                        levelNameWithStyles,
                        fnFullNameWithStyles,
                        msgWithStyles,
                        dataWithStyles,
                        callStackNameWithStyles,
                    ].filter(Boolean).reduce((map, item) => {
                        map.args.push(item[0]);
                        map.styles.push(item[1]);
                        return map;
                    }, { args: [], styles: [] });
                    const consoleArgs = [
                        parts.args.join(" "),
                        ...parts.styles,
                    ].filter(Boolean);
                    /* eslint-disable sort-keys, no-console */
                    console.log(...consoleArgs);
                    if (data) {
                        console.log(data);
                    }
                    /* eslint-enable sort-keys, no-console */
                },
            },
            customLevels: {
                verbose: 10,
            },
            formatters: {
                bindings: () => ({}),
                log: (payload) => {
                    /**
                     * Call stack info.
                     */
                    let $tid;
                    let $parentTid;
                    let $call;
                    let $fnName;
                    if (payload.traceCtx) {
                        $tid = payload.traceCtx.id;
                        $call = payload.traceCtx.callStackName;
                        $fnName = payload.traceCtx.fnFullName;
                        if (payload.traceCtx.parent) {
                            $parentTid = payload.traceCtx.parent.id;
                        }
                    }
                    return {
                        $call,
                        $fnName,
                        $parentTid,
                        $tid,
                        color: payload.traceCtx.serviceColor,
                        data: payload.data,
                        levelName: payload.levelName,
                        msg: payload.msg,
                        name: payload.traceCtx.serviceName,
                    };
                },
            },
            level: "debug",
            name: context_1.default.serviceName,
        }, transport);
    }
    /**
     * This function implements the main logging function.
     *
     * @param level - The log level.
     * @param message - A main message to be logged.
     * @param data - Extra data to be logged with the message.
     * @param opts - Extra options to be used.
     */
    log(level, message, data, opts) {
        const obj = Object.assign({ data, levelName: level, msg: message }, opts);
        switch (level) {
            case "debug": return this._logger.debug(obj);
            case "info": return this._logger.info(obj);
            case "warn": return this._logger.warn(obj);
            case "error": return this._logger.error(obj);
            default: return this._logger.debug(obj);
        }
    }
}
exports.default = PinoLogDriver;
//# sourceMappingURL=pino.js.map