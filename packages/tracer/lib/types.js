"use strict";
/**
 * This file is intended to prevent circular dependency.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TraceContextInjectMode = exports.SpanStatus = exports.LOG_LEVELS = void 0;
//#####################################################
// Logger Types
//#####################################################
exports.LOG_LEVELS = [
    "error",
    "warn",
    "info",
    "debug",
    "verbose",
];
//#####################################################
// Tracer Types
//#####################################################
var SpanStatus;
(function (SpanStatus) {
    SpanStatus["ABORTED"] = "aborted";
    SpanStatus["ALREADY_EXISTS"] = "already_exists";
    SpanStatus["CANCELLED"] = "cancelled";
    SpanStatus["DATA_LOSS"] = "data_loss";
    SpanStatus["DEADLINE_EXCEEDED"] = "deadline_exceeded";
    SpanStatus["FAILED_PRECONDITION"] = "failed_precondition";
    SpanStatus["INTERNAL_ERROR"] = "internal_error";
    SpanStatus["INVALID_ARGUMENT"] = "invalid_argument";
    SpanStatus["NOT_FOUND"] = "not_found";
    SpanStatus["OK"] = "ok";
    SpanStatus["OUT_OF_RANGE"] = "out_of_range";
    SpanStatus["PERMISSION_DENIED"] = "permission_denied";
    SpanStatus["RESOURCE_EXHAUSTED"] = "resource_exhausted";
    SpanStatus["UNAUTHENTICATED"] = "unauthenticated";
    SpanStatus["UNAVAILABLE"] = "unavailable";
    SpanStatus["UNIMPLEMENTED"] = "unimplemented";
    SpanStatus["UNKNOWN_ERROR"] = "unknown_error";
})(SpanStatus = exports.SpanStatus || (exports.SpanStatus = {}));
//#####################################################
// Context Types
//#####################################################
var TraceContextInjectMode;
(function (TraceContextInjectMode) {
    /**
     * With this option we are going to inject context in
     * the first arg object the function receive. The key
     * we add is "$ctx" and this is useful in react or grpc
     * were all arguments are grouped together in a object
     * and passed it over to functions.
     */
    TraceContextInjectMode["IN_FIRST_ARG_OBJECT"] = "in_first_arg_object";
    /**
     * With this option we are going to inject the context
     * as a regular argument of a called function. This is
     * the default.
     */
    TraceContextInjectMode["AS_ARG"] = "as_last_arg";
})(TraceContextInjectMode = exports.TraceContextInjectMode || (exports.TraceContextInjectMode = {}));
//# sourceMappingURL=types.js.map