"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.hashEncode = exports.hashDecode = exports.TRACE_HASH_PREFIX = void 0;
const lzutf8_1 = __importDefault(require("lzutf8"));
//#####################################################
// Constants
//#####################################################
const TRACE_HASH_PREFIX = "$TRACE_HASH_";
exports.TRACE_HASH_PREFIX = TRACE_HASH_PREFIX;
//#####################################################
// Utilitary Functions
//#####################################################
/**
 * This function going to encode trace context data into
 * a hash.
 *
 * @param hashData - The context data to be encoded to hash.
 */
function hashEncode(hashData) {
    const base64Hash = lzutf8_1.default.compress(JSON.stringify(hashData), {
        outputEncoding: "Base64",
    });
    return `${TRACE_HASH_PREFIX}${base64Hash}`;
}
exports.hashEncode = hashEncode;
/**
 * This function going to decode a hash into it's data.
 *
 * @param hash - The hash to be decoded.
 */
function hashDecode(hash) {
    const base64Hash = hash.replace(TRACE_HASH_PREFIX, "");
    const hashDataStr = lzutf8_1.default.decompress(base64Hash, {
        inputEncoding: "Base64",
    });
    const hashData = JSON.parse(hashDataStr);
    return hashData;
}
exports.hashDecode = hashDecode;
//# sourceMappingURL=utils.js.map