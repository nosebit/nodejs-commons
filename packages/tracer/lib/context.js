"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.traceNs = exports.traceFn = exports.default = exports.trace = void 0;
const dayjs_1 = __importDefault(require("dayjs"));
const lodash_1 = __importDefault(require("lodash"));
const nanoid_1 = require("nanoid");
const pino_1 = __importDefault(require("./drivers/pino"));
const logger_1 = __importDefault(require("./logger"));
const types_1 = require("./types");
const utils_1 = require("./utils");
//#####################################################
// Constants
//#####################################################
/**
 * ~357 days needed, in order to have a 1% probability
 * of at least one collision.
 *
 * https://zelark.github.io/nano-id-cc/
 */
const nanoid = (0, nanoid_1.customAlphabet)("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 10);
/**
 * Regex for identifying trace function arg name for
 * trace context.
 */
const CTX_ARG_NAME_REGEX = /^_?\$ctx$/;
/**
 * Function signature regexes.
 */
const FN_SIGN = /(?:function)? ?[^ (]*?\(((?:.|\n)*?)(?=\) ?(?:=>|{))/;
/**
 * Set of regexes we going to use to parse function signature
 * to get argument names.
 */
const FN_ARG_REGEXES = [
    // Simple argument (no param)
    /^([\w\d-_$]+),/,
    // Argument with param followed by simple arg.
    /^([^ =]+)=((?:[^=]|=>)+?)(?=,[\w\d-_$]+,)/,
    // Argument with param followed by argument with param
    /^([^ =]+)=(.+?)(?=,[\w\d-_$]+=)/,
    // Argument with param at the end (if none of the prev matched)
    /^([^ =]+)=(.+),$/,
];
//#####################################################
// Auxiliary Functions
//#####################################################
/**
 * This function going to extract the argument names of
 * a function as provided in the function signature.
 *
 * @param fn - Target function.
 */
function getFnArgNames(fn) {
    if (fn.$__argNames) {
        return fn.$__argNames;
    }
    const fnStr = fn.toString().replace(/\n| /g, "");
    const MAX_TRIES = 150;
    const argNames = [];
    const argsSignMatch = FN_SIGN.exec(fnStr);
    if (argsSignMatch) {
        let [, argsStr] = argsSignMatch;
        // Remove first parenthesis.
        argsStr = argsStr.replace(/\(/, "");
        // Remove line breaks.
        argsStr = argsStr.replace(/\n/g, "");
        // Remove white spaces.
        argsStr = argsStr.replace(/ +/g, "");
        // Remove inline comments.
        argsStr = argsStr.replace(/\/\*.*?\*\//g, "");
        // Add auxiliary comma at the end.
        if (argsStr.length && !argsStr.endsWith(",")) {
            argsStr = `${argsStr},`;
        }
        let tryCount = 0;
        // Start getting arg names in order.
        while (argsStr.length && tryCount < MAX_TRIES) {
            tryCount++;
            for (const rgx of FN_ARG_REGEXES) {
                const match = rgx.exec(argsStr);
                if (match) {
                    argNames.push(match[1]);
                    argsStr = argsStr.replace(match[0], "");
                    /* eslint-disable max-depth */
                    if (argsStr.startsWith(",")) {
                        argsStr = argsStr.replace(",", "");
                    }
                    /* eslint-enable max-depth */
                    break;
                }
            }
        }
        if (argsStr.length && tryCount === MAX_TRIES) {
            throw new Error("Failed to get function arg names");
        }
        fn.$__argNames = argNames;
    }
    return argNames;
}
//#####################################################
// Main Class
//#####################################################
/**
 * This class implements a context for all tracing
 * functionality.
 */
class TraceContext {
    /**
     * Creates a new instance of trace context.
     *
     * @param config - Set of config options.
     */
    constructor(config = {}) {
        this._id = config.id || nanoid();
        this._autoTracing = lodash_1.default.defaultTo(config.autoTracing, true);
        this._serviceName = config.serviceName || TraceContext.serviceName;
        this._serviceColor = config.serviceColor || TraceContext.serviceColor;
        this._callStackNamesChain = config.callStackNamesChain;
        this._className = config.className;
        this._filePath = config.filePath;
        this._fnArgs = config.fnArgs;
        this._fnArgsToLog = config.fnArgsToLog;
        this._fnResultsToLog = config.fnResultsToLog;
        this._fnName = config.fnName;
        this._parent = config.parent;
        this._spanHash = config.spanHash;
        this._logger = new logger_1.default({
            driver: TraceContext._logDriver,
            level: TraceContext.logLevel,
            traceCtx: this,
        });
        // Automatically pass parent data down.
        if (this._parent && this._parent.data) {
            this.data = this._parent.data;
        }
    }
    /**
     * Getter for shared trace context.
     */
    static get shared() {
        return TraceContext._shared;
    }
    /**
     * Getter for trace driver.
     */
    static get traceDriver() {
        return TraceContext._traceDriver;
    }
    /**
     * Getter for traceDriverRecord.
     */
    static get shouldRecordTracing() {
        return TraceContext._shouldRecordTracing;
    }
    /**
     * Getter for logStartEnd.
     */
    static get shouldLogStartEnd() {
        return TraceContext._shouldLogStartEnd;
    }
    /**
     * Getter for log driver.
     */
    static get logDriver() {
        return TraceContext._logDriver;
    }
    /**
     * Getter for the log level of all contexts.
     */
    static get logLevel() {
        return TraceContext._logLevel;
    }
    /**
     * Getter for global logger
     */
    static get logger() {
        return TraceContext.shared
            ? TraceContext.shared.logger
            : new logger_1.default({
                level: TraceContext.logLevel,
            });
    }
    /**
     * Getter for global service color.
     */
    static get serviceColor() {
        return TraceContext._serviceColor;
    }
    /**
     * Getter for global service name.
     */
    static get serviceName() {
        return TraceContext._serviceName;
    }
    /**
     * Setter for global service color.
     */
    static set user(user) {
        TraceContext._user = user;
        if (TraceContext.traceDriver) {
            TraceContext.traceDriver.setUser(user);
        }
    }
    /**
     * Setter for global service color.
     */
    static get user() {
        return TraceContext._user;
    }
    /**
     * Getter for inject mode.
     */
    static get injectMode() {
        return TraceContext._injectMode;
    }
    /**
     * This function going to initialized a shared trace
     * context.
     *
     * @param config - A set of config options.
     */
    static sharedInit(config = {}) {
        if (!TraceContext._shared) {
            TraceContext._serviceName = config.serviceName;
            TraceContext._serviceColor = config.serviceColor;
            TraceContext._traceDriver = config.traceDriver;
            TraceContext._logDriver = config.logDriver || new pino_1.default();
            TraceContext._logLevel = config.logLevel;
            TraceContext._shared = new TraceContext({ fnName: "main" });
            TraceContext._injectMode
                = config.injectMode || types_1.TraceContextInjectMode.AS_ARG;
            TraceContext._shouldLogStartEnd
                = lodash_1.default.defaultTo(config.shouldLogStartEnd, true);
            TraceContext._shouldRecordTracing
                = lodash_1.default.defaultTo(config.shouldRecordTracing, true);
            // Setup global logger level.
            logger_1.default.level = config.logLevel;
            /**
             * Initialize trace driver now. This way driver can use the
             * created shared trace context.
             */
            if (TraceContext._traceDriver) {
                TraceContext._traceDriver.init();
            }
            /**
             * Initialize log driver now. This way driver can use the
             * created shared trace context.
             */
            if (TraceContext._logDriver) {
                TraceContext._logDriver.init();
            }
        }
        return TraceContext._shared;
    }
    /**
     * This function checks if shared instance already exists
     * and therefore if global context was initialized.
     */
    static sharedExists() {
        return Boolean(TraceContext.shared);
    }
    /**
     * This functions only return data if we are in environments other
     * than production.
     *
     * @param data - The secret data to log if we are allowed to (dev like envs).
     */
    static secret(data) {
        if (process.env.LOG_SECRETS) {
            return data;
        }
    }
    /**
     * This function going to create a trace context from a trace
     * hash.
     *
     * @param hash - The hash representing the parent context.
     */
    static fromHash(hash) {
        const hashData = (0, utils_1.hashDecode)(hash);
        return new TraceContext(hashData);
    }
    /**
     * Id getter.
     */
    get id() {
        return this._id;
    }
    /**
     * Getter for auto tracing flag.
     */
    get autoTracing() {
        return this._autoTracing;
    }
    /**
     * This function get the name/ids of all context stack
     * iterating of content ancestors.
     */
    get callStackName() {
        const chain = this._evalCallStackNamesChain();
        let currServiceName = TraceContext.serviceName;
        let currClassName;
        const names = [];
        let currNames = [];
        for (const item of chain) {
            const itemNames = [item.fnName];
            if (item.className !== currClassName
                || item.serviceName !== currServiceName) {
                if (currNames.length) {
                    names.push(currNames.join("."));
                }
                if (item.className) {
                    itemNames.unshift(item.className);
                }
                if (item.serviceName
                    && item.serviceName !== currServiceName
                    && item.serviceName !== TraceContext.serviceName) {
                    itemNames[0] = `[${item.serviceName}]${itemNames[0]}`;
                }
                currNames = [];
                currClassName = item.className;
                currServiceName = item.serviceName;
            }
            currNames.push(itemNames.join(":"));
        }
        if (currNames.length) {
            names.push(currNames.join("."));
        }
        return names.join("->");
    }
    /**
     * This getter is going to retrieve _callStackNamesChain.
     */
    get callStackNamesChain() {
        return this._callStackNamesChain;
    }
    /**
     * Getter for className.
     */
    get className() {
        return this._className;
    }
    /**
     * Function args getter.
     */
    get fnArgs() {
        return this._fnArgs;
    }
    /**
     * Function args to log getter.
     */
    get fnArgsToLog() {
        return this._fnArgsToLog;
    }
    /**
     * Function args to log getter.
     */
    get fnName() {
        return this._fnName;
    }
    /**
     * Function full name including class name if present.
     */
    get fnFullName() {
        return [
            this.className,
            this.fnName,
        ].filter(Boolean).join(":");
    }
    /**
     * Function args to log getter.
     */
    get fnResultsToLog() {
        return this._fnResultsToLog;
    }
    /**
     * File path getter.
     */
    get filePath() {
        return this._filePath;
    }
    /**
     * Flag indicating if this context is the shared one.
     */
    get isShared() {
        return this === TraceContext.shared;
    }
    /**
     * Logger getter.
     */
    get logger() {
        return this._logger;
    }
    /**
     * Parent getter.
     */
    get parent() {
        return this._parent;
    }
    /**
     * Getter for service color.
     */
    get serviceColor() {
        return this._serviceColor;
    }
    /**
     * Getter for service name.
     */
    get serviceName() {
        return this._serviceName;
    }
    /**
     * Getter for span hash.
     */
    get spanHash() {
        return this._spanHash;
    }
    /**
     * This function get the name/ids of all context stack
     * iterating of content ancestors.
     */
    _evalCallStackNamesChain() {
        if (this.isShared) {
            return [];
        }
        if (this._callStackNamesChain) {
            return this._callStackNamesChain;
        }
        const names = [
            {
                className: this.className,
                fnName: this.fnName || this._id,
                serviceName: this.serviceName,
            },
        ];
        let nextParent = this.parent;
        while (nextParent) {
            if (nextParent.callStackNamesChain) {
                names.unshift(...nextParent.callStackNamesChain);
                break;
            }
            names.unshift({
                className: nextParent.className,
                fnName: nextParent.fnName || nextParent.id,
                serviceName: nextParent.serviceName,
            });
            nextParent = nextParent.parent;
        }
        return names;
    }
    /**
     * This function going to call another function and inject
     * the context to it's args.
     *
     * @param fn - Function to be called.
     * @param args - The function arguments.
     */
    call(fn, args) {
        return fn(Object.assign(Object.assign({}, args), { $ctx: this }));
    }
    /**
     * This function going to bind context into a set of arguments.
     *
     * @param args - The function arguments.
     */
    bind(args) {
        return Object.assign(Object.assign({}, args), { $ctx: this, $traceHash: this.toHash() });
    }
    /**
     * This function going to use the client to sent an error
     * notification to Sentry.
     *
     * @param error - The error to notify.
     */
    errorNotify(error) {
        if (TraceContext.traceDriver) {
            TraceContext.traceDriver.errorNotify(error);
        }
    }
    /**
     * This function going to encode this context into a hash
     * we going to use to trace down requests.
     */
    toHash() {
        const hashData = {
            callStackNamesChain: this._evalCallStackNamesChain(),
            className: this.className,
            fnName: this.fnName,
            id: this.id,
            serviceName: TraceContext.serviceName,
        };
        if (this.span) {
            hashData.spanHash = this.span.toHash();
        }
        return (0, utils_1.hashEncode)(hashData);
    }
    /**
     * This function going to start tracing associated with this
     * context.
     */
    traceStart() {
        /**
         * Start a new context span.
         */
        if (TraceContext.traceDriver
            && this.autoTracing
            && this.fnName) {
            this.span = TraceContext.traceDriver.spanStart(this.fnName, {
                parent: this.parent && this.parent.span,
                parentHash: this.parent && this.parent.spanHash,
                tags: {
                    serviceName: TraceContext.serviceName,
                },
            });
        }
    }
    /**
     * This function going to finish tracing associated with this
     * context.
     *
     * @param status - Finish status.
     * @param _data - Any data we want to associate with span.
     */
    traceFinish(status = types_1.SpanStatus.OK, _data) {
        if (this.span) {
            this.span.status = status;
            this.span.finish();
        }
    }
}
exports.default = TraceContext;
/**
 * Trace driver to be used.
 */
TraceContext._logLevel = "info";
/**
 * Trace driver to be used.
 */
TraceContext._shouldLogStartEnd = true;
/**
 * Flag to force record of tracing using the driver.
 */
TraceContext._shouldRecordTracing = true;
/**
 * This is the main trace function that going to trace
 * the start/end of execution of a function, log the args
 * and inject a custom logger the function can use.
 *
 * @param fnNameOrConfigOrFn -
 *  A name to be assigned to the function we are about to trace or
 *  a config object containing fnName as a field or the function itself.
 * @param fnToTraceOrConfig -
 *  The function to trace or a config object.
 * @param fnToTrace -
 *  The function to trace if not already provided in the other args.
 */
function traceFn(fnNameOrConfigOrFn, // eslint-disable-line max-len
fnToTraceOrConfig, fnToTrace) {
    let config = {};
    let fnToCall = fnToTrace;
    let fnToGetInfoFrom = fnToTrace;
    let traceFnConfig = {};
    if (lodash_1.default.isString(fnNameOrConfigOrFn)) {
        config.fnName = fnNameOrConfigOrFn;
    }
    else if (lodash_1.default.isPlainObject(fnNameOrConfigOrFn)) {
        traceFnConfig = fnNameOrConfigOrFn;
    }
    else if (lodash_1.default.isFunction(fnNameOrConfigOrFn)) {
        fnToCall = fnNameOrConfigOrFn;
        fnToGetInfoFrom = fnToCall;
    }
    if (lodash_1.default.isFunction(fnToTraceOrConfig)) {
        fnToCall = fnToTraceOrConfig;
        fnToGetInfoFrom = fnToCall;
    }
    else if (lodash_1.default.isPlainObject(fnToTraceOrConfig)) {
        traceFnConfig = fnToTraceOrConfig;
    }
    // Extract options from config.
    fnToGetInfoFrom = traceFnConfig.originalFn || fnToGetInfoFrom;
    config = Object.assign(Object.assign({}, config), traceFnConfig);
    if (!fnToCall) {
        return null;
    }
    config.fnName = config.fnName || fnToGetInfoFrom.name;
    /**
     * @todo : The following uses function toString to get the argument
     * names in function signature. This might not be a good idea because
     * of performance. Maybe we should drop this solution and just assume
     * that we going to inject context as the last argument in the function
     * (using function length to know how many arguments it can receive).
     */
    /**
     * To improve the efficiency a little bit we going to make sure we
     * get arg names just once for the function.
     */
    const argNames = traceFnConfig.fnArgNames
        || getFnArgNames(fnToGetInfoFrom);
    /* eslint-disable complexity */
    /**
     * This is a wrapped version of the function we want to
     * trace.
     *
     * @param args - List of arguments.
     */
    return (...args) => {
        const injectMode = traceFnConfig.injectMode || TraceContext.injectMode;
        const shouldLogStartEnd = traceFnConfig.shouldLogStartEnd
            || TraceContext.shouldLogStartEnd;
        const shouldRecordTracing = traceFnConfig.shouldRecordTracing
            || TraceContext.shouldRecordTracing;
        let $parentCtx;
        let $parentTraceHash;
        /**
         * Try to find parent context among the received args.
         */
        if (config.parent) {
            $parentCtx = config.parent;
        }
        else {
            for (let i = 0; i < args.length; i++) {
                const arg = args[i];
                if (lodash_1.default.isPlainObject(arg) && arg.$ctx) {
                    $parentCtx = arg.$ctx;
                    $parentTraceHash = arg.$traceHash;
                }
                else if (arg instanceof TraceContext) {
                    $parentCtx = arg;
                }
                else if (lodash_1.default.isString(arg) && arg.startsWith("$TRACE_HASH_")) {
                    $parentTraceHash = arg;
                }
                if ($parentCtx || $parentTraceHash) {
                    break;
                }
            }
        }
        // Use traceHashArgPath to get traceHash.
        if (traceFnConfig.traceHashArgPath) {
            const traceHashArg = lodash_1.default.get(args, traceFnConfig.traceHashArgPath);
            if (lodash_1.default.isFunction(traceHashArg)) {
                $parentTraceHash = traceHashArg();
            }
            else if (lodash_1.default.isString(traceHashArg)) {
                $parentTraceHash = traceHashArg;
            }
        }
        if (!$parentCtx && $parentTraceHash) {
            $parentCtx = TraceContext.fromHash($parentTraceHash);
        }
        const $ctx = new TraceContext(Object.assign(Object.assign({}, config), { fnArgs: [], parent: $parentCtx }));
        /**
         * Start a new context span.
         */
        if (shouldRecordTracing) {
            $ctx.traceStart();
        }
        /**
         * Record start time so we can evaluate elapsed time.
         */
        const startTime = (0, dayjs_1.default)();
        /**
         * Log start with arguments if provided.
         */
        let startArgs;
        if (traceFnConfig.logArgs) {
            startArgs = {};
            if (lodash_1.default.isArray(traceFnConfig.logArgs)) {
                traceFnConfig.logArgs.forEach((key) => {
                    const keyParts = key.split(".");
                    const argName = keyParts.shift();
                    const valPath = keyParts.length > 0
                        ? keyParts.join(".")
                        : null;
                    const argIdx = argNames.findIndex((name) => argName === name);
                    if (argIdx >= 0) {
                        const argVal = args[argIdx];
                        const valToLog = valPath
                            ? lodash_1.default.get(argVal, valPath)
                            : argVal;
                        lodash_1.default.set(startArgs, key, valToLog);
                    }
                });
            }
            else {
                for (let i = 0; i < args.length; i++) {
                    const arg = args[i];
                    const argName = argNames[i];
                    lodash_1.default.set(startArgs, argName, arg);
                }
            }
        }
        if (shouldLogStartEnd) {
            $ctx.logger.debug("start", startArgs);
        }
        /**
         * Encapsulate the finish process into a function to handle
         * the case when the function to trace returns a promise.
         *
         * @param result - Traced function result.
         * @param error - Traced function thrown error.
         */
        const finish = (result, error) => {
            const endTime = (0, dayjs_1.default)();
            if (shouldLogStartEnd) {
                $ctx.logger.debug(`end [ELAPSED=${endTime.diff(startTime, "milliseconds")}]`);
            }
            /**
             * If we started a trace context span let's finish it
             * now to mark the end of function execution.
             */
            if (shouldRecordTracing) {
                $ctx.traceFinish(error ? types_1.SpanStatus.INTERNAL_ERROR : types_1.SpanStatus.OK);
            }
            return result;
        };
        /**
         * Call function injecting new context both to first arg
         * if it's a plain object and at the end of the function.
         * The injection going to done like the following:
         *
         * 1) If the first argument is a plain object then we going
         *    to inject the $ctx in that object. This is useful
         *    for React components where we receive everything packaged
         *    in a props object.
         * 2) Otherwise if we have an argument named $ctx in the fnToCall
         *    signature then we going to inject $ctx as an argument in that
         *    exact position (filling other positional arguments with
         *    undefined).
         */
        const newArgs = new Array(argNames.length).fill(undefined); // eslint-disable-line no-undefined
        /**
         * Since fnToCall.length does not consider arguments with
         * default values we need to use argNames as the source of
         * truth for the amount of arguments a function can receive.
         */
        for (let i = 0; i < argNames.length; i++) {
            if (i < args.length) {
                newArgs[i] = CTX_ARG_NAME_REGEX.test(argNames[i])
                    ? $ctx
                    : args[i];
            }
            else if (CTX_ARG_NAME_REGEX.test(argNames[i])) {
                newArgs[i] = $ctx;
            }
        }
        if (injectMode === types_1.TraceContextInjectMode.IN_FIRST_ARG_OBJECT) {
            if (!newArgs.length) {
                newArgs.push({});
            }
            if (lodash_1.default.isPlainObject(newArgs[0])) {
                newArgs[0] = Object.assign(Object.assign({}, newArgs[0]), { $ctx });
            }
        }
        let result = fnToCall(...newArgs);
        /**
         * Finish up.
         */
        if (result instanceof Promise) {
            result = result
                .then(finish)
                .catch((error) => {
                $ctx.logger.error("fail", {
                    fileName: error.fileName,
                    message: error.message,
                });
                finish(null, error);
                throw error;
            });
        }
        else {
            try {
                finish();
            }
            catch (error) {
                $ctx.logger.error("fail", {
                    fileName: error.fileName,
                    message: error.message,
                });
                finish(null, error);
            }
        }
        return result;
    };
    /* eslint-enable complexity */
}
exports.traceFn = traceFn;
//#####################################################
// Namespace Trace
//#####################################################
/**
 * This function going to create a trace namespace so we
 * can trace standalone functions (i.e., not class methods).
 *
 * @param nsMap -
 *   A map of the functions to trace.
 */
const traceNs = (nsMap) => Object.keys(nsMap).reduce((ns, fnName) => {
    const fn = nsMap[fnName];
    return Object.assign(Object.assign({}, ns), { [fnName]: traceFn(fnName, fn) });
}, {});
exports.traceNs = traceNs;
//#####################################################
// Class Method Trace
//#####################################################
/**
 * This function going to trace class methods.
 *
 * @param config - Trace context config options.
 */
function trace(config = {}) {
    return function traceDecorator(...decArgs) {
        if (decArgs.length !== 3 || typeof decArgs[2] === "number") {
            throw new Error("Method does not allow decoration");
        }
        /* eslint-disable prefer-destructuring */
        const target = decArgs[0];
        const descriptor = decArgs[2];
        /* eslint-enable prefer-destructuring */
        const originalMethod = descriptor.value;
        const methodName = originalMethod.name;
        const fnArgNames = getFnArgNames(originalMethod);
        descriptor.value = function value(...args) {
            /**
             * When decorated method is a static one target.name going
             * to be the class name. Otherwise, if decorated method is
             * an instance method then target.name is undefined and we
             * get the class name from constructor name.
             */
            const className = target.name || target.constructor.name;
            return traceFn(Object.assign(Object.assign({}, config), { className,
                fnArgNames, fnArgs: args[0], fnName: methodName, originalFn: originalMethod }), originalMethod.bind(this)).apply(this, args);
        };
    };
}
exports.trace = trace;
//# sourceMappingURL=context.js.map