# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.5.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/tracer@0.5.1...@nosebit/tracer@0.5.2) (2022-04-26)

**Note:** Version bump only for package @nosebit/tracer





## [0.5.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/tracer@0.5.0...@nosebit/tracer@0.5.1) (2022-02-09)

**Note:** Version bump only for package @nosebit/tracer





# [0.5.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/tracer@0.4.2...@nosebit/tracer@0.5.0) (2022-01-07)


### Bug Fixes

* **tracer:** fix span status name typos ([1b3c711](https://github.com/nosebit/nodejs-commons/commit/1b3c711b23a5c5def4d934c9861a3805f4a5c788))


### Features

* **tracer:** allow passing service color to trace decorator function ([37a7db5](https://github.com/nosebit/nodejs-commons/commit/37a7db5745b0fa161701f658af4a05a1352230fd))





## [0.4.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/tracer@0.4.1...@nosebit/tracer@0.4.2) (2021-11-14)


### Bug Fixes

* **tracer:** correct trace logging ([0e2eaf1](https://github.com/nosebit/nodejs-commons/commit/0e2eaf15790e8de867541eb7d8661486555fba77))
* **tracer:** use parentHash instead of hash config opt ([0316aec](https://github.com/nosebit/nodejs-commons/commit/0316aec9a73e8815beba3fefc8e38b7a59b83785))





## [0.4.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/tracer@0.4.0...@nosebit/tracer@0.4.1) (2021-10-09)


### Bug Fixes

* **tracer:** throw error when get arg names max tries is achieved ([4247890](https://github.com/nosebit/nodejs-commons/commit/4247890b4de9bcb515c0c0a1959810360b8f997b))





# [0.4.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/tracer@0.2.1...@nosebit/tracer@0.4.0) (2021-10-09)


### Bug Fixes

* **tracer:** fix include compiler field to include js files ([afd9e14](https://github.com/nosebit/nodejs-commons/commit/afd9e14e833c73a4bc20b393501ad2ae0a9058f8))


### Features

* **tracer:** better parsing of fn arg names considering default params ([979ad3c](https://github.com/nosebit/nodejs-commons/commit/979ad3cc9742a84af9fa411afd6842b6615d9a90))





# [0.3.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/tracer@0.2.1...@nosebit/tracer@0.3.0) (2021-10-09)


### Features

* **tracer:** better parsing of fn arg names considering default params ([979ad3c](https://github.com/nosebit/nodejs-commons/commit/979ad3cc9742a84af9fa411afd6842b6615d9a90))





## [0.2.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/tracer@0.2.0...@nosebit/tracer@0.2.1) (2021-10-07)


### Bug Fixes

* **tracer:** fix log spacing between message and callstack name ([aaee3a9](https://github.com/nosebit/nodejs-commons/commit/aaee3a90c607b5cc60bdc5b9e82d829e25849012))





# [0.2.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/tracer@0.1.0...@nosebit/tracer@0.2.0) (2021-10-07)


### Features

* **tracer:** add injectMode config param and improve get function args method ([dca8fef](https://github.com/nosebit/nodejs-commons/commit/dca8fef26fc79d46eb1bc32eb876192bc8cb0a5b))





# 0.1.0 (2021-10-07)


### Bug Fixes

* **tracer:** make ts lint happy and remove lib folder ([3b0c1fa](https://github.com/nosebit/nodejs-commons/commit/3b0c1fa5bcf5434b106ad04939461350da64aa04))


### Features

* **tracer:** add new tracer package ([26ddf92](https://github.com/nosebit/nodejs-commons/commit/26ddf9239365ebcc94e0186b52851be69778659d))
