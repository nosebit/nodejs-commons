"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseSearchParams = exports.parseSearchQuery = void 0;
const lodash_1 = __importDefault(require("lodash"));
const moment_1 = __importDefault(require("moment"));
const mongodb_1 = require("mongodb");
/**
 * This function parse a primitive type to a more complex type
 * based on type user informed.
 *
 * @param value - The primitive value.
 * @param transform - The type we wish to convert the primitive value to.
 */
function parseValue(value, transform) {
    switch (transform) {
        case "id": {
            return new mongodb_1.ObjectId(value);
        }
        case "idList": {
            return value.map((aValue) => new mongodb_1.ObjectId(aValue));
        }
        case "date": {
            return (0, moment_1.default)(value).toDate();
        }
        case "null": {
            return { $type: "null" };
        }
        default: {
            break;
        }
    }
    return value === "null" ? null : value;
}
/**
 * This function convert a nosebit search query to a mongo
 * query so we can perform queries in mongo.
 *
 * @param query - The nosebit search query.
 * @param $ctx - Trace context.
 */
function parseSearchQuery(query, $ctx) {
    const searchField = query.field;
    const { transform } = query;
    const mongoQuery = {};
    if (query._and) {
        mongoQuery.$and = query._and.map((nestedQuery) => parseSearchQuery(nestedQuery, $ctx));
    }
    else if (query._or) {
        mongoQuery.$or = query._or.map((nestedQuery) => parseSearchQuery(nestedQuery, $ctx));
    }
    else if (query._not) {
        mongoQuery.$not = parseSearchQuery(query._not, $ctx);
    }
    else if (query._elemMatch) {
        mongoQuery[searchField] = {
            $elemMatch: parseSearchQuery(query._elemMatch, $ctx),
        };
    }
    else {
        const keys = Object.keys(query);
        for (const key of keys) {
            const value = lodash_1.default.get(query, key);
            if (key.startsWith("_map")) {
                value.forEach((mapVal, mapKey) => {
                    mongoQuery[mapKey] = parseValue(mapVal, transform);
                });
            }
            else if (key.startsWith("_eq")) {
                mongoQuery[searchField] = parseValue(value, transform);
            }
            else if ((/^_gt|_lt/).test(key)) {
                const opId = key.replace(/_|Str|Int|Float/g, "");
                lodash_1.default.set(mongoQuery, `${searchField}.$${opId}`, parseValue(value, transform));
            }
            else if ((/^_regex$/).test(key)) {
                const match = value.match(/\/([^/]+)\/([^/]+)/);
                if (match) {
                    mongoQuery[searchField] = {
                        $options: lodash_1.default.get(match, "2"),
                        $regex: match[1],
                    };
                }
            }
            else if (key.startsWith("_in")) {
                mongoQuery[searchField] = {
                    $in: parseValue(value, transform),
                };
            }
            else if (key.startsWith("_all")) {
                mongoQuery[searchField] = {
                    $all: parseValue(value, transform),
                };
            }
            else if (key.startsWith("_ne")) {
                mongoQuery[searchField] = {
                    $ne: parseValue(value, transform),
                };
            }
            else if (key !== "field") {
                mongoQuery[searchField] = parseValue(value, transform);
            }
        }
    }
    return mongoQuery;
}
exports.parseSearchQuery = parseSearchQuery;
/**
 * This function convert a search params to a mongo
 * query so we can perform queries in mongo.
 *
 * @param params - The search params to be parsed.
 * @param $ctx - Trace context.
 */
function parseSearchParams(params, $ctx) {
    const { query, sort } = params;
    const result = {
        query: parseSearchQuery(query),
    };
    if (!params.includeDeleted) {
        result.query = Object.assign(Object.assign({}, result.query), { deletedAt: { $type: "null" } });
    }
    if (sort) {
        result.sort = sort.reduce((accum, item) => {
            const parts = item.split(":");
            let order = 1;
            if (parts.length > 1) {
                try {
                    order = parseInt(parts[1], 10);
                }
                catch (error) {
                    $ctx.logger.error("could not parse order to int", error);
                }
            }
            accum[parts[0]] = order;
            return accum;
        }, {});
    }
    return result;
}
exports.parseSearchParams = parseSearchParams;
//# sourceMappingURL=search.js.map