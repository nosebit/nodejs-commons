import { TraceContext } from "@nosebit/tracer";
/**
 * This is the interface for normalized search queries
 * (all databases). We can call this Nosebit search
 * query.
 */
export interface ISearchQuery {
    field?: string;
    transform?: string;
    _eqStr?: string;
    _eqInt?: number;
    _eqFloat?: number;
    _eqBool?: boolean;
    _eqStrList?: string[];
    _eqIntList?: number[];
    _eqFloatList?: number[];
    _eqBoolList?: boolean[];
    _neStr?: string;
    _neInt?: number;
    _neFloat?: number;
    _neBool?: boolean;
    _inStr?: string[];
    _inInt?: number[];
    _inFloat?: number[];
    _inBool?: boolean[];
    _mapEqStr?: Map<string, string>;
    _mapEqInt?: Map<string, number>;
    _mapEqFloat?: Map<string, number>;
    _regex?: string;
    _gtStr?: string;
    _gteStr?: string;
    _ltStr?: string;
    _lteStr?: string;
    _gtInt?: number;
    _gteInt?: number;
    _ltInt?: number;
    _lteInt?: number;
    _gtFloat?: number;
    _gteFloat?: number;
    _ltFloat?: number;
    _lteFloat?: number;
    _and?: ISearchQuery[];
    _or?: ISearchQuery[];
    _not?: ISearchQuery;
    _elemMatch?: ISearchQuery;
}
/**
 * This is the common interface for params all search methods
 * going to receive (all databases).
 */
export interface ISearchParams {
    query?: ISearchQuery;
    sort?: string[];
    limit?: number;
    includeDeleted?: boolean;
}
/**
 * This is the search params for mongo.
 */
export interface IMongoSearchParams {
    query: {
        [key: string]: any;
    };
    sort?: {
        [key: string]: number;
    };
    limit?: number;
}
/**
 * This function convert a nosebit search query to a mongo
 * query so we can perform queries in mongo.
 *
 * @param query - The nosebit search query.
 * @param $ctx - Trace context.
 */
declare function parseSearchQuery(query: ISearchQuery, $ctx?: TraceContext): {
    [key: string]: any;
};
/**
 * This function convert a search params to a mongo
 * query so we can perform queries in mongo.
 *
 * @param params - The search params to be parsed.
 * @param $ctx - Trace context.
 */
declare function parseSearchParams(params: ISearchParams, $ctx?: TraceContext): IMongoSearchParams;
export { parseSearchQuery, parseSearchParams, };
