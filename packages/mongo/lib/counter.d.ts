import { TraceContext } from "@nosebit/tracer";
import { Filter, UpdateFilter, FindOneAndUpdateOptions } from "mongodb";
import { Db } from "./client";
/**
 * Counter data that is stored in db.
 */
interface ICounter {
    _id: string;
    count: number;
}
/**
 * Interface for the insert result.
 */
interface IDbInsertResult {
    insertedId: string | {
        toString: () => string;
    };
}
/**
 * Interface for the insert result.
 */
interface IDbFindOneAndUpdateResult {
    value?: ICounter;
}
/**
 * Interface for mongodb collection driver.
 */
interface ICounterCollection {
    insertOne: (counter: ICounter) => Promise<IDbInsertResult>;
    findOneAndUpdate: (filter: Filter<ICounter>, update: UpdateFilter<ICounter>, option: FindOneAndUpdateOptions) => Promise<IDbFindOneAndUpdateResult>;
}
/**
 * Interface for counter config options.
 */
interface ICounterConfig {
    mode?: string;
    collection?: ICounterCollection;
    name?: string;
}
/**
 * This class handles the counters collection.
 */
declare class Counter {
    /**
     * Shared instance.
     */
    private static _shared;
    /**
     * This function initializes a shared mongo client.
     *
     * @param config - A set of config options.
     */
    static sharedInit(config: ICounterConfig): Promise<Counter>;
    /**
     * This function gets the shared instance.
     */
    static get shared(): Counter;
    /**
     * Check if shared instance exists.
     */
    static sharedExists(): boolean;
    /**
     * This function setup the counter collection.
     *
     * @param db - The database connector instance.
     * @param $ctx - Trace context.
     */
    static setupCollection(db?: Partial<Db>, $ctx?: TraceContext): Promise<void>;
    /**
     * The selected name for this counter.
     */
    private _name;
    /**
     * The collection.
     */
    private readonly _collection;
    /**
     * This function creates a new instance.
     *
     * @param config - A set of config options.
     */
    constructor(config?: ICounterConfig);
    /**
     * This function selects an entry in counters collection.
     *
     * @param name - The name of collection to be selected.
     * @param $ctx - Trace context.
     */
    select(name: string, $ctx?: TraceContext): Promise<void>;
    /**
     * This function generate next count for a specific name.
     *
     * @param name - The name of collection that we should get next count.
     * @param $ctx - Trace context.
     */
    getNextCount(name?: string, $ctx?: TraceContext): Promise<number>;
}
export { Counter, };
