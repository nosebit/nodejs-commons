# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.7](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.6.6...@nosebit/mongo@0.6.7) (2022-04-26)

**Note:** Version bump only for package @nosebit/mongo





## [0.6.6](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.6.5...@nosebit/mongo@0.6.6) (2022-02-09)

**Note:** Version bump only for package @nosebit/mongo





## [0.6.5](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.6.4...@nosebit/mongo@0.6.5) (2022-01-08)

**Note:** Version bump only for package @nosebit/mongo





## [0.6.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.6.3...@nosebit/mongo@0.6.4) (2022-01-07)

**Note:** Version bump only for package @nosebit/mongo





## [0.6.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.6.2...@nosebit/mongo@0.6.3) (2021-11-16)

**Note:** Version bump only for package @nosebit/mongo





## [0.6.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.6.1...@nosebit/mongo@0.6.2) (2021-11-14)


### Bug Fixes

* **mongo:** add _in operators to ISearchQuery interface ([10de353](https://github.com/nosebit/nodejs-commons/commit/10de353d4f0b6942f8a7366035e5735813a2b3d8))





## [0.6.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.6.0...@nosebit/mongo@0.6.1) (2021-10-09)

**Note:** Version bump only for package @nosebit/mongo





# [0.6.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.4.2...@nosebit/mongo@0.6.0) (2021-10-09)


### Features

* **mongo:** allow passing trace context to client sharedInit ([f92fe09](https://github.com/nosebit/nodejs-commons/commit/f92fe09bff08ba9f0d2a045d9ec76afe86870d72))





# [0.5.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.4.2...@nosebit/mongo@0.5.0) (2021-10-09)


### Features

* **mongo:** allow passing trace context to client sharedInit ([f92fe09](https://github.com/nosebit/nodejs-commons/commit/f92fe09bff08ba9f0d2a045d9ec76afe86870d72))





## [0.4.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.4.1...@nosebit/mongo@0.4.2) (2021-10-07)

**Note:** Version bump only for package @nosebit/mongo





## [0.4.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.4.0...@nosebit/mongo@0.4.1) (2021-10-07)

**Note:** Version bump only for package @nosebit/mongo





# [0.4.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.3.0...@nosebit/mongo@0.4.0) (2021-10-07)


### Bug Fixes

* **mongo:** make trace context optional in disconnect function ([df35b63](https://github.com/nosebit/nodejs-commons/commit/df35b63ce45f0fbef7aa819654f273b8805182fb))


### Features

* **mongo:** use tracer instead of logger ([f46f1fd](https://github.com/nosebit/nodejs-commons/commit/f46f1fd1faf70ea2fa8ea1abd16a4e4617dd4b16))





# [0.3.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.2.13...@nosebit/mongo@0.3.0) (2021-09-12)


### Features

* **mongo:** export everything from mongodb lib ([5ac7f3e](https://github.com/nosebit/nodejs-commons/commit/5ac7f3eb5bd8e1b631006d5a345abd744a7e2f0a))





## [0.2.13](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.2.12...@nosebit/mongo@0.2.13) (2021-09-11)

**Note:** Version bump only for package @nosebit/mongo





## [0.2.12](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.2.11...@nosebit/mongo@0.2.12) (2021-07-08)

**Note:** Version bump only for package @nosebit/mongo





## [0.2.11](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.2.10...@nosebit/mongo@0.2.11) (2021-06-12)

**Note:** Version bump only for package @nosebit/mongo





## [0.2.10](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.2.9...@nosebit/mongo@0.2.10) (2021-06-09)

**Note:** Version bump only for package @nosebit/mongo





## [0.2.9](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.2.8...@nosebit/mongo@0.2.9) (2021-06-09)

**Note:** Version bump only for package @nosebit/mongo





## [0.2.8](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.2.7...@nosebit/mongo@0.2.8) (2020-11-11)


### Bug Fixes

* **mongo:** pass useUnifiedTopology to connect method ([8de071b](https://github.com/nosebit/nodejs-commons/commit/8de071bcc875253fd359f0b2af4bdcab48fc0247))





## [0.2.7](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.2.6...@nosebit/mongo@0.2.7) (2020-10-26)


### Bug Fixes

* **mongo:** make lint happy ([bf318a4](https://github.com/nosebit/nodejs-commons/commit/bf318a4c45fcc35480079d76893012a500eadf3f))





## [0.2.6](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.2.5...@nosebit/mongo@0.2.6) (2020-10-15)

**Note:** Version bump only for package @nosebit/mongo





## [0.2.5](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.2.4...@nosebit/mongo@0.2.5) (2020-10-15)

**Note:** Version bump only for package @nosebit/mongo





## [0.2.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.2.3...@nosebit/mongo@0.2.4) (2020-10-14)

**Note:** Version bump only for package @nosebit/mongo





## [0.2.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.2.2...@nosebit/mongo@0.2.3) (2020-10-12)

**Note:** Version bump only for package @nosebit/mongo





## [0.2.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.2.1...@nosebit/mongo@0.2.2) (2020-10-12)

**Note:** Version bump only for package @nosebit/mongo





## [0.2.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.2.0...@nosebit/mongo@0.2.1) (2020-10-10)

**Note:** Version bump only for package @nosebit/mongo





# [0.2.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.1.2...@nosebit/mongo@0.2.0) (2020-09-24)


### Features

* **mongo:** export withId type from mongo client ([4fc40e3](https://github.com/nosebit/nodejs-commons/commit/4fc40e3f119792f05324629040978040d8c721e4))





## [0.1.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.1.1...@nosebit/mongo@0.1.2) (2020-09-12)

**Note:** Version bump only for package @nosebit/mongo





## [0.1.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/mongo@0.1.0...@nosebit/mongo@0.1.1) (2020-05-13)

**Note:** Version bump only for package @nosebit/mongo





# 0.1.0 (2020-04-19)


### Features

* New npm package names removing nodejs-* prefix and adding mongo and thrift mock packages. ([461df10](https://github.com/nosebit/nodejs-commons/commit/461df1099b71f838b046a60f2d232e891a65f666))
