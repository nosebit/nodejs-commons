"use strict";
/* eslint-disable import/prefer-default-export */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RaftPeer = void 0;
//#####################################################
// Imports
//#####################################################
const net_1 = __importDefault(require("net"));
const logger_1 = require("@nosebit/logger");
const utils_1 = require("@nosebit/utils");
const lodash_1 = __importDefault(require("lodash"));
const shortid_1 = __importDefault(require("shortid"));
const uuid_1 = require("uuid");
//#####################################################
// Types
//#####################################################
var IRaftPeerRole;
(function (IRaftPeerRole) {
    IRaftPeerRole["LEADER"] = "leader";
    IRaftPeerRole["CANDIDATE"] = "candidate";
    IRaftPeerRole["FOLLOWER"] = "follower";
})(IRaftPeerRole || (IRaftPeerRole = {}));
//#####################################################
// Main class
//#####################################################
/**
 * This is the main class that implements raft peer functionality.
 */
class RaftPeer {
    /**
     * This function creates a new instance of this class.
     *
     * @param config - A set of config options.
     */
    constructor(config) {
        /**
         * This informs if this peers is already bootstraped.
         */
        this._isBootstraped = false;
        /**
         * The election term.
         */
        this._electionTerm = 0;
        /**
         * The election vote count.
         */
        this._electionVoteCount = 0;
        /**
         * The current role of this peer.
         */
        this._role = IRaftPeerRole.FOLLOWER;
        /**
         * Flag indicating if we want to force leadership.
         */
        this._forceLeadership = false;
        /**
         * A map of all known peers (neighbors) by this peer.
         */
        this._peers = {};
        /**
         * Mark if this peer is in failure mode.
         */
        this._isInFailureMode = false;
        // Get listen hostname and port
        const listenUrlParts = (0, utils_1.getUrlParts)(config.listenHost);
        const DEFAULT_PORT = 7392;
        const HEARTBEAT_TIMEOUT = 2000;
        this._listenHostname = listenUrlParts.hostname || "127.0.0.1";
        this._listenPort = listenUrlParts.port || DEFAULT_PORT;
        // Get advertise hostname and port
        const advertiseUrlParts = config.advertiseHost
            ? (0, utils_1.getUrlParts)(config.advertiseHost)
            : {
                host: `${this._listenHostname}:${this._listenPort}`,
                hostname: listenUrlParts.hostname,
                port: listenUrlParts.port,
            };
        this._advertiseHost = advertiseUrlParts.host;
        this._advertiseHostname = advertiseUrlParts.hostname;
        this._advertisePort = advertiseUrlParts.port;
        // Other peer data.
        this._name = config.name || `P${shortid_1.default.generate()}`;
        this._bootstrapExpect = config.bootstrapExpect || 1;
        // Add itself as a known peer.
        this._peers[this._advertiseHost] = {
            info: {
                hostname: this._advertiseHostname,
                name: this._name,
                port: this._advertisePort,
            },
        };
        // Get random election timeout for this peer.
        this._electionTimeoutRange = Object.assign({ max: 6000, min: 4000 }, (config.electionTimeoutRange || {}));
        // Set heartbeat
        this._heartbeatTimeout = config.heartbeatTimeout || HEARTBEAT_TIMEOUT;
        // Initialize the peer.
        this._init(config);
    }
    /**
     * This function creates a new shared raft peer.
     *
     * @param config - A set of config opts.
     */
    static sharedInit(config) {
        if (!RaftPeer._shared) {
            RaftPeer._shared = new RaftPeer(config);
        }
        return RaftPeer._shared;
    }
    /**
     * This function retrieves the shared raft peer.
     */
    static get shared() {
        return RaftPeer._shared;
    }
    /**
     * This getter retrieves the value of leader host.
     */
    get leaderHost() {
        return this._leaderHost;
    }
    /**
     * This getter retrieves the value of isBootstraped.
     */
    get isBootstraped() {
        return this._isBootstraped;
    }
    /**
     * This function retrieves the isLeader.
     */
    get isLeader() {
        return this._role === IRaftPeerRole.LEADER;
    }
    /**
     * This function initializes this peer.
     *
     * @param config - A set of config options.
     * @param ctx - The log context.
     */
    _init(config, ctx = logger_1.globalCtx) {
        // Create the server
        this._server = net_1.default.createServer((socket) => {
            ctx.logger.info("new peer connected", {
                count: lodash_1.default.size(this._peers),
            });
            // Generate a new socket unique id.
            const id = (0, uuid_1.v4)();
            socket._id = id;
            // Handle messages
            socket.on("data", this._handleEvent.bind(this, socket));
            // Log socket error
            socket.on("error", (error) => {
                ctx.logger.error("peer socket error", error);
            });
        });
        // Set flags
        this._forceLeadership = config.forceLeadership;
        this._server.listen(this._listenPort, this._listenHostname);
        ctx.logger.info(`server listen on "${this._listenHostname}:${this._listenPort}"`);
        /**
         * Auto join
         *
         * @todo : We wait some time before actually trying to join
         * to give some time for remote peer to bootstrap itself when
         * running locally. This would not be required since we
         * implemented a "try" algorithm within join method that keeps
         * retrying to connect when remote peer is not ready yet, but
         * for some reason client peer is logging that it got connected
         * but server peer does not log aknowlege of this connection.
         * Very weird and maybe needs some investigation in the future.
         */
        if (config.joinUrl) {
            const TIMEOUT = 4000;
            setTimeout(() => this.join(config.joinUrl), TIMEOUT);
        }
        // Bootstrap election process if we are there yet.
        this._bootstrapElection();
    }
    /**
     * This function kick off the election process.
     *
     * @param ctx - The log context.
     */
    _bootstrapElection(ctx = logger_1.globalCtx) {
        if (!this._isBootstraped
            && lodash_1.default.size(this._peers) >= this._bootstrapExpect) {
            this._isBootstraped = true;
            ctx.logger.info("bootstrap threshold reached : starting election");
            this._startElectionTimer();
        }
    }
    /**
     * This function starts the election timer.
     *
     * @param ctx - The log context.
     */
    _startElectionTimer(ctx = logger_1.globalCtx) {
        // Generate a new random election timeout.
        const { min, max } = this._electionTimeoutRange;
        const timeout = Math.floor(Math.random() * (max - min + 1)) + min;
        ctx.logger.debug("timeout", timeout);
        // Stop previous timer.
        if (this._electionTimer) {
            ctx.logger.debug("clearing previous timeout");
            clearTimeout(this._electionTimer);
        }
        /**
         * This callback will be called from time to time.
         */
        const electionCallback = () => {
            ctx.logger.info("election timeout reached");
            // Auto elect as leader.
            if (this._bootstrapExpect === 1) {
                this._role = IRaftPeerRole.LEADER;
                this._leaderHost = this._advertiseHost;
                return this._startHeartbeatTimer();
            }
            /**
             * If we reach here is because there isn't a leader (we haven't heard
             * a heartbeat so far) and therefore we need to kick a new election.
             */
            this._role = IRaftPeerRole.CANDIDATE;
            // Start a new election term
            this._electionTerm++;
            // First vote : self vote.
            this._electionVoteCount = 1;
            // Emit a vote request for all connected peers.
            return this._emitToAll("vote:request", {
                term: this._electionTerm,
            });
        };
        // Start the timer.
        this._electionTimer = setTimeout(electionCallback, timeout);
        /**
         * If we set to force leadership then let's call election
         * right away.
         */
        if (this._forceLeadership) {
            electionCallback();
        }
    }
    /**
     * This function starts the heartbeat timer for this peer
     * when it becomes a leader.
     *
     * @param _ctx - The log context.
     */
    _startHeartbeatTimer(_ctx = logger_1.globalCtx) {
        // Stop election timer
        if (this._electionTimer) {
            clearTimeout(this._electionTimer);
            this._electionTimer = null;
        }
        // Start heartbeat interval.
        this._heartbeatTimer = setInterval(() => this._heartbeatLoop(), this._heartbeatTimeout);
        // Run heartbeat for the first time.
        this._heartbeatLoop();
    }
    /**
     * This function runs one loop of heartbeat.
     */
    _heartbeatLoop() {
        /**
         * Emit a heartbeat event to all peers with some info
         * from this leader peer.
         */
        this._emitToAll("heartbeat", {
            leader: {
                hostname: this._advertiseHostname,
                name: this._name,
                port: this._advertisePort,
            },
        });
    }
    /**
     * This function handles the main data event.
     *
     * @param socket - The socket receiving the event.
     * @param data - The data received.
     * @param ctx - The log context.
     */
    _handleEvent(socket, data, ctx = logger_1.globalCtx) {
        // Simulate a failure mode.
        if (this._isInFailureMode) {
            return;
        }
        const messages = data.toString().split("/n");
        for (let i = 0; i < messages.length; i++) {
            // Skip empty messages
            if (messages[i].length === 0) {
                continue;
            }
            try {
                const message = JSON.parse(messages[i]);
                ctx.logger.debug("got message", message);
                switch (message.type) {
                    case "peers": {
                        this._handlePeersEvent(socket, message);
                        break;
                    }
                    case "peer:info": {
                        this._handlePeerInfoEvent(socket, message);
                        break;
                    }
                    case "vote:request": {
                        this._handleVoteRequestEvent(socket, message);
                        break;
                    }
                    case "vote": {
                        this._handleVoteEvent(socket, message);
                        break;
                    }
                    case "heartbeat": {
                        this._handleHeartbeatEvent(socket, message);
                        break;
                    }
                    case "heartbeat:ack": {
                        this._handleHeartbeatAckEvent(socket, message);
                        break;
                    }
                    case "end": {
                        this._handleEndEvent(socket);
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
            catch (error) {
                ctx.logger.error("could not parse message", {
                    error,
                    message: messages[i],
                });
            }
        }
    }
    /**
     * This function handle the event where a remote peer requires
     * info regarding this peer.
     *
     * @param socket - The socket receiving the event.
     * @param message - The message received.
     * @param message.data - Message data.
     * @param ctx - The log context.
     */
    _handlePeerInfoEvent(socket, message, ctx = logger_1.globalCtx) {
        ctx.logger.debug("remote peer info", message.data);
        const remotePeerHost = `${message.data.hostname}:${message.data.port}`;
        // Set peer id within socket
        socket._peerHost = remotePeerHost;
        socket._peerName = message.data.name;
        // Add info to peers map.
        this._peers[remotePeerHost] = {
            info: message.data,
            socket,
        };
        // Propagate the peers list to all connected peers.
        const peerHosts = Object.keys(this._peers);
        const peerInfos = [];
        for (let i = 0; i < peerHosts.length; i++) {
            const peerHost = peerHosts[i];
            const peer = this._peers[peerHost];
            if (peer.info) {
                peerInfos.push(peer.info);
            }
        }
        ctx.logger.debug("new peers info", peerInfos);
        this._emitToAll("peers", peerInfos);
        // Check if we reach expected bootstrap peers count
        this._bootstrapElection();
    }
    /**
     * This function handles peers event which is responsible to
     * propagate known peers to all peers.
     *
     * @param _socket - The socket receiving the event.
     * @param message - The message received.
     * @param message.data - Message data.
     * @param ctx - The log context.
     */
    _handlePeersEvent(_socket, message, ctx = logger_1.globalCtx) {
        // Conciliate data.
        //
        // @TODO : What about consensus here?
        for (let i = 0; i < message.data.length; i++) {
            const { name, hostname, port, isLeader, } = message.data[i];
            const host = `${hostname}:${port}`;
            const ourPeer = this._peers[host];
            // If peerInfo is about this peer, then move on.
            if (host === this._advertiseHost) {
                ctx.logger.debug("self peer info : skipping");
                continue;
            }
            // If we already have peer info.
            if (ourPeer) {
                // Add info in case it's not there yet (because of join).
                ourPeer.info = ourPeer.info || {
                    hostname,
                    isLeader,
                    name,
                    port,
                };
                /**
                 * All peers except from this one have socket info and therefore
                 * since we already discarted this own peer in previous "if" clause
                 * we are sure that ourPeer have socket property.
                 */
                ourPeer.socket._peerHost = host;
                ourPeer.socket._peerName = name;
                // New leader.
                if (!this._leaderHost && isLeader) {
                    this._leaderHost = host;
                    this._startElectionTimer();
                }
                ctx.logger.debug("peer info already here : skipping");
                continue;
            }
            ctx.logger.debug("peer info not here yet");
            this.join(host, ctx);
        }
    }
    /**
     * This function handles a vote request event comming from some peer
     * that it's current a candidate to become a leader.
     *
     * @param socket - The socket receiving the event.
     * @param message - The message received.
     * @param message.data - Message data.
     * @param message.data.term - Message data term.
     * @param ctx - The log context.
     */
    _handleVoteRequestEvent(socket, message, ctx = logger_1.globalCtx) {
        ctx.logger.debug("data", {
            remoteTermo: message.data.term,
            term: this._electionTerm,
        });
        /**
         * We just process messages comming from peers that has higher
         * election term.
         */
        if (message.data.term <= this._electionTerm) {
            return;
        }
        // Reset election timer.
        this._startElectionTimer();
        /**
         * Set our term to match candidate peer term so that we know
         * a vote for that term was sent by us.
         */
        this._electionTerm = message.data.term;
        // Send a vote to peer.
        this._emitTo(socket, "vote");
    }
    /**
     * This function handles a vote request comming from neighbor peer.
     *
     * @param socket - The socket receiving the event.
     * @param message - The message received.
     * @param ctx - The log context.
     */
    _handleVoteEvent(socket, message, ctx = logger_1.globalCtx) {
        ctx.logger.debug("data", {
            message,
            role: this._role,
        });
        // If we are not a candidate then skip.
        if (this._role !== IRaftPeerRole.CANDIDATE) {
            ctx.logger.debug("peer is not a candidate");
            return;
        }
        // Increment the vote count.
        this._electionVoteCount++;
        ctx.logger.debug(`vote from ${socket._peerName}`, {
            voteCount: this._electionVoteCount,
        });
        // Evaluate the majority.
        const majority = Math.floor(lodash_1.default.size(this._peers) / 2) + 1;
        // If we reach a majoroty of votes, then turn this peer the leader.
        if (this._electionVoteCount >= majority) {
            ctx.logger.info("voted as leader");
            this._role = IRaftPeerRole.LEADER;
            // We are the leader now.
            this._leaderHost = this._advertiseHost;
            // Start the heartbeat timer.
            this._startHeartbeatTimer();
        }
        else {
            ctx.logger.debug("vote do NOT reach majaroty yet");
            // If we not reached the majority, then let's try again.
            this._startElectionTimer();
        }
    }
    /**
     * This function handle a heartbeat event.
     *
     * @param socket - The socket receiving the event.
     * @param message - The message received.
     * @param message.data - Message data.
     * @param message.data.leader - Current leader.
     * @param ctx - The log context.
     */
    _handleHeartbeatEvent(socket, message, ctx = logger_1.globalCtx) {
        ctx.logger.debug("data", {
            message,
            role: this._role,
        });
        // Heartbeat matters only to followers.
        if (this._role !== IRaftPeerRole.FOLLOWER) {
            ctx.logger.debug("peer is not a follower");
            return;
        }
        // Restart election timer.
        this._startElectionTimer();
        // Setup leader host.
        const { leader } = message.data;
        const leaderHost = `${leader.hostname}:${leader.port}`;
        ctx.logger.debug("leader info", {
            currentLeaderHost: leaderHost,
            leader,
        });
        // Leader host changed for some (weird?) reason.
        if (this._leaderHost && this._leaderHost !== leaderHost) {
            ctx.logger.debug("current leader changed");
            lodash_1.default.set(this._peers[this._leaderHost], `info.isLeader`, false);
        }
        // Set a leader flag withing peers info.
        lodash_1.default.set(this._peers[leaderHost], `info.isLeader`, true);
        // Update leader host.
        this._leaderHost = leaderHost;
        // Emit heartbeat ack.
        this._emitTo(socket, "heartbeat:ack");
    }
    /**
     * This function handle a heartbeat event.
     *
     * @param _socket - The socket receiving the event.
     * @param message - The message received.
     * @param ctx - The log context.
     */
    _handleHeartbeatAckEvent(_socket, message, ctx = logger_1.globalCtx) {
        ctx.logger.debug("data", {
            message,
            role: this._role,
        });
        // Heartbeat ack matters only to leaders.
        if (this._role !== IRaftPeerRole.LEADER) {
            ctx.logger.debug("peer is not a leader : skipping");
        }
    }
    /**
     * This function handles the case where socket was closed by
     * remote peer.
     *
     * @param socket - The socket receiving the event.
     */
    _handleEndEvent(socket) {
        this._peers[socket._peerHost].socket = null;
        socket.destroy();
    }
    /**
     * This function emits an event to a particular peer.
     *
     * @param socket - The socket receiving the event.
     * @param evtName - The name of event to be emitted.
     * @param evtData - Data to be emitted.
     * @param ctx - The log context.
     */
    _emitTo(socket, evtName, evtData, ctx = logger_1.globalCtx) {
        let message;
        // Try to build up a message.
        try {
            // Convert message body to string
            message = JSON.stringify({
                data: evtData || {},
                type: evtName,
            });
            // Add message termination mark.
            message += "/n";
            ctx.logger.debug("message string built", message);
        }
        catch (error) {
            return ctx.logger.error("could not build the message");
        }
        /**
         * Try to send message (write rises an error when remote
         * peer closes the connection).
         */
        try {
            socket.write(message, () => {
                ctx.logger.debug("message emitted", message);
            });
        }
        catch (error) {
            ctx.logger.error("could not write message", error);
        }
        return null;
    }
    /**
     * This function emits an event to all connected peers.
     *
     * @param evtName - The name of event to be emitted.
     * @param evtData - Data to be emitted.
     */
    _emitToAll(evtName, evtData) {
        const ids = Object.keys(this._peers);
        for (let i = 0; i < ids.length; i++) {
            const { socket } = this._peers[ids[i]];
            if (socket) {
                this._emitTo(socket, evtName, evtData);
            }
        }
    }
    /**
     * This function join this peer to another one.
     *
     * @param url - The peer url to join with.
     * @param ctx - The log context.
     */
    join(url, ctx = logger_1.globalCtx) {
        ctx.logger.debug("url", url);
        const urlParts = (0, utils_1.getUrlParts)(url);
        let connectTryCount = 0;
        // Create socket
        const socket = new net_1.default.Socket();
        // Setup socket metadata
        socket._id = (0, uuid_1.v4)();
        socket._peerHost = urlParts.host;
        /**
         * Try to connect function.
         */
        const tryToConnect = () => {
            ctx.logger.debug(`try ${connectTryCount + 1}`);
            try {
                socket.connect(urlParts.port, urlParts.hostname);
            }
            catch (error) {
                const MAX_TRIES = 10;
                const TIMEOUT = 2000;
                ctx.logger.error(`could not connect to ${url}`);
                connectTryCount++;
                // Keep trying to join for a number of times.
                if (connectTryCount < MAX_TRIES) {
                    setTimeout(tryToConnect.bind(this), TIMEOUT);
                }
                else {
                    throw new Error(`could not join after ${connectTryCount} times`);
                }
            }
        };
        // Socket event handlers.
        socket.on("connect", () => {
            ctx.logger.info("connected to remote peer");
            connectTryCount = 0;
            // Mark that we successfully connected to peer.
            this._peers[socket._peerHost] = {
                socket,
            };
            // Inform to remote peer who i am.
            this._emitTo(socket, "peer:info", {
                hostname: this._advertiseHostname,
                name: this._name,
                port: this._advertisePort,
            });
            // Check if we reach expected bootstrap peers count
            this._bootstrapElection();
        });
        // Bind handler for "data" event.
        socket.on("data", this._handleEvent.bind(this, socket));
        // On error just log.
        //
        // @TODO : We should do something smartter here.
        socket.on("error", (error) => {
            ctx.logger.error("socket error", error);
        });
        /**
         * @description : For some weird reason after the first connect
         * the following two event are called and therefore server is
         * "closing" the connection although it keeps sending events
         * normally. If we don't set keep alive mode above we get
         * connected and then stop to receive any connections. I'm
         * suspecting this is some sort of bug on NodeJS since it
         * should not emit a "close" event just after connect the socket.
         *
         * Node version = 8.11.1.
         */
        socket.on("end", () => {
            ctx.logger.info("socket ended");
        });
        socket.on("close", () => {
            ctx.logger.info("socket closed");
        });
        // Start trying to join.
        tryToConnect();
    }
    /**
     * This function prints all known peers of this peer.
     *
     * @param ctx - The log context.
     */
    printPeers(ctx = logger_1.globalCtx) {
        ctx.logger.info("peers", Object.keys(this._peers));
    }
    /**
     * This function simulates a failure for this peer.
     *
     * @param _ctx - The log context.
     */
    simulateFailure(_ctx = logger_1.globalCtx) {
        // Stop all timers.
        clearTimeout(this._electionTimer);
        clearInterval(this._heartbeatTimer);
        // Close the server socket.
        this._server.close();
        // Set failure flag.
        this._isInFailureMode = true;
    }
}
__decorate([
    (0, logger_1.log)()
], RaftPeer.prototype, "_init", null);
__decorate([
    (0, logger_1.log)()
], RaftPeer.prototype, "_bootstrapElection", null);
__decorate([
    (0, logger_1.log)({
        preventTracing: true,
    })
], RaftPeer.prototype, "_startElectionTimer", null);
__decorate([
    (0, logger_1.log)({
        preventTracing: true,
    })
], RaftPeer.prototype, "_startHeartbeatTimer", null);
__decorate([
    (0, logger_1.log)({
        preventTracing: true,
    })
], RaftPeer.prototype, "_handlePeerInfoEvent", null);
__decorate([
    (0, logger_1.log)({
        argsToLog: ["_", "message"],
        preventTracing: true,
    })
], RaftPeer.prototype, "_handlePeersEvent", null);
__decorate([
    (0, logger_1.log)({
        argsToLog: ["_", "message"],
        preventTracing: true,
    })
], RaftPeer.prototype, "_handleVoteRequestEvent", null);
__decorate([
    (0, logger_1.log)({
        argsToLog: ["_", "message"],
        preventTracing: true,
    })
], RaftPeer.prototype, "_handleVoteEvent", null);
__decorate([
    (0, logger_1.log)({
        argsToLog: ["_", "message"],
        preventTracing: true,
    })
], RaftPeer.prototype, "_handleHeartbeatEvent", null);
__decorate([
    (0, logger_1.log)({
        preventTracing: true,
    })
], RaftPeer.prototype, "_handleHeartbeatAckEvent", null);
__decorate([
    (0, logger_1.log)({
        argsToLog: ["_", "evtName", "evtData"],
        preventTracing: true,
    })
], RaftPeer.prototype, "_emitTo", null);
__decorate([
    (0, logger_1.log)({
        argsToLog: ["url"],
    })
], RaftPeer.prototype, "join", null);
__decorate([
    (0, logger_1.log)()
], RaftPeer.prototype, "printPeers", null);
__decorate([
    (0, logger_1.log)()
], RaftPeer.prototype, "simulateFailure", null);
exports.RaftPeer = RaftPeer;
//# sourceMappingURL=index.js.map