import { LogContext } from "@nosebit/logger";
interface IRange {
    min?: number;
    max?: number;
}
interface IRaftPeerConfig {
    bootstrapExpect?: number;
    electionTimeoutRange?: IRange;
    forceLeadership?: boolean;
    heartbeatTimeout?: number;
    listenHost: string;
    advertiseHost?: string;
    hostname?: string;
    joinUrl?: string;
    name?: string;
    neighbors?: string[];
    url?: string;
}
/**
 * This is the main class that implements raft peer functionality.
 */
declare class RaftPeer {
    /**
     * Shared instance.
     */
    private static _shared;
    /**
     * This function creates a new shared raft peer.
     *
     * @param config - A set of config opts.
     */
    static sharedInit(config: IRaftPeerConfig): RaftPeer;
    /**
     * This function retrieves the shared raft peer.
     */
    static get shared(): RaftPeer;
    /**
     * This is the hostname to listen for connections.
     */
    private readonly _listenHostname;
    /**
     * This is the port to listen for connections.
     */
    private readonly _listenPort;
    /**
     * This is the host (hostname:port) to advertise to other peers.
     */
    private readonly _advertiseHost;
    /**
     * This is the hostname to advertise to other peers.
     */
    private readonly _advertiseHostname;
    /**
     * This is the port to advertise to other peers.
     */
    private readonly _advertisePort;
    /**
     * The name of this peer.
     */
    private readonly _name;
    /**
     * Wait for a this certain number of peers to be connected so we can
     * start leader election.
     */
    private readonly _bootstrapExpect;
    /**
     * This informs if this peers is already bootstraped.
     */
    private _isBootstraped;
    /**
     * The timeout range for this peer to start the election process.
     */
    private readonly _electionTimeoutRange;
    /**
     * The election term.
     */
    private _electionTerm;
    /**
     * The election vote count.
     */
    private _electionVoteCount;
    /**
     * This is the election timer interval.
     */
    private _electionTimer;
    /**
     * This is the leader host.
     */
    private _leaderHost;
    /**
     * The timeout for this peer to send hertbeat when it's the leader.
     */
    private readonly _heartbeatTimeout;
    /**
     * This is the heartbeat timer interval when this peer is the leader.
     */
    private _heartbeatTimer;
    /**
     * The current role of this peer.
     */
    private _role;
    /**
     * The server instance that it's listening for client sockets.
     */
    private _server;
    /**
     * Flag indicating if we want to force leadership.
     */
    private _forceLeadership;
    /**
     * A map of all known peers (neighbors) by this peer.
     */
    private readonly _peers;
    /**
     * Mark if this peer is in failure mode.
     */
    private _isInFailureMode;
    /**
     * This getter retrieves the value of leader host.
     */
    get leaderHost(): string;
    /**
     * This getter retrieves the value of isBootstraped.
     */
    get isBootstraped(): boolean;
    /**
     * This function retrieves the isLeader.
     */
    get isLeader(): boolean;
    /**
     * This function creates a new instance of this class.
     *
     * @param config - A set of config options.
     */
    constructor(config: IRaftPeerConfig);
    /**
     * This function initializes this peer.
     *
     * @param config - A set of config options.
     * @param ctx - The log context.
     */
    private _init;
    /**
     * This function kick off the election process.
     *
     * @param ctx - The log context.
     */
    private _bootstrapElection;
    /**
     * This function starts the election timer.
     *
     * @param ctx - The log context.
     */
    private _startElectionTimer;
    /**
     * This function starts the heartbeat timer for this peer
     * when it becomes a leader.
     *
     * @param _ctx - The log context.
     */
    private _startHeartbeatTimer;
    /**
     * This function runs one loop of heartbeat.
     */
    private _heartbeatLoop;
    /**
     * This function handles the main data event.
     *
     * @param socket - The socket receiving the event.
     * @param data - The data received.
     * @param ctx - The log context.
     */
    private _handleEvent;
    /**
     * This function handle the event where a remote peer requires
     * info regarding this peer.
     *
     * @param socket - The socket receiving the event.
     * @param message - The message received.
     * @param message.data - Message data.
     * @param ctx - The log context.
     */
    private _handlePeerInfoEvent;
    /**
     * This function handles peers event which is responsible to
     * propagate known peers to all peers.
     *
     * @param _socket - The socket receiving the event.
     * @param message - The message received.
     * @param message.data - Message data.
     * @param ctx - The log context.
     */
    private _handlePeersEvent;
    /**
     * This function handles a vote request event comming from some peer
     * that it's current a candidate to become a leader.
     *
     * @param socket - The socket receiving the event.
     * @param message - The message received.
     * @param message.data - Message data.
     * @param message.data.term - Message data term.
     * @param ctx - The log context.
     */
    private _handleVoteRequestEvent;
    /**
     * This function handles a vote request comming from neighbor peer.
     *
     * @param socket - The socket receiving the event.
     * @param message - The message received.
     * @param ctx - The log context.
     */
    private _handleVoteEvent;
    /**
     * This function handle a heartbeat event.
     *
     * @param socket - The socket receiving the event.
     * @param message - The message received.
     * @param message.data - Message data.
     * @param message.data.leader - Current leader.
     * @param ctx - The log context.
     */
    private _handleHeartbeatEvent;
    /**
     * This function handle a heartbeat event.
     *
     * @param _socket - The socket receiving the event.
     * @param message - The message received.
     * @param ctx - The log context.
     */
    private _handleHeartbeatAckEvent;
    /**
     * This function handles the case where socket was closed by
     * remote peer.
     *
     * @param socket - The socket receiving the event.
     */
    private _handleEndEvent;
    /**
     * This function emits an event to a particular peer.
     *
     * @param socket - The socket receiving the event.
     * @param evtName - The name of event to be emitted.
     * @param evtData - Data to be emitted.
     * @param ctx - The log context.
     */
    private _emitTo;
    /**
     * This function emits an event to all connected peers.
     *
     * @param evtName - The name of event to be emitted.
     * @param evtData - Data to be emitted.
     */
    private _emitToAll;
    /**
     * This function join this peer to another one.
     *
     * @param url - The peer url to join with.
     * @param ctx - The log context.
     */
    join(url: string, ctx?: LogContext): void;
    /**
     * This function prints all known peers of this peer.
     *
     * @param ctx - The log context.
     */
    printPeers(ctx?: LogContext): void;
    /**
     * This function simulates a failure for this peer.
     *
     * @param _ctx - The log context.
     */
    simulateFailure(_ctx?: LogContext): void;
}
export { RaftPeer, };
