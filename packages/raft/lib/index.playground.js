"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = require("@nosebit/logger");
const index_1 = require("./index");
// Initialize common modules
logger_1.Logger.sharedInit({
    level: "debug",
    serviceName: "raft-test",
});
// Create 3 peers
const p1 = new index_1.RaftPeer({
    bootstrapExpect: 3,
    listenHost: "127.0.0.1:9123",
    name: "P1",
});
const p2 = new index_1.RaftPeer({
    bootstrapExpect: 3,
    joinUrl: "127.0.0.1:9123",
    listenHost: "127.0.0.1:9124",
    name: "P2",
});
const p3 = new index_1.RaftPeer({
    bootstrapExpect: 3,
    joinUrl: "127.0.0.1:9123",
    listenHost: "127.0.0.1:9125",
    name: "P3",
});
// Name to peer map
const hostToPeer = {
    "127.0.0.1:9123": p1,
    "127.0.0.1:9124": p2,
    "127.0.0.1:9125": p3,
};
const TIMEOUT = 30000;
// After some time simulate a failure on leader
setTimeout(() => {
    const { leaderHost } = p1;
    const leader = hostToPeer[leaderHost];
    leader.simulateFailure();
}, TIMEOUT);
//# sourceMappingURL=index.playground.js.map