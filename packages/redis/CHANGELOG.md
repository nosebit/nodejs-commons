# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.9](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.2.8...@nosebit/redis@0.2.9) (2022-04-26)

**Note:** Version bump only for package @nosebit/redis





## [0.2.8](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.2.7...@nosebit/redis@0.2.8) (2022-02-09)

**Note:** Version bump only for package @nosebit/redis





## [0.2.7](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.2.6...@nosebit/redis@0.2.7) (2022-01-07)

**Note:** Version bump only for package @nosebit/redis





## [0.2.6](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.2.5...@nosebit/redis@0.2.6) (2021-11-14)

**Note:** Version bump only for package @nosebit/redis





## [0.2.5](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.2.4...@nosebit/redis@0.2.5) (2021-10-09)

**Note:** Version bump only for package @nosebit/redis





## [0.2.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.2.2...@nosebit/redis@0.2.4) (2021-10-09)

**Note:** Version bump only for package @nosebit/redis





## [0.2.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.2.2...@nosebit/redis@0.2.3) (2021-10-09)

**Note:** Version bump only for package @nosebit/redis





## [0.2.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.2.1...@nosebit/redis@0.2.2) (2021-10-07)

**Note:** Version bump only for package @nosebit/redis





## [0.2.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.2.0...@nosebit/redis@0.2.1) (2021-10-07)

**Note:** Version bump only for package @nosebit/redis





# [0.2.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.1.15...@nosebit/redis@0.2.0) (2021-10-07)


### Features

* **redis:** use tracer instead of logger ([96e812d](https://github.com/nosebit/nodejs-commons/commit/96e812d1c454484ae53a699184f4f2c503530e4a))





## [0.1.15](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.1.14...@nosebit/redis@0.1.15) (2021-09-12)

**Note:** Version bump only for package @nosebit/redis





## [0.1.14](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.1.13...@nosebit/redis@0.1.14) (2021-09-11)

**Note:** Version bump only for package @nosebit/redis





## [0.1.13](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.1.12...@nosebit/redis@0.1.13) (2021-07-08)

**Note:** Version bump only for package @nosebit/redis





## [0.1.12](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.1.11...@nosebit/redis@0.1.12) (2021-06-12)

**Note:** Version bump only for package @nosebit/redis





## [0.1.11](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.1.10...@nosebit/redis@0.1.11) (2021-06-09)

**Note:** Version bump only for package @nosebit/redis





## [0.1.10](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.1.9...@nosebit/redis@0.1.10) (2020-10-26)

**Note:** Version bump only for package @nosebit/redis





## [0.1.9](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.1.8...@nosebit/redis@0.1.9) (2020-10-15)

**Note:** Version bump only for package @nosebit/redis





## [0.1.8](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.1.7...@nosebit/redis@0.1.8) (2020-10-15)

**Note:** Version bump only for package @nosebit/redis





## [0.1.7](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.1.6...@nosebit/redis@0.1.7) (2020-10-14)

**Note:** Version bump only for package @nosebit/redis





## [0.1.6](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.1.5...@nosebit/redis@0.1.6) (2020-10-12)

**Note:** Version bump only for package @nosebit/redis





## [0.1.5](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.1.4...@nosebit/redis@0.1.5) (2020-10-12)

**Note:** Version bump only for package @nosebit/redis





## [0.1.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.1.3...@nosebit/redis@0.1.4) (2020-10-10)

**Note:** Version bump only for package @nosebit/redis





## [0.1.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.1.2...@nosebit/redis@0.1.3) (2020-09-24)

**Note:** Version bump only for package @nosebit/redis





## [0.1.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.1.1...@nosebit/redis@0.1.2) (2020-09-12)

**Note:** Version bump only for package @nosebit/redis





## [0.1.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/redis@0.1.0...@nosebit/redis@0.1.1) (2020-05-13)

**Note:** Version bump only for package @nosebit/redis





# 0.1.0 (2020-04-19)


### Features

* New npm package names removing nodejs-* prefix and adding mongo and thrift mock packages. ([461df10](https://github.com/nosebit/nodejs-commons/commit/461df1099b71f838b046a60f2d232e891a65f666))
