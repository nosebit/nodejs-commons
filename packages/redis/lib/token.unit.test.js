"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//#####################################################
// Imports
//#####################################################
require("jest");
const tracer_1 = require("@nosebit/tracer");
const token_1 = __importDefault(require("./token"));
//#####################################################
// Constants
//#####################################################
const tokenMock = "tokenMock";
const dataMock = { test: "hello" };
//#####################################################
// Local variables
//#####################################################
let config;
let jwtClientMock;
let redisClientMock;
let manager;
//#####################################################
// Utilitary methods
//#####################################################
/**
 * This function generates both a fakeRedisClient and a token manager.
 *
 * @param args - A set of args.
 * @param args.redisClientMock - A mock client for redis.
 * @param args.jwtClientMock - Mock for jwt client.
 * @param args.config - A set of config options.
 */
function createManager(args = {}) {
    jwtClientMock = Object.assign({ sign: jest.fn((_payload, _secretOrPvtKey, _options, callback) => callback(null, tokenMock)), verify: jest.fn((_token, _secret, callback) => callback(null, dataMock)) }, (args.jwtClientMock || {}));
    redisClientMock = Object.assign({ expire: jest.fn(), get: jest.fn((_key) => Promise.resolve()), initialized: Promise.resolve(), set: jest.fn((_key, _val, _expTimeInSec) => Promise.resolve("")) }, (args.redisClientMock || {}));
    const SEC_PER_MIN = 60;
    const MIN_PER_HOUR = 60;
    const HOUR_PER_DAY = 24;
    config = Object.assign({ expiration: HOUR_PER_DAY * MIN_PER_HOUR * SEC_PER_MIN, jwtClient: jwtClientMock, redisClient: redisClientMock, redisIdx: 1, secret: "shhhhhhhhhhhhhh", type: "token" }, (args.config || {}));
    manager = new token_1.default(config);
}
//#####################################################
// Test definitions
//#####################################################
describe("token manager", () => {
    beforeEach(() => {
        createManager();
    });
    describe("create method", () => {
        it("should call jwt.sign with correct arguments", () => __awaiter(void 0, void 0, void 0, function* () {
            const token = yield manager.create(dataMock);
            expect(jwtClientMock.sign).toHaveBeenCalledWith(dataMock, config.secret, {
                expiresIn: config.expiration,
            }, expect.any(Function));
            expect(token).toBe(tokenMock);
        }));
    });
    describe("decode method", () => {
        it("should call jwt and redis methods with correct arguments", () => __awaiter(void 0, void 0, void 0, function* () {
            const keyMock = `${config.type}_${tokenMock}`;
            const decoded = yield manager.decode(tokenMock);
            expect(jwtClientMock.verify).toHaveBeenCalledWith(tokenMock, config.secret, expect.any(Function));
            expect(redisClientMock.get).toHaveBeenCalledWith(keyMock, expect.any(tracer_1.TraceContext));
            expect(decoded).toBe(dataMock);
        }));
        it("should fails when jwt.verify fails", () => __awaiter(void 0, void 0, void 0, function* () {
            const errorMsg = "some error";
            let decoded;
            let error;
            /**
             * This function mocks the jwt verify method simulating
             * a failure.
             *
             * @param _token - The token to be verified.
             * @param _secret - The public secret.
             * @param callback - The callback.
             */
            const jwtVerifyMock = (_token, _secret, callback) => callback(new Error(errorMsg), null);
            // Create the manager with mock
            createManager({
                jwtClientMock: {
                    verify: jest.fn(jwtVerifyMock),
                },
            });
            try {
                decoded = yield manager.decode(tokenMock);
            }
            catch (catchedError) {
                error = catchedError;
            }
            expect(jwtClientMock.verify).toHaveBeenCalledWith(tokenMock, config.secret, expect.any(Function));
            expect(redisClientMock.get).not.toHaveBeenCalled();
            expect(decoded).toBeUndefined();
            expect(error).toBeDefined();
            expect(error.message).toBe(errorMsg);
        }));
        it("should fails when redisClient.get fails", () => __awaiter(void 0, void 0, void 0, function* () {
            const keyMock = `${config.type}_${tokenMock}`;
            const errorMsg = "some error";
            /**
             * This function is a mock for redis get function that
             * going to simulate a fail.
             *
             * @param _key - The key to be stored.
             * @param callback - The callback function.
             */
            const redisGetMock = (_key) => __awaiter(void 0, void 0, void 0, function* () { return Promise.reject(new Error(errorMsg)); });
            // Create the manager with mock
            createManager({
                redisClientMock: {
                    get: jest.fn(redisGetMock),
                },
            });
            let decoded;
            let error;
            try {
                decoded = yield manager.decode(tokenMock);
            }
            catch (catchedError) {
                error = catchedError;
            }
            expect(jwtClientMock.verify).toHaveBeenCalledWith(tokenMock, config.secret, expect.any(Function));
            expect(redisClientMock.get).toHaveBeenCalledWith(keyMock, expect.any(tracer_1.TraceContext));
            expect(decoded).toBeUndefined();
            expect(error).toBeDefined();
            expect(error.message).toBe(errorMsg);
        }));
    });
    describe("expire method", () => {
        it("should call jwt and redis methods with correct arguments", () => __awaiter(void 0, void 0, void 0, function* () {
            const keyMock = `${config.type}_${tokenMock}`;
            yield manager.expire(tokenMock);
            expect(jwtClientMock.verify).toHaveBeenCalledWith(tokenMock, config.secret, expect.any(Function));
            expect(redisClientMock.set).toHaveBeenCalledWith(keyMock, expect.any(String), 0, expect.any(tracer_1.TraceContext));
            expect(redisClientMock.expire).toHaveBeenCalledWith(tokenMock, config.expiration);
        }));
        it("should success when jwt.verify fails with TokenExpiredError", () => __awaiter(void 0, void 0, void 0, function* () {
            const errorMsg = "TokenExpiredError";
            let error;
            // Create the manager with mock
            createManager({
                jwtClientMock: {
                    verify: jest.fn((_token, _secret, callback) => callback({ name: errorMsg }, null)),
                },
            });
            try {
                yield manager.expire(tokenMock);
            }
            catch (catchedError) {
                error = catchedError;
            }
            expect(jwtClientMock.verify).toHaveBeenCalledWith(tokenMock, config.secret, expect.any(Function));
            expect(redisClientMock.set).not.toHaveBeenCalled();
            expect(redisClientMock.expire).not.toHaveBeenCalled();
            expect(error).toBeUndefined();
        }));
        it("should fails when jwt.verify fails", () => __awaiter(void 0, void 0, void 0, function* () {
            const errorMsg = "some error";
            let error;
            // Create the manager with mock
            createManager({
                jwtClientMock: {
                    verify: jest.fn((_token, _secret, callback) => callback(new Error(errorMsg), null)),
                },
            });
            try {
                yield manager.expire(tokenMock);
            }
            catch (catchedError) {
                error = catchedError;
            }
            expect(jwtClientMock.verify).toHaveBeenCalledWith(tokenMock, config.secret, expect.any(Function));
            expect(redisClientMock.set).not.toHaveBeenCalled();
            expect(redisClientMock.expire).not.toHaveBeenCalled();
            expect(error).toBeDefined();
            expect(error.message).toBe(errorMsg);
        }));
        it("should fails when redisClient.set fails", () => __awaiter(void 0, void 0, void 0, function* () {
            const keyMock = `${config.type}_${tokenMock}`;
            const errorMsg = "some error";
            let error;
            createManager({
                redisClientMock: {
                    set: jest.fn((_key, _data, _expTimeInSec) => Promise.reject(new Error(errorMsg))),
                },
            });
            try {
                yield manager.expire(tokenMock);
            }
            catch (catchedError) {
                error = catchedError;
            }
            expect(jwtClientMock.verify).toHaveBeenCalledWith(tokenMock, config.secret, expect.any(Function));
            expect(redisClientMock.set).toHaveBeenCalledWith(keyMock, expect.any(String), 0, expect.any(tracer_1.TraceContext));
            expect(redisClientMock.expire).not.toHaveBeenCalled();
            expect(error).toBeDefined();
            expect(error.message).toBe(errorMsg);
        }));
    });
});
//# sourceMappingURL=token.unit.test.js.map