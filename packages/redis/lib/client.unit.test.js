"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
require("jest");
const client_1 = require("./client");
//#####################################################
// Local variables
//#####################################################
let mockedRedisDriver;
let redisClient;
//#####################################################
// Constants
//#####################################################
const mockedKey = "fooKey";
const mockedValue = "fooValue";
//#####################################################
// Test definitions
//#####################################################
beforeEach(() => {
    mockedRedisDriver = {
        expire: jest.fn((_k, _t, callback) => {
            callback(null, "OK");
        }),
        get: jest.fn((_k, callback) => {
            /* We must call the callback with the stringified value because
            it will be parsed inside this callback before being returned. */
            callback(null, JSON.stringify(mockedValue));
        }),
        quit: jest.fn((callback) => {
            callback(null, "OK");
        }),
        set: jest.fn((_k, data, callback) => {
            /* The data is parsed here because redisDriver set method firstly stringifies it and
            then call this mocked set method. To prevent parsing the result on the test implementation,
            it's better and faster to call the callback with the already parsed value. */
            callback(null, JSON.parse(data));
        }),
    };
    const config = {
        _redisDriver: mockedRedisDriver,
    };
    redisClient = new client_1.RedisClient(config);
});
describe("Redis driver test", () => {
    it("should call set method correctly", () => __awaiter(void 0, void 0, void 0, function* () {
        const value = yield redisClient.set(mockedKey, mockedValue);
        expect(value).toBe(mockedValue);
        expect(mockedRedisDriver.set).toHaveBeenCalled();
    }));
    it("should call get method correctly", () => __awaiter(void 0, void 0, void 0, function* () {
        const value = yield redisClient.get(mockedKey);
        expect(value).toBe(mockedValue);
        expect(mockedRedisDriver.get).toHaveBeenCalled();
    }));
    it("should call expire method correctly", () => __awaiter(void 0, void 0, void 0, function* () {
        const value = yield redisClient.expire(mockedKey, 1000); //eslint-disable-line
        expect(value).toBe("OK");
        expect(mockedRedisDriver.expire).toHaveBeenCalled();
    }));
    it("should call quit method correctly", () => __awaiter(void 0, void 0, void 0, function* () {
        const value = yield redisClient.disconnect();
        expect(value).toBe("OK");
        expect(mockedRedisDriver.quit).toHaveBeenCalled();
    }));
});
//# sourceMappingURL=client.unit.test.js.map