"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RedisClient = void 0;
const tracer_1 = require("@nosebit/tracer");
const utils_1 = require("@nosebit/utils");
const ioredis_1 = __importDefault(require("ioredis"));
//#####################################################
// Main class
//#####################################################
/**
 * This class implements a basic wrapper around mongo driver.
 */
class RedisClient {
    /**
     *
     * @param config - A set of config options to be passed down to redis client.
     */
    constructor(config) {
        this._namespace = config.namespace || "";
        this._initialized = this.init(config);
    }
    /**
     * This function either retrieves a redis client, if it's already created, or initializes one (uses the Singleton pattern).
     *
     * @param config - Options passed down to the redis client, used to create it with a particular configuration.
     */
    static sharedInit(config) {
        if (!RedisClient._shared) {
            RedisClient._shared = new RedisClient(config);
        }
        return RedisClient._shared;
    }
    /**
     * Get shared driver.
     */
    static get shared() {
        if (!RedisClient._shared) {
            throw new Error("redis shared client not initialized");
        }
        return RedisClient._shared;
    }
    /**
     * Check if shared instance exists.
     */
    static sharedExists() {
        return Boolean(RedisClient._shared);
    }
    /**
     * This function returns the instance of the client.
     */
    get driver() {
        return this._driver;
    }
    /**
     * This function returns the initialized attribute.
     */
    get initialized() {
        return this._initialized;
    }
    /**
     * This method puts the namespace prefix before the key and returns it.
     *
     * @param key - The key to be inserted.
     */
    _getNamespacedKey(key) {
        return this._namespace
            ? `${this._namespace}_${key}`
            : key;
    }
    /**
     *
     * @param config - A set of config options.
     */
    init(config) {
        return __awaiter(this, void 0, void 0, function* () {
            this._driver = config._redisDriver
                ? config._redisDriver
                : yield this.createDriver(config);
            this._namespace = config.namespace
                ? config.namespace
                : null;
        });
    }
    /**
     *
     * @param config - A set of config options.
     * @param $ctx - Trace context.
     */
    createDriver(config, $ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const url = config.url || `${config.host}:${config.port}`;
            const driverConfig = {
                db: config.index || 0,
                family: 4,
            };
            // Resolve URL
            try {
                const resolvedUrl = yield (0, utils_1.resolveUrl)(url);
                $ctx.logger.debug("Resolved url", resolvedUrl);
                driverConfig.host = resolvedUrl.hostname;
                driverConfig.port = resolvedUrl.port;
            }
            catch (error) {
                $ctx.logger.debug("Error trying to resolve redis url");
            }
            // Check if we already got a client for that host:port.
            const hostId = `${config.host}:${config.port}`;
            if (RedisClient._drivers[hostId]) {
                return RedisClient._drivers[hostId];
            }
            // Create new redis client.
            const driver = new ioredis_1.default(driverConfig);
            // Set to available clients for reuse.
            RedisClient._drivers[hostId] = driver;
            // Return driver to caller.
            return driver;
        });
    }
    /**
     *
     * @param key - Key to be inserted into redis.
     * @param value - Value to be inserted with the key into redis.
     * @param expTimeInSec - Expiration time to be set.
     * @param $ctx - Trace context.
     */
    set(key, value, expTimeInSec = 0, $ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            // All values to be inserted will be stringified first.
            const valueStr = JSON.stringify(value);
            const keyWithNamespace = this._getNamespacedKey(key);
            return new Promise((resolve, reject) => {
                if (expTimeInSec > 0) {
                    this._driver.set(keyWithNamespace, valueStr, "EX", expTimeInSec, (error, reply) => {
                        if (error) {
                            $ctx.logger.error("Error inserting data into redis ", error);
                            return reject(error);
                        }
                        return resolve(reply);
                    });
                }
                else {
                    this._driver.set(keyWithNamespace, valueStr, (error, reply) => {
                        if (error) {
                            $ctx.logger.error("Error inserting data into redis ", error);
                            return reject(error);
                        }
                        return resolve(reply);
                    });
                }
            });
        });
    }
    /**
     *
     * @param key - Key to get the value from.
     * @param $ctx - Trace context.
     */
    get(key, $ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const keyWithNamespace = this._getNamespacedKey(key);
            return new Promise((resolve, reject) => {
                this._driver.get(keyWithNamespace, (error, reply) => {
                    if (error) {
                        $ctx.logger.error("Error getting data from redis ", error);
                        return reject(error);
                    }
                    else if (!reply) {
                        return resolve(null);
                    }
                    /**
                     * Let's try to parse the value stored in redis, but if we couldn't
                     * parse it let's just return what we got from redis.
                     */
                    let parsed = reply;
                    try {
                        parsed = JSON.parse(reply);
                    }
                    catch (parseError) {
                        $ctx.logger.error("could not parse value");
                    }
                    return resolve(parsed);
                });
            });
        });
    }
    /**
     *
     * @param key - Key to get the value from.
     * @param expTimeInSec - Expiration time to be set.
     * @param $ctx - Trace context.
     */
    expire(key, expTimeInSec, $ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const keyWithNamespace = this._getNamespacedKey(key);
            return new Promise((resolve, reject) => {
                this._driver.expire(keyWithNamespace, expTimeInSec, (error, reply) => {
                    if (error) {
                        $ctx.logger.error("Error setting expire time to key ", error);
                        return reject(error);
                    }
                    if (!reply) {
                        return reject(new Error("Expiration time was not set."));
                    }
                    return resolve(reply);
                });
            });
        });
    }
    /**
     * This function going to disconnect this client.
     *
     * @param $ctx - Log context.
     */
    disconnect($ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                this._driver.quit((error, reply) => {
                    if (error) {
                        $ctx.logger.error("Error trying to disconnect from redis ", error);
                        return reject(error);
                    }
                    return resolve(reply);
                });
            });
        });
    }
}
/**
 * Store created redis clients statically so we can reuse them.
 */
RedisClient._drivers = {};
__decorate([
    (0, tracer_1.trace)()
], RedisClient.prototype, "init", null);
__decorate([
    (0, tracer_1.trace)()
], RedisClient.prototype, "createDriver", null);
__decorate([
    (0, tracer_1.trace)()
], RedisClient.prototype, "set", null);
__decorate([
    (0, tracer_1.trace)()
], RedisClient.prototype, "get", null);
__decorate([
    (0, tracer_1.trace)()
], RedisClient.prototype, "expire", null);
__decorate([
    (0, tracer_1.trace)()
], RedisClient.prototype, "disconnect", null);
exports.RedisClient = RedisClient;
//# sourceMappingURL=client.js.map