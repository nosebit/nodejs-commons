/**
 * This function stops the test for a specific amount of
 * time in milliseconds.
 *
 * @param time - Time to sleep in milliseconds.
 */
declare function delay(time: number): Promise<any>;
export { delay, };
