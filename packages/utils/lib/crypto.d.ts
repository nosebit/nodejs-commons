interface IEncryptConfig {
    algorithm?: string;
    secret?: string;
}
/**
 * This function going to encrypt (encode) a text.
 *
 * @param text - Text to be encrypted.
 * @param config - A set of option configs to control the encryption process.
 */
declare function encrypt(text: string, config?: IEncryptConfig): string;
/**
 * This function going to decrypt (decode) a text.
 *
 * @param text - Text to be decrypted.
 * @param config - A set of option configs to control the encryption process.
 */
declare function decrypt(text: string, config?: IEncryptConfig): string;
export { encrypt, decrypt, };
