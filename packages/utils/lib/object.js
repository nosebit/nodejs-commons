"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.diffMin = exports.toPlainObject = exports.removeNulls = void 0;
const deep_diff_1 = require("deep-diff");
const lodash_1 = __importDefault(require("lodash"));
//#####################################################
// Methods
//#####################################################
/**
 * This function convert an object like entity to plain object.
 *
 * @param objLike - A structure that could be parsed to plain object.
 */
function toPlainObject(objLike) {
    return JSON.parse(JSON.stringify(objLike));
}
exports.toPlainObject = toPlainObject;
/**
 * This function remove all nulls presented in an object.
 *
 * @param objLike - A structure that could be parsed to plain object.
 */
function removeNulls(objLike) {
    const plainObj = toPlainObject(objLike);
    Object.keys(plainObj).forEach((key) => ( // eslint-disable-line @typescript-eslint/no-extra-parens
    (plainObj[key] === null) && delete plainObj[key] // eslint-disable-line @typescript-eslint/no-dynamic-delete
    ));
    return plainObj;
}
exports.removeNulls = removeNulls;
/**
 * This function reduce objects to the bare minimal of their differences.
 *
 * @param before - An object before change.
 * @param after - An object after change.
 */
function diffMin(before, after) {
    const objs = [{}, {}];
    // Convert before/after to plain objects.
    const befter = [
        JSON.parse(JSON.stringify(before)),
        JSON.parse(JSON.stringify(after)),
    ];
    for (let i = 0; i < 2; i++) {
        const nextIdx = (i + 1) % befter.length;
        const curr = befter[i];
        const next = befter[nextIdx];
        (0, deep_diff_1.observableDiff)(next, curr, (change) => {
            (0, deep_diff_1.applyChange)(objs[i], curr, change);
        });
    }
    /**
     * This function going to do a deep mapping of object values.
     *
     * @param obj - Object to map.
     * @param fn - Mapper function.
     */
    const mapValuesDeep = (obj, fn) => lodash_1.default.mapValues(obj, (val, key) => lodash_1.default.isPlainObject(val)
        ? mapValuesDeep(val, fn)
        : fn(val, key, obj));
    return objs.map((obj) => mapValuesDeep(obj, (val) => {
        if (lodash_1.default.isArray(val)) {
            return lodash_1.default.filter(val, (item) => !lodash_1.default.isUndefined(item));
        }
        return val;
    }));
}
exports.diffMin = diffMin;
//# sourceMappingURL=object.js.map