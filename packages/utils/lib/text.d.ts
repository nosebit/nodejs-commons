/**
 * This template function going to remove identation from multi-line string.
 *
 * @param texts - The texts to be timed.
 * @param args - Template variable values.
 */
declare function tim(texts: TemplateStringsArray, ...args: string[]): string;
export { tim, };
