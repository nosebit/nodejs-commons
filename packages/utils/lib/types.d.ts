/**
 * This type is used to modify thrift types to match
 * what going to be retrieved from mongodb.
 */
export declare type Modify<TTypeA, TTypeB> = Pick<TTypeA, Exclude<keyof TTypeA, keyof TTypeB>> & TTypeB;
export declare type DeepPartial<T> = {
    [P in keyof T]?: DeepPartial<T[P]>;
};
export declare type RequireFields<T, TKeys extends keyof T> = Omit<T, TKeys> & Required<Pick<T, TKeys>>;
