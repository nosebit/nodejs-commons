/**
 * This function converts an RGB color to hex color.
 *
 * @param rgb - The red, green, blue components.
 */
declare function rgbToHex(rgb: [number, number, number]): string;
/**
 * This function going to generate a random RGB color based
 * on the following article:
 *
 * https://martin.ankerl.com/2009/12/09/how-to-create-random-colors-programmatically/
 *
 * @param hue - The hue in HSV color space.
 * @param saturation - The saturation in HSV color space.
 * @param value - The value in HSV color space.
 */
declare function getRandomRGBColor(hue?: number, saturation?: number, // eslint-disable-line @typescript-eslint/no-magic-numbers
value?: number): [number, number, number];
/**
 * This function going to generate a random hex color based
 * on the following article:
 *
 * https://martin.ankerl.com/2009/12/09/how-to-create-random-colors-programmatically/
 *
 * @param hue - The hue in HSV color space.
 * @param saturation - The saturation in HSV color space.
 * @param value - The value in HSV color space.
 */
declare function getRandomHexColor(hue?: number, saturation?: number, value?: number): string;
export { rgbToHex, getRandomRGBColor, getRandomHexColor, };
