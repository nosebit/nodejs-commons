"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importDefault(require("lodash"));
const color_1 = require("./color");
//#####################################################
// Test definitions
//#####################################################
describe("color util", () => {
    it("should generate a correct hex random color", () => {
        const hex = (0, color_1.getRandomHexColor)();
        expect(lodash_1.default.isString(hex)).toBeTruthy();
        expect(hex.length).toBe(7);
    });
});
//# sourceMappingURL=color.unit.test.js.map