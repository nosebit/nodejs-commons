interface IUrlParts {
    host?: string;
    hostname?: string;
    origin?: string;
    password?: string;
    path?: string;
    port?: number;
    protocol?: string;
    user?: string;
    userinfo?: string;
}
interface IServiceUrl {
    hostname: string;
    port: number;
    host: string;
}
/**
 * This function breaks an url into it's parts. By url
 * we mean somethig with the following schema.
 *
 * [protocol://user:password@hostname:port/path].
 *
 * @param url - The url to be broken.
 * @param opts - A set of options.
 * @param opts.port - A custom port number.
 */
declare function getUrlParts(url: string, opts?: {
    port?: number;
}): IUrlParts;
/**
 * This function resolves an url hostname to ip.
 *
 * @param url - The url to be resolved.
 * @param opts - A set of options.
 * @param opts.ignoredIps - List of ips to ignore.
 */
declare function resolveUrlHostname(url: string, opts?: {
    ignoredIps?: string[];
}): Promise<IUrlParts>;
/**
 * This function resolves a service port.
 *
 * @param url - The url to be resolved.
 */
declare function resolveUrlPort(url: string): Promise<IUrlParts>;
/**
 * This function resolves a service url.
 *
 * @deprecated In favor of resolveUrlPort and resolveUrlHostname
 *
 * @param url - The url to be resolved.
 */
declare function resolveUrl(url: string): Promise<IUrlParts>;
export { getUrlParts, IServiceUrl, IUrlParts, resolveUrl, resolveUrlHostname, resolveUrlPort, };
