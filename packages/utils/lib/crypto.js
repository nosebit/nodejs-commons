"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.decrypt = exports.encrypt = void 0;
const crypto_1 = __importDefault(require("crypto"));
//#####################################################
// Constants
//#####################################################
const ALGORITHM = "aes-256-cbc";
const IV_LENGTH = 16;
const SEPARATOR_CHAR = ".";
const DEFAULT_SECRET = "dRgUjXn2r5u8x/A?D(G+KbPeShVmYq3s";
//#####################################################
// Functions
//#####################################################
/**
 * This function going to encrypt (encode) a text.
 *
 * @param text - Text to be encrypted.
 * @param config - A set of option configs to control the encryption process.
 */
function encrypt(text, config = {}) {
    const secret = config.secret
        || process.env.ENCRYPTION_SECRET
        || DEFAULT_SECRET;
    const iv = crypto_1.default.randomBytes(IV_LENGTH);
    const cipher = crypto_1.default.createCipheriv(ALGORITHM, Buffer.from(secret), iv);
    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return `${iv.toString("hex")}${SEPARATOR_CHAR}${encrypted.toString("hex")}`;
}
exports.encrypt = encrypt;
/**
 * This function going to decrypt (decode) a text.
 *
 * @param text - Text to be decrypted.
 * @param config - A set of option configs to control the encryption process.
 */
function decrypt(text, config = {}) {
    const secret = config.secret
        || process.env.ENCRYPTION_SECRET
        || DEFAULT_SECRET;
    const [ivStr, textContent] = text.split(SEPARATOR_CHAR);
    const iv = Buffer.from(ivStr, "hex");
    const encryptedText = Buffer.from(textContent, "hex");
    const decipher = crypto_1.default.createDecipheriv(ALGORITHM, Buffer.from(secret), iv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
}
exports.decrypt = decrypt;
//# sourceMappingURL=crypto.js.map