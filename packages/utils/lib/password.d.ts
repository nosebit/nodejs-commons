interface IComparePasswordArgs {
    password: string;
    hashedPassword: string;
}
/**
 * This function compares a raw password and a hashed password to
 * see if both matches.
 *
 * @param args - The arguments object.
 * @param args.password - The raw password as entered by the user.
 * @param args.hashedPassword - The hashed password as stored in db.
 */
declare function comparePassword(args: IComparePasswordArgs): Promise<boolean>;
/**
 * This function hashes a password.
 *
 * @param value - The value to be hashed.
 * @param traceInfo - The trace info.
 */
declare function hashPassword(value?: string): Promise<string>;
export { comparePassword, hashPassword, IComparePasswordArgs, };
