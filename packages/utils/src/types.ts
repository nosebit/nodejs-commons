/* eslint-disable import/prefer-default-export */

/**
 * This type is used to modify thrift types to match
 * what going to be retrieved from mongodb.
 */
export type Modify<TTypeA, TTypeB> = Pick<TTypeA, Exclude<keyof TTypeA, keyof TTypeB>> & TTypeB; // eslint-disable-line max-len

export type DeepPartial<T> = {
  [P in keyof T]?: DeepPartial<T[P]>;
};

export type RequireFields<T, TKeys extends keyof T>
  = Omit<T, TKeys> & Required<Pick<T, TKeys>>;
