# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.3.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/utils@0.3.1...@nosebit/utils@0.3.2) (2022-04-26)

**Note:** Version bump only for package @nosebit/utils





## [0.3.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/utils@0.3.0...@nosebit/utils@0.3.1) (2022-01-07)

**Note:** Version bump only for package @nosebit/utils





# [0.3.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/utils@0.2.1...@nosebit/utils@0.3.0) (2021-11-14)


### Features

* **utils:** add crypto module with encrypt/decrypt methods ([fdd26dc](https://github.com/nosebit/nodejs-commons/commit/fdd26dc4b89a0a0190ad7341e5d00684f17d336b))





## [0.2.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/utils@0.2.0...@nosebit/utils@0.2.1) (2021-10-07)


### Bug Fixes

* **utils/colors:** better random color generation ([042e844](https://github.com/nosebit/nodejs-commons/commit/042e844952338f9ec4cc9bf6bd66fad3311c5a4f))





# [0.2.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/utils@0.1.5...@nosebit/utils@0.2.0) (2021-09-12)


### Features

* **utils:** add color utilitary functions ([ec019b9](https://github.com/nosebit/nodejs-commons/commit/ec019b90e3a178b577aa6e179007c9a3d1b65c8d))





## [0.1.5](https://github.com/nosebit/nodejs-commons/compare/@nosebit/utils@0.1.4...@nosebit/utils@0.1.5) (2021-09-11)

**Note:** Version bump only for package @nosebit/utils





## [0.1.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/utils@0.1.3...@nosebit/utils@0.1.4) (2021-06-09)

**Note:** Version bump only for package @nosebit/utils





## [0.1.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/utils@0.1.2...@nosebit/utils@0.1.3) (2020-09-24)

**Note:** Version bump only for package @nosebit/utils





## [0.1.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/utils@0.1.1...@nosebit/utils@0.1.2) (2020-09-12)

**Note:** Version bump only for package @nosebit/utils





## [0.1.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/utils@0.1.0...@nosebit/utils@0.1.1) (2020-05-13)


### Bug Fixes

* **@nosebit/utils:** Add missing export of password module in index.js. ([2b73fff](https://github.com/nosebit/nodejs-commons/commit/2b73fff0cd33db83f74f6b57e5816ce3ae3e1e83))





# 0.1.0 (2020-04-19)


### Features

* New npm package names removing nodejs-* prefix and adding mongo and thrift mock packages. ([461df10](https://github.com/nosebit/nodejs-commons/commit/461df1099b71f838b046a60f2d232e891a65f666))
