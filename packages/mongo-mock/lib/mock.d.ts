import { MongoClient } from "@nosebit/mongo";
/**
 * This class provides a mongodb mock server and client.
 */
declare class MongoMock {
    /**
     * Static method to create an awaitable instance of this class.
     */
    static create(): Promise<MongoMock>;
    /**
     * Flags if the instance is already fully initialized.
     */
    private readonly _initialized;
    /**
     * The mocked server.
     */
    private _server;
    /**
     * Mongo client connected to mocked server.
     */
    private _client;
    /**
     * Getter for cleint variable.
     */
    get client(): MongoClient;
    /**
     * Getter for initialized variable.
     */
    get initialized(): Promise<void>;
    /**
     * This function creates a new instance of this class.
     */
    constructor();
    /**
     * This function going to initialize the new instance.
     */
    private _init;
    /**
     * This function going to close this mock.
     */
    close(): Promise<void>;
}
export { MongoMock, };
