"use strict";
/* eslint-disable import/prefer-default-export */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MongoMock = void 0;
const mongo_1 = require("@nosebit/mongo");
const mongodb_memory_server_1 = require("mongodb-memory-server");
//#####################################################
// Main class
//#####################################################
/**
 * This class provides a mongodb mock server and client.
 */
class MongoMock {
    /**
     * This function creates a new instance of this class.
     */
    constructor() {
        this._initialized = this._init();
    }
    /**
     * Static method to create an awaitable instance of this class.
     */
    static create() {
        return __awaiter(this, void 0, void 0, function* () {
            const instance = new MongoMock();
            yield instance.initialized;
            return instance;
        });
    }
    /**
     * Getter for cleint variable.
     */
    get client() {
        return this._client;
    }
    /**
     * Getter for initialized variable.
     */
    get initialized() {
        return this._initialized;
    }
    /**
     * This function going to initialize the new instance.
     */
    _init() {
        return __awaiter(this, void 0, void 0, function* () {
            const dbName = "MockDb";
            this._server = yield mongodb_memory_server_1.MongoMemoryServer.create({
                instance: {
                    dbName,
                },
            });
            const uri = this._server.getUri();
            // Let us create a client connected to mocked mongo server.
            this._client = new mongo_1.MongoClient();
            yield this._client.connect({
                dbName,
                url: uri,
            });
        });
    }
    /**
     * This function going to close this mock.
     */
    close() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._client.disconnect();
            yield this._server.stop();
        });
    }
}
exports.MongoMock = MongoMock;
//# sourceMappingURL=mock.js.map