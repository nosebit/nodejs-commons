/* eslint-disable import/prefer-default-export */

import { MongoClient } from "@nosebit/mongo";
import { MongoMemoryServer } from "mongodb-memory-server";

//#####################################################
// Main class
//#####################################################
/**
 * This class provides a mongodb mock server and client.
 */
class MongoMock {
  /**
   * Static method to create an awaitable instance of this class.
   */
  public static async create() {
    const instance = new MongoMock();

    await instance.initialized;

    return instance;
  }

  /**
   * Flags if the instance is already fully initialized.
   */
  private readonly _initialized: Promise<void>;

  /**
   * The mocked server.
   */
  private _server: MongoMemoryServer;

  /**
   * Mongo client connected to mocked server.
   */
  private _client: MongoClient;

  /**
   * Getter for cleint variable.
   */
  public get client() {
    return this._client;
  }

  /**
   * Getter for initialized variable.
   */
  public get initialized() {
    return this._initialized;
  }

  /**
   * This function creates a new instance of this class.
   */
  constructor() {
    this._initialized = this._init();
  }

  /**
   * This function going to initialize the new instance.
   */
  private async _init() {
    const dbName = "MockDb";

    this._server = await MongoMemoryServer.create({
      instance: {
        dbName,
      },
    });
    const uri = this._server.getUri();

    // Let us create a client connected to mocked mongo server.
    this._client = new MongoClient();

    await this._client.connect({
      dbName,
      url: uri,
    });
  }

  /**
   * This function going to close this mock.
   */
  public async close() {
    await this._client.disconnect();
    await this._server.stop();
  }
}

//#####################################################
// Exports
//#####################################################
export {
  MongoMock,
};
