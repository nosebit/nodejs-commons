# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.30](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.29...@nosebit/thrift-mock@0.2.30) (2022-04-26)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.29](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.28...@nosebit/thrift-mock@0.2.29) (2022-02-09)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.28](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.27...@nosebit/thrift-mock@0.2.28) (2022-01-07)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.27](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.26...@nosebit/thrift-mock@0.2.27) (2021-11-14)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.26](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.25...@nosebit/thrift-mock@0.2.26) (2021-10-09)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.25](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.23...@nosebit/thrift-mock@0.2.25) (2021-10-09)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.24](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.23...@nosebit/thrift-mock@0.2.24) (2021-10-09)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.23](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.22...@nosebit/thrift-mock@0.2.23) (2021-10-07)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.22](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.21...@nosebit/thrift-mock@0.2.22) (2021-10-07)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.21](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.20...@nosebit/thrift-mock@0.2.21) (2021-10-07)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.20](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.19...@nosebit/thrift-mock@0.2.20) (2021-09-12)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.19](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.18...@nosebit/thrift-mock@0.2.19) (2021-09-11)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.18](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.17...@nosebit/thrift-mock@0.2.18) (2021-07-14)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.17](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.16...@nosebit/thrift-mock@0.2.17) (2021-07-08)


### Bug Fixes

* **thrift:** fix build ([c6e794a](https://github.com/nosebit/nodejs-commons/commit/c6e794a6969fed494bf8e288f5f6379ca7355e1a))





## [0.2.16](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.15...@nosebit/thrift-mock@0.2.16) (2021-06-12)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.15](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.14...@nosebit/thrift-mock@0.2.15) (2021-06-09)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.14](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.13...@nosebit/thrift-mock@0.2.14) (2020-10-26)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.13](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.12...@nosebit/thrift-mock@0.2.13) (2020-10-15)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.12](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.11...@nosebit/thrift-mock@0.2.12) (2020-10-15)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.11](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.10...@nosebit/thrift-mock@0.2.11) (2020-10-14)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.10](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.9...@nosebit/thrift-mock@0.2.10) (2020-10-12)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.9](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.8...@nosebit/thrift-mock@0.2.9) (2020-10-12)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.8](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.7...@nosebit/thrift-mock@0.2.8) (2020-10-10)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.7](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.6...@nosebit/thrift-mock@0.2.7) (2020-09-24)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.6](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.5...@nosebit/thrift-mock@0.2.6) (2020-09-12)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.5](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.4...@nosebit/thrift-mock@0.2.5) (2020-05-13)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.3...@nosebit/thrift-mock@0.2.4) (2020-05-12)


### Bug Fixes

* **@nosebit/thrift-mock:** Add missing service name param to constructor and static create method. ([dff1ce0](https://github.com/nosebit/nodejs-commons/commit/dff1ce0338ab919cd3dee71e216d9a093ab831d5))





## [0.2.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.2...@nosebit/thrift-mock@0.2.3) (2020-05-12)


### Bug Fixes

* **@nosebit/thrift-mock:** Add missing required arg (service name) when creating a new ThriftService instance. ([e837487](https://github.com/nosebit/nodejs-commons/commit/e837487339309ad8182c3e20829048a7353dcbd3))





## [0.2.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.1...@nosebit/thrift-mock@0.2.2) (2020-05-12)

**Note:** Version bump only for package @nosebit/thrift-mock





## [0.2.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/thrift-mock@0.2.0...@nosebit/thrift-mock@0.2.1) (2020-04-19)

**Note:** Version bump only for package @nosebit/thrift-mock





# 0.2.0 (2020-04-19)


### Features

* New npm package names removing nodejs-* prefix and adding mongo and thrift mock packages. ([461df10](https://github.com/nosebit/nodejs-commons/commit/461df1099b71f838b046a60f2d232e891a65f666))





# 0.1.0 (2020-04-18)


### Features

* Add basic funcionality for mongo and thrift mocks. ([54247f9](https://github.com/nosebit/nodejs-mocks/commit/54247f97c0a098ea9976a0212a288063cb13847e))
