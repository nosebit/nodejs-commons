/* eslint-disable import/prefer-default-export */

import http from "http";
import tls from "tls";

import { ThriftService } from "@nosebit/thrift";
import * as thrift from "thrift";

//#####################################################
// Types
//#####################################################
type IClientConstructor<TClient = any> = new (
  output: thrift.TTransport,
  protocol: new (trans: thrift.TTransport) => thrift.TProtocol,
) => TClient;

type IProcessorConstructor<THandler, TProcessor = any> = new (
  handler: THandler,
) => TProcessor;

interface IThriftSpec<
  THandler,
  TClient,
> {
  Client: IClientConstructor<TClient>;
  Processor: IProcessorConstructor<THandler>;
}

//#####################################################
// Main class
//#####################################################
/**
 * This class provides a mongodb mock server and client.
 */
class ThriftMock<
  THandler,
  TClient,
> {
  /**
   * Static method to create an awaitable instance of this class.
   *
   * @param name - Service name.
   * @param spec - Thrift spec obj.
   * @param handler - Thrift handler.
   */
  public static async create<
    THandler2,
    TClient2
  >(
    name: string,
    spec: IThriftSpec<THandler2, TClient2>,
    handler?: Partial<THandler2>,
  ) {
    const instance = new ThriftMock(name, spec, handler);

    await instance.initialized;

    return instance;
  }

  /**
   * Flags if the instance is already fully initialized.
   */
  private readonly _initialized: Promise<void>;

  /**
   * The handler so we can access it from tests.
   */
  private readonly _handler: Partial<THandler>;

  /**
   * Getter for handler variable.
   */
  public get handler() {
    return this._handler;
  }

  /**
   * The mocked server.
   */
  private _server: http.Server | tls.Server;

  /**
   * Mongo client connected to mocked server.
   */
  private _client: ThriftService<TClient>;

  /**
   * Getter for client variable.
   */
  public get client() {
    return this._client.client;
  }

  /**
   * Getter for initialized variable.
   */
  public get initialized() {
    return this._initialized;
  }

  /**
   * This function creates a new instance of this class.
   *
   * @param name - Service name.
   * @param spec - Thrift spec obj.
   * @param handler - Thrift handler.
   */
  constructor(
    name: string,
    spec: IThriftSpec<THandler, TClient>,
    handler?: Partial<THandler>
  ) {
    this._handler = handler;
    this._initialized = this._init(name, spec, handler);
  }

  /**
   * This function going to initialize the new instance.
   *
   * @param name - Thrift service name.
   * @param spec - Thrift spec obj.
   * @param handler - Thrift handler.
   */
  private async _init(
    name: string,
    spec: IThriftSpec<THandler, TClient>,
    handler?: Partial<THandler>,
  ) {
    this._server = thrift.createServer(spec, handler);

    /**
     * Start listen to a random free port.
     */
    await new Promise<void>((resolve) => this._server.listen({
      host: "127.0.0.1",
      port: 0,
    }, resolve));

    const info = this._server.address();

    if (typeof info === "string") {
      throw new Error("could not get thrift server port");
    }

    const { address, port } = info;

    /**
     * Create a client connected to mocked server.
     */
    this._client = new ThriftService<TClient>(name, spec);

    await this._client.connect(`${address}:${port}`);
  }

  /**
   * This function going to close this mock.
   */
  public async close() {
    this._client.disconnect();
    this._server.close();
  }
}

//#####################################################
// Exports
//#####################################################
export {
  ThriftMock,
};
