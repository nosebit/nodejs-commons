import * as thrift from "thrift";
declare type IClientConstructor<TClient = any> = new (output: thrift.TTransport, protocol: new (trans: thrift.TTransport) => thrift.TProtocol) => TClient;
declare type IProcessorConstructor<THandler, TProcessor = any> = new (handler: THandler) => TProcessor;
interface IThriftSpec<THandler, TClient> {
    Client: IClientConstructor<TClient>;
    Processor: IProcessorConstructor<THandler>;
}
/**
 * This class provides a mongodb mock server and client.
 */
declare class ThriftMock<THandler, TClient> {
    /**
     * Static method to create an awaitable instance of this class.
     *
     * @param name - Service name.
     * @param spec - Thrift spec obj.
     * @param handler - Thrift handler.
     */
    static create<THandler2, TClient2>(name: string, spec: IThriftSpec<THandler2, TClient2>, handler?: Partial<THandler2>): Promise<ThriftMock<THandler2, TClient2>>;
    /**
     * Flags if the instance is already fully initialized.
     */
    private readonly _initialized;
    /**
     * The handler so we can access it from tests.
     */
    private readonly _handler;
    /**
     * Getter for handler variable.
     */
    get handler(): Partial<THandler>;
    /**
     * The mocked server.
     */
    private _server;
    /**
     * Mongo client connected to mocked server.
     */
    private _client;
    /**
     * Getter for client variable.
     */
    get client(): TClient;
    /**
     * Getter for initialized variable.
     */
    get initialized(): Promise<void>;
    /**
     * This function creates a new instance of this class.
     *
     * @param name - Service name.
     * @param spec - Thrift spec obj.
     * @param handler - Thrift handler.
     */
    constructor(name: string, spec: IThriftSpec<THandler, TClient>, handler?: Partial<THandler>);
    /**
     * This function going to initialize the new instance.
     *
     * @param name - Thrift service name.
     * @param spec - Thrift spec obj.
     * @param handler - Thrift handler.
     */
    private _init;
    /**
     * This function going to close this mock.
     */
    close(): Promise<void>;
}
export { ThriftMock, };
