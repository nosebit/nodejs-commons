"use strict";
/* eslint-disable import/prefer-default-export */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ThriftMock = void 0;
const thrift_1 = require("@nosebit/thrift");
const thrift = __importStar(require("thrift"));
//#####################################################
// Main class
//#####################################################
/**
 * This class provides a mongodb mock server and client.
 */
class ThriftMock {
    /**
     * This function creates a new instance of this class.
     *
     * @param name - Service name.
     * @param spec - Thrift spec obj.
     * @param handler - Thrift handler.
     */
    constructor(name, spec, handler) {
        this._handler = handler;
        this._initialized = this._init(name, spec, handler);
    }
    /**
     * Static method to create an awaitable instance of this class.
     *
     * @param name - Service name.
     * @param spec - Thrift spec obj.
     * @param handler - Thrift handler.
     */
    static create(name, spec, handler) {
        return __awaiter(this, void 0, void 0, function* () {
            const instance = new ThriftMock(name, spec, handler);
            yield instance.initialized;
            return instance;
        });
    }
    /**
     * Getter for handler variable.
     */
    get handler() {
        return this._handler;
    }
    /**
     * Getter for client variable.
     */
    get client() {
        return this._client.client;
    }
    /**
     * Getter for initialized variable.
     */
    get initialized() {
        return this._initialized;
    }
    /**
     * This function going to initialize the new instance.
     *
     * @param name - Thrift service name.
     * @param spec - Thrift spec obj.
     * @param handler - Thrift handler.
     */
    _init(name, spec, handler) {
        return __awaiter(this, void 0, void 0, function* () {
            this._server = thrift.createServer(spec, handler);
            /**
             * Start listen to a random free port.
             */
            yield new Promise((resolve) => this._server.listen({
                host: "127.0.0.1",
                port: 0,
            }, resolve));
            const info = this._server.address();
            if (typeof info === "string") {
                throw new Error("could not get thrift server port");
            }
            const { address, port } = info;
            /**
             * Create a client connected to mocked server.
             */
            this._client = new thrift_1.ThriftService(name, spec);
            yield this._client.connect(`${address}:${port}`);
        });
    }
    /**
     * This function going to close this mock.
     */
    close() {
        return __awaiter(this, void 0, void 0, function* () {
            this._client.disconnect();
            this._server.close();
        });
    }
}
exports.ThriftMock = ThriftMock;
//# sourceMappingURL=mock.js.map