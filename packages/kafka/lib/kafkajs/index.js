"use strict";
/* eslint-disable import/prefer-default-export */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KafkaConsumer = void 0;
var consumer_1 = require("./consumer");
Object.defineProperty(exports, "KafkaConsumer", { enumerable: true, get: function () { return __importDefault(consumer_1).default; } });
//# sourceMappingURL=index.js.map