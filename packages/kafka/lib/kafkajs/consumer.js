"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const tracer_1 = require("@nosebit/tracer");
const KafkaJS = __importStar(require("kafkajs"));
//#####################################################
// Main class
//#####################################################
/**
 * This is a consumer which uses KafkaJS under the hood.
 * We use KafkaJS for consumer because RdKafka has problems
 * when consuming messages and performance for reading
 * messages seems to be similar for both packages.
 */
class KafkaConsumer {
    /**
     * Constructor.
     *
     * @param client - Main kafka client.
     * @param config - Consumer configs.
     */
    constructor(client, config = {}) {
        /**
         * Flag indicating if this producer is already connected.
         */
        this._isConnected = false;
        /**
         * List of registered callbacks.
         */
        this._callbacks = [];
        this._client = client;
        this._initialized = this._init(config);
    }
    /**
     * Getter for initialized promise.
     */
    get initialized() {
        return this._initialized;
    }
    /**
     * Getter for is isConnected flag.
     */
    get isConnected() {
        return this._isConnected;
    }
    /**
     * Getter for connected promise.
     */
    get connected() {
        return this._connected;
    }
    /**
     * This function going to initialize the consumer.
     *
     * @param config - Consumer config options.
     * @param _$ctx - Trace context.
     */
    _init(config, _$ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            this._kafkajsClient = new KafkaJS.Kafka({
                brokers: this._client.brokers,
                clientId: this._client.id,
                logLevel: KafkaJS.logLevel.NOTHING,
            });
            this._consumer = this._kafkajsClient.consumer({
                groupId: config.groupId || this._client.id,
            });
            this._connected = this.connect();
        });
    }
    /**
     * This function going to connect the underlying producer.
     *
     * @param $ctx - Trace Context.
     */
    connect($ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this._consumer.connect();
                this._isConnected = true;
                $ctx.logger.debug("kafkajs consumer is ready");
            }
            catch (error) {
                $ctx.logger.error("kafkajs consumer can't connect", error);
            }
        });
    }
    /**
     * This function going to disconnect this producer.
     */
    disconnect() {
        return __awaiter(this, void 0, void 0, function* () {
            return this._consumer.disconnect();
        });
    }
    /**
     * This function going to subscribe to topic.
     *
     * @param topic - Topic to subscribe to.
     * @param $ctx - Trace context.
     */
    subscribe(topic, $ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.connected;
            try {
                yield this._consumer.subscribe({
                    topic,
                });
                $ctx.logger.debug(`kafkajs subscribed to topic "${topic}"`);
            }
            catch (error) {
                $ctx.logger.error(`could not subscribe to topic ${topic}`, error);
            }
        });
    }
    /**
     * Start consuming messages.
     */
    start() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._connected;
            return this._consumer.run({
                eachMessage: ({ topic, message, partition, }) => __awaiter(this, void 0, void 0, function* () {
                    yield Promise.all(this._callbacks.map((callback) => {
                        const value = message.value.toString();
                        const key = message.key.toString();
                        const msg = {
                            key,
                            partition,
                            topic,
                            value,
                        };
                        return callback(msg);
                    }));
                }),
            });
        });
    }
    /**
     * This function register a callback for on message.
     *
     * @param callback - Callback to be invoked.
     */
    onMessage(callback) {
        return __awaiter(this, void 0, void 0, function* () {
            this._callbacks.push(callback);
        });
    }
}
__decorate([
    (0, tracer_1.trace)()
], KafkaConsumer.prototype, "_init", null);
__decorate([
    (0, tracer_1.trace)()
], KafkaConsumer.prototype, "connect", null);
__decorate([
    (0, tracer_1.trace)()
], KafkaConsumer.prototype, "disconnect", null);
__decorate([
    (0, tracer_1.trace)()
], KafkaConsumer.prototype, "subscribe", null);
__decorate([
    (0, tracer_1.trace)()
], KafkaConsumer.prototype, "start", null);
exports.default = KafkaConsumer;
//# sourceMappingURL=consumer.js.map