import { TraceContext } from "@nosebit/tracer";
import { IConsumer, IConsumerConfig, IConsumerReceiveMessage, IKafkaClient } from "../types";
/**
 * This is a consumer which uses KafkaJS under the hood.
 * We use KafkaJS for consumer because RdKafka has problems
 * when consuming messages and performance for reading
 * messages seems to be similar for both packages.
 */
export default class KafkaConsumer implements IConsumer {
    /**
     * Flag indicating if this producer is already connected.
     */
    private _isConnected;
    /**
     * Promise for connected status.
     */
    private _connected;
    /**
     * The kafka client.
     */
    private readonly _client;
    /**
     * The kafka client.
     */
    private _kafkajsClient;
    /**
     * The kafkajs consumer.
     */
    private _consumer;
    /**
     * Flag indicating if producer is initialized and ready.
     */
    private readonly _initialized;
    /**
     * List of registered callbacks.
     */
    private readonly _callbacks;
    /**
     * Getter for initialized promise.
     */
    get initialized(): Promise<void>;
    /**
     * Getter for is isConnected flag.
     */
    get isConnected(): boolean;
    /**
     * Getter for connected promise.
     */
    get connected(): Promise<void>;
    /**
     * Constructor.
     *
     * @param client - Main kafka client.
     * @param config - Consumer configs.
     */
    constructor(client: IKafkaClient, config?: IConsumerConfig);
    /**
     * This function going to initialize the consumer.
     *
     * @param config - Consumer config options.
     * @param _$ctx - Trace context.
     */
    private _init;
    /**
     * This function going to connect the underlying producer.
     *
     * @param $ctx - Trace Context.
     */
    connect($ctx?: TraceContext): Promise<void>;
    /**
     * This function going to disconnect this producer.
     */
    disconnect(): Promise<void>;
    /**
     * This function going to subscribe to topic.
     *
     * @param topic - Topic to subscribe to.
     * @param $ctx - Trace context.
     */
    subscribe(topic: string, $ctx?: TraceContext): Promise<void>;
    /**
     * Start consuming messages.
     */
    start(): Promise<void>;
    /**
     * This function register a callback for on message.
     *
     * @param callback - Callback to be invoked.
     */
    onMessage(callback: (msg: IConsumerReceiveMessage) => void): Promise<void>;
}
