/// <reference types="node" />
import { EventEmitter } from "events";
import { TraceContext } from "@nosebit/tracer";
import { ISendMessage, ISendOpts, IMessage, IKafkaClient, IKafkaClientConfig } from "./types";
/**
 * This function implements a kafka client.
 */
declare class KafkaClient implements IKafkaClient {
    /**
     * Shared instance.
     */
    private static _shared;
    /**
     * Topics already created by the clien.
     */
    private static readonly _createdTopicsSet;
    /**
     * This static function retrieves the shared instance.
     *
     * @param config - The client config.
     * @param _$ctx - Trace context.
     */
    static sharedInit(config?: IKafkaClientConfig, _$ctx?: TraceContext): KafkaClient;
    /**
     * This function decode a message key which has the format
     * TOPIC:EVT_NAME.
     *
     * @param type - The message type.
     */
    static msgTypeDecode(type: string): {
        evtName: string;
        topicName: string;
    };
    /**
     * Static getter for shared.
     */
    static get shared(): KafkaClient;
    /**
     * Client id.
     */
    private readonly _id;
    /**
     * Kafka broker urls.
     */
    private readonly _brokers;
    /**
     * Environment name which going to be used to scope topic names.
     */
    private readonly _env;
    /**
     * Node emitter to be used instead of kafka. Useful when running a monolith
     * version of backend.
     */
    private _emitter;
    /**
     * The producer.
     */
    private _producer;
    /**
     * The consumer.
     */
    private _consumer;
    /**
     * Admin client.
     */
    private _adminClient;
    /**
     * Events namespace.
     */
    private readonly _namespace;
    /**
     * Flag indicating if we should create topics on the fly.
     */
    private readonly _shouldCreateTopics;
    /**
     * Topics config for creation.
     */
    private readonly _topicConfig;
    /**
     * This is a promise that gets resolved when client is fully initialized.
     */
    private readonly _initialized;
    /**
     * The producer queue which store all producer messages while
     * producer is not ready yet.
     */
    private readonly _sendQueue;
    /**
     * Consumer callbacks map.
     */
    private readonly _callbacksMap;
    /**
     * Getter for emiiter.
     */
    get emitter(): EventEmitter;
    /**
     * This getter retrieves the initialized property.
     */
    get initialized(): Promise<void>;
    /**
     * Getter for id.
     */
    get id(): string;
    /**
     * Getter for brokers.
     */
    get brokers(): string[];
    /**
     * This function creates a new instance of this class.
     *
     * @param config - The client config.
     */
    constructor(config: IKafkaClientConfig);
    /**
     * This function initializes the client.
     *
     * @param args -
     */
    private _init;
    /**
     * This function going to create a set of topics based on topic
     * config received.
     *
     * @param topics - Topic names to be created.
     * @param $ctx - Trace context.
     */
    private _createTopics;
    /**
     * This function handle a consumer message.
     *
     * @param consumerMsg - The message as coming from consumer.
     * @param $ctx - Trace context.
     */
    private _handleConsumerMessage;
    /**
     * This function encode topic name with env.
     *
     * @param name - Topic name to encode.
     */
    private _encodeTopicName;
    /**
     * This function decode topic name with env.
     *
     * @param name - Name to be decoded.
     */
    private _decodeTopicName;
    /**
     * This function going to create a topic.
     *
     * @param name - Topic name.
     * @param opts - Set of options.
     * @param opts.partitions - Number of partitions.
     * @param opts.replication - Replication factor.
     * @param opts.timeout - The timeout to create topic.
     * @param $ctx - Trace context.
     */
    createTopic(name: string, opts?: {
        partitions?: number;
        replication?: number;
        timeout?: number;
    }, $ctx?: TraceContext): Promise<void>;
    /**
     * This function going to emit an event. You can send to
     * multiple topics using `topic_1,topic_2,...,topic_n:evt-name`.
     *
     * @param type - The message type.
     * @param msg - Message to send.
     * @param opts - Options to control sending.
     * @param $ctx - Trace context.
     */
    send<T = any>(type: string, msg: ISendMessage<T>, opts?: ISendOpts, $ctx?: TraceContext): Promise<void | void[]>;
    /**
     * This function going to register a new callback.
     *
     * @param type - Type of messages to listen to.
     * @param callback - Callback to get called when a message arrives.
     * @param _ctx - Log context.
     */
    on<T = any>(type: string, callback: (msg: IMessage<T>) => any): () => void;
    /**
     * This function is going to unregister a callback.
     *
     * @param type - Type of messages to unregister the callback from.
     * @param callback - The callback.
     * @param _ctx - Log context.
     */
    off<T = any>(type: string, callback: (msg: IMessage<T>) => any): Promise<void>;
    /**
     * This function disconnect this client from the remote broker.
     *
     * @param _$ctx - Trace context.
     */
    disconnect(_$ctx?: TraceContext): Promise<any>;
}
export { KafkaClient, };
