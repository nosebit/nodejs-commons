"use strict";
/* eslint-disable import/prefer-default-export, max-classes-per-file */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KafkaClient = void 0;
//#####################################################
// Imports
//#####################################################
const events_1 = require("events");
const tracer_1 = require("@nosebit/tracer");
const lodash_1 = __importDefault(require("lodash"));
const moment_1 = __importDefault(require("moment"));
const nanoid_1 = require("nanoid");
const kafkajs_1 = require("./kafkajs");
const rdkafka_1 = require("./rdkafka");
//#####################################################
// Auxiliary Functions
//#####################################################
/**
 * This function converts a struct to a plain javascript
 * object that encapsulates the original types info within
 * the struct.
 *
 * @param data - Data struct to be converted.
 */
function convertToTypedObj(data) {
    if (lodash_1.default.isMap(data)) {
        const dataObj = [
            ...data.entries(),
        ].reduce((accum, [key, val]) => (Object.assign(Object.assign({}, accum), { [key]: val })), {});
        return {
            __nsbKakfaType: "Map",
            value: convertToTypedObj(dataObj),
        };
    }
    else if (lodash_1.default.isSet(data)) {
        return {
            __nsbKakfaType: "Set",
            value: [...data.keys()].map((item) => convertToTypedObj(item)),
        };
    }
    else if (lodash_1.default.isArray(data)) {
        return data.map((val) => convertToTypedObj(val));
    }
    else if (lodash_1.default.isDate(data)) {
        return {
            __nsbKakfaType: "Date",
            value: data.toString(),
        };
    }
    else if (moment_1.default.isMoment(data)) {
        return {
            __nsbKakfaType: "Moment",
            value: data.toISOString(),
        };
    }
    else if (lodash_1.default.isError(data)) {
        return {
            __nsbKakfaType: "Error",
            stack: data.stack,
            value: data.message,
        };
    }
    else if (lodash_1.default.isObject(data)) {
        return Object.entries(data).reduce((accum, [key, val]) => (Object.assign(Object.assign({}, accum), { [key]: convertToTypedObj(val) })), {});
    }
    else if (lodash_1.default.isNumber(data)
        || lodash_1.default.isString(data)
        || lodash_1.default.isBoolean(data)
        || lodash_1.default.isNull(data)) {
        return data;
    }
    return {
        __nsbKakfaType: "unknown",
        value: data,
    };
}
/**
 * This function converts a plain typed object to original struct
 * with correct types.
 *
 * @param data - Typed object to be converted.
 */
function convertFromTypedObj(data) {
    if (lodash_1.default.isArray(data)) {
        return data.map((val) => convertFromTypedObj(val));
    }
    else if (lodash_1.default.isObject(data)) {
        data = data; // eslint-disable-line no-param-reassign
        switch (data.__nsbKakfaType) {
            case "Map": {
                return new Map(Object.entries(data.value).map(([key, val]) => // eslint-disable-line dot-notation
                 [key, convertFromTypedObj(val)]));
            }
            case "Set": {
                return new Set(data.value.map((val) => // eslint-disable-line dot-notation
                 convertFromTypedObj(val)));
            }
            case "Date": {
                return new Date(data.value);
            }
            case "Moment": {
                return (0, moment_1.default)(data.value);
            }
            case "Error": {
                const error = new Error(data.value);
                error.stack = data.stack;
                return error;
            }
            case "unknown": {
                return data.value;
            }
            default: {
                break;
            }
        }
        /**
         * Iterate over each key.
         */
        return Object.entries(data).reduce((accum, [key, val]) => (Object.assign(Object.assign({}, accum), { [key]: convertFromTypedObj(val) })), {});
    }
    return data;
}
//#####################################################
// Main class
//#####################################################
/**
 * This function implements a kafka client.
 */
class KafkaClient {
    /**
     * This function creates a new instance of this class.
     *
     * @param config - The client config.
     */
    constructor(config) {
        /**
         * Flag indicating if we should create topics on the fly.
         */
        this._shouldCreateTopics = false;
        /**
         * The producer queue which store all producer messages while
         * producer is not ready yet.
         */
        this._sendQueue = [];
        this._id = config.id || (0, nanoid_1.nanoid)();
        this._brokers = config.brokers;
        this._callbacksMap = new Map();
        this._shouldCreateTopics = config.shouldCreateTopics;
        this._topicConfig = config.topicConfig;
        this._env = config.env;
        this._namespace = config.namespace;
        this._initialized = this._init(config);
    }
    /**
     * This static function retrieves the shared instance.
     *
     * @param config - The client config.
     * @param _$ctx - Trace context.
     */
    static sharedInit(config, _$ctx) {
        if (!KafkaClient._shared) {
            KafkaClient._shared = new KafkaClient(config);
        }
        return KafkaClient._shared;
    }
    /**
     * This function decode a message key which has the format
     * TOPIC:EVT_NAME.
     *
     * @param type - The message type.
     */
    static msgTypeDecode(type) {
        const parts = type.split(":");
        const topicName = parts.length ? parts[0] : null;
        const evtName = parts.length > 1 ? parts[1] : null;
        return {
            evtName,
            topicName,
        };
    }
    /**
     * Static getter for shared.
     */
    static get shared() {
        return KafkaClient._shared;
    }
    /**
     * Getter for emiiter.
     */
    get emitter() {
        return this._emitter;
    }
    /**
     * This getter retrieves the initialized property.
     */
    get initialized() {
        return this._initialized;
    }
    /**
     * Getter for id.
     */
    get id() {
        return this._id;
    }
    /**
     * Getter for brokers.
     */
    get brokers() {
        return this._brokers;
    }
    /**
     * This function initializes the client.
     *
     * @param args -
     */
    _init(args) {
        return __awaiter(this, void 0, void 0, function* () {
            const { $ctx } = args;
            if (args.useNodeEmitter) {
                this._emitter = new events_1.EventEmitter();
                this._emitter.on("event", this._handleConsumerMessage.bind(this));
                return Promise.resolve();
            }
            const consumerConfig = Object.assign({}, args.consumerConfig);
            if (args.namespace) {
                consumerConfig.groupId = consumerConfig.groupId
                    ? `${args.namespace}-${consumerConfig.groupId}`
                    : consumerConfig.groupId;
            }
            this._producer = args.producer
                || new rdkafka_1.KafkaProducer(this, args.producerConfig);
            this._consumer = args.consumer
                || new kafkajs_1.KafkaConsumer(this, consumerConfig);
            this._adminClient = args.adminClient || new rdkafka_1.KafkaAdminClient(this);
            // Flush queued messages when producer is ready.
            this._producer.connected.then(() => {
                while (this._sendQueue.length > 0) { // eslint-disable-line no-magic-numbers
                    const sendArgs = this._sendQueue.shift();
                    this._producer.send(sendArgs);
                }
            });
            // Setup consumer when connected.
            const consumerPromise = this._consumer.connected.then(() => __awaiter(this, void 0, void 0, function* () {
                // Register consumer.
                this._consumer.onMessage((consumerMsg) => {
                    this._handleConsumerMessage(consumerMsg);
                });
                // Subscribe to topics.
                yield Promise.all((args.subscribe || []).map(({ topic }) => this._consumer.subscribe(this._encodeTopicName(topic))));
                if (this._shouldCreateTopics) {
                    this._createTopics((args.subscribe || []).map(({ topic }) => topic), $ctx);
                }
                // Start consuming.
                yield this._consumer.start();
            }));
            return Promise.all([
                this._producer.connected,
                consumerPromise,
            ]).then(() => null);
        });
    }
    /**
     * This function going to create a set of topics based on topic
     * config received.
     *
     * @param topics - Topic names to be created.
     * @param $ctx - Trace context.
     */
    _createTopics(topics, $ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const promises = topics.map((topic) => this.createTopic(topic, {
                partitions: lodash_1.default.get(this._topicConfig, "numbPartitions"),
                replication: lodash_1.default.get(this._topicConfig, "replicationFactor"),
            }, $ctx));
            return Promise.all(promises);
        });
    }
    /**
     * This function handle a consumer message.
     *
     * @param consumerMsg - The message as coming from consumer.
     * @param $ctx - Trace context.
     */
    _handleConsumerMessage(consumerMsg, $ctx) {
        // Built message
        let msg;
        // Parse msg data.
        try {
            msg = JSON.parse(consumerMsg.value);
        }
        catch (error) {
            $ctx.logger.error("could not parse message", error);
            return;
        }
        msg.data = convertFromTypedObj(msg.data);
        /**
         * Decode topic name to get env.
         */
        const { env } = this._decodeTopicName(consumerMsg.topic);
        /**
         * Find out the key coming from remote sender (from
         * message). The key sent is something in the form:
         *
         * TOPIC:EVT_NAME.
         */
        const decodedType = KafkaClient.msgTypeDecode(msg.type);
        // Remove namespace prefix from topicName
        if (this._namespace) {
            decodedType.topicName = decodedType.topicName.replace(new RegExp(`^${this._namespace}-`), "");
        }
        // Fulfill message.
        msg.evtName = decodedType.evtName;
        msg.topic = decodedType.topicName;
        msg.env = env;
        $ctx.logger.debug("new message", tracer_1.TraceContext.secret(msg));
        /**
         * Now we going to iterate over all registered consumer
         * callbacks to verify which callback we going to call
         * (i.e., which match the message key).
         */
        const typesWithCallbacks = [...this._callbacksMap.keys()];
        typesWithCallbacks.forEach((typeWithListener) => {
            const { topicName: targetTopicName, evtName: targetEvtName, } = KafkaClient.msgTypeDecode(typeWithListener);
            /**
             * Check if keyWithListener match incoming key.
             */
            if (typeWithListener === "*"
                || (msg.topic === targetTopicName
                    && targetEvtName === "*") || msg.type === typeWithListener) {
                // Iterate over all registered callbacks.
                if (this._callbacksMap.has(typeWithListener)) {
                    this._callbacksMap.get(typeWithListener).forEach((callback) => {
                        callback.fn(msg);
                    });
                }
            }
        });
    }
    /**
     * This function encode topic name with env.
     *
     * @param name - Topic name to encode.
     */
    _encodeTopicName(name) {
        let topic = name;
        if (this._env) {
            topic = `${topic}___${this._env}`;
        }
        if (this._namespace) {
            topic = `${this._namespace}-${topic}`;
        }
        return topic;
    }
    /**
     * This function decode topic name with env.
     *
     * @param name - Name to be decoded.
     */
    _decodeTopicName(name) {
        const parts = name.split("___");
        const env = parts.length === 2 ? parts[1] : null;
        return { env, topic: parts[0] };
    }
    /**
     * This function going to create a topic.
     *
     * @param name - Topic name.
     * @param opts - Set of options.
     * @param opts.partitions - Number of partitions.
     * @param opts.replication - Replication factor.
     * @param opts.timeout - The timeout to create topic.
     * @param $ctx - Trace context.
     */
    createTopic(name, opts = {}, $ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const partitions = opts.partitions || 1;
            const replication = opts.replication || 1;
            if (KafkaClient._createdTopicsSet.has(name)) {
                $ctx.logger.debug(`topic "${name}" already created`);
                return Promise.resolve();
            }
            const topicEncodedName = this._encodeTopicName(name);
            try {
                yield this._adminClient.createTopic(topicEncodedName, {
                    partitions,
                    replication,
                    timeout: opts.timeout,
                });
                $ctx.logger.debug(`topic ${topicEncodedName} created`);
                KafkaClient._createdTopicsSet.add(topicEncodedName);
            }
            catch (error) {
                $ctx.logger.error(`could not create topic ${topicEncodedName}`, error);
            }
            return null;
        });
    }
    /**
     * This function going to emit an event. You can send to
     * multiple topics using `topic_1,topic_2,...,topic_n:evt-name`.
     *
     * @param type - The message type.
     * @param msg - Message to send.
     * @param opts - Options to control sending.
     * @param $ctx - Trace context.
     */
    send(type, msg, opts = {}, $ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const topics = type.split(",");
            // Process last topic to extract evtName
            const lastParts = topics.pop().split(":");
            const lastTopic = lastParts.shift();
            const evtName = lastParts.length ? lastParts.shift() : null;
            topics.push(lastTopic);
            const promises = [];
            if (this._shouldCreateTopics || opts.shouldCreateTopics) {
                try {
                    yield this._createTopics(topics, $ctx);
                }
                catch (error) {
                    $ctx.logger.error("could not create topics", error);
                }
            }
            const plainMsgData = convertToTypedObj(msg.data);
            for (let i = 0; i < topics.length; i++) { // eslint-disable-line no-magic-numbers
                const encodedTopicName = this._encodeTopicName(topics[i]);
                const msgType = evtName ? `${topics[i]}:${evtName}` : topics[i];
                const sendArgs = {
                    key: msgType,
                    message: Object.assign(Object.assign({}, msg), { data: plainMsgData, timestamp: (0, moment_1.default)().unix(), type: msgType }),
                    partition: opts.partition,
                    topic: encodedTopicName,
                };
                if (this._producer && !this._producer.isConnected) {
                    // Enqueue payload to send it latter.
                    this._sendQueue.push(sendArgs);
                    return Promise.resolve();
                }
                if (this._emitter) {
                    const consumerMsg = {
                        key: sendArgs.key,
                        partition: sendArgs.partition,
                        topic: sendArgs.topic,
                        value: JSON.stringify(sendArgs.message),
                    };
                    this._emitter.emit("event", consumerMsg);
                }
                else {
                    promises.push(this._producer.send(sendArgs));
                }
            }
            return Promise.all(promises);
        });
    }
    /**
     * This function going to register a new callback.
     *
     * @param type - Type of messages to listen to.
     * @param callback - Callback to get called when a message arrives.
     * @param _ctx - Log context.
     */
    on(type, callback) {
        /**
         * This function is going to unsubscribe the callback and
         * is just for convenience.
         */
        const unsubscribeFn = () => {
            this.off(type, callback);
        };
        // Register to callback map.
        if (!this._callbacksMap.has(type)) {
            this._callbacksMap.set(type, []);
        }
        this._callbacksMap.get(type).push({
            fn: callback,
        });
        /**
         * Return an unsubscribe function just for convenience.
         */
        return unsubscribeFn;
    }
    /**
     * This function is going to unregister a callback.
     *
     * @param type - Type of messages to unregister the callback from.
     * @param callback - The callback.
     * @param _ctx - Log context.
     */
    off(type, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            // Unregister to callback map.
            if (this._callbacksMap.has(type)) {
                const callbacks = this._callbacksMap.get(type);
                const idx = callbacks.findIndex((aCallback) => aCallback.fn === callback);
                if (idx >= 0) {
                    callbacks.splice(idx, 1);
                }
            }
        });
    }
    /**
     * This function disconnect this client from the remote broker.
     *
     * @param _$ctx - Trace context.
     */
    disconnect(_$ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            return Promise.all([
                this._producer.disconnect(),
                this._consumer.disconnect(),
                this._adminClient.disconnect(),
            ]).then(() => null);
        });
    }
}
/**
 * Topics already created by the clien.
 */
KafkaClient._createdTopicsSet = new Set();
__decorate([
    (0, tracer_1.trace)()
], KafkaClient.prototype, "_init", null);
__decorate([
    (0, tracer_1.trace)()
], KafkaClient.prototype, "_createTopics", null);
__decorate([
    (0, tracer_1.trace)()
], KafkaClient.prototype, "_handleConsumerMessage", null);
__decorate([
    (0, tracer_1.trace)()
], KafkaClient.prototype, "_encodeTopicName", null);
__decorate([
    (0, tracer_1.trace)()
], KafkaClient.prototype, "_decodeTopicName", null);
__decorate([
    (0, tracer_1.trace)()
], KafkaClient.prototype, "createTopic", null);
__decorate([
    (0, tracer_1.trace)()
], KafkaClient.prototype, "send", null);
__decorate([
    (0, tracer_1.trace)()
], KafkaClient.prototype, "disconnect", null);
__decorate([
    (0, tracer_1.trace)()
], KafkaClient, "sharedInit", null);
exports.KafkaClient = KafkaClient;
//# sourceMappingURL=client.js.map