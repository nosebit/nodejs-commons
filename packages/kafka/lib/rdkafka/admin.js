"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const tracer_1 = require("@nosebit/tracer");
const RdKafka = __importStar(require("node-rdkafka"));
//#####################################################
// Main class
//#####################################################
/**
 * This is an admin client that uses RdKafka under the hood.
 */
class KafkaAdminClient {
    /**
     * Constructor.
     *
     * @param client - The kafka client.
     */
    constructor(client) {
        this._adminClient = RdKafka.AdminClient.create({
            "client.id": `${client.id}-admin`,
            "metadata.broker.list": client.brokers.join(","),
        });
    }
    /**
     * This function creates a single topic in kafka.
     *
     * @param topic - Name of topic to create.
     * @param opts - A set of options to control creation.
     * @param opts.partitions - Number of partitions to use.
     * @param opts.replication - The replication factor.
     * @param opts.timeout - Timeout value.
     * @param $ctx - Trace context.
     */
    createTopic(topic, opts = {}, $ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const DEFAULT_TIMEOUT = 30000;
            return new Promise((res, rej) => {
                this._adminClient.createTopic({
                    num_partitions: opts.partitions,
                    replication_factor: opts.replication,
                    topic,
                }, opts.timeout || DEFAULT_TIMEOUT, (error) => {
                    const ALREADY_CREATED_CODE = 36;
                    if (error && error.code !== ALREADY_CREATED_CODE) {
                        $ctx.logger.error(`could not create topic "${topic}"`, error);
                        rej(error);
                        return;
                    }
                    else if (error && error.code === ALREADY_CREATED_CODE) {
                        $ctx.logger.debug(`topic "${topic}" already created`);
                    }
                    res();
                });
            });
        });
    }
    /**
     * This function going to disconnect admin client.
     *
     * @param _$ctx - Log context.
     */
    disconnect(_$ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            return Promise.resolve(this._adminClient.disconnect());
        });
    }
}
__decorate([
    (0, tracer_1.trace)()
], KafkaAdminClient.prototype, "createTopic", null);
__decorate([
    (0, tracer_1.trace)()
], KafkaAdminClient.prototype, "disconnect", null);
exports.default = KafkaAdminClient;
//# sourceMappingURL=admin.js.map