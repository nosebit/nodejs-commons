"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const tracer_1 = require("@nosebit/tracer");
const RdKafka = __importStar(require("node-rdkafka"));
//#####################################################
// Main class
//#####################################################
/**
 * This is a producer which uses RdKafka under ther hood.
 * We use RdKafka for producer because of performance
 * (high throughput when delivering messages).
 */
class KafkaProducer {
    /**
     * Constructor.
     *
     * @param client - The kafka client to use.
     * @param config - Producer config options.
     */
    constructor(client, config = {}) {
        /**
         * Flag indicating if this producer is already connected.
         */
        this._isConnected = false;
        this._client = client;
        this._initialized = this._init(config);
    }
    /**
     * Getter for is isConnected flag.
     */
    get isConnected() {
        return this._isConnected;
    }
    /**
     * Getter for connected promise.
     */
    get connected() {
        return this._connected;
    }
    /**
     * Getter for initialized promise.
     */
    get initialized() {
        return this._initialized;
    }
    /**
     * This function going to initialize the producer.
     *
     * @param config - Producer config options.
     * @param $ctx - Trace context.
     */
    _init(config = {}, $ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const maxRetries = config.maxRetries || 10; // eslint-disable-line @typescript-eslint/no-magic-numbers
            this._producer = new RdKafka.Producer({
                "batch.num.messages": 1000000,
                "client.id": this._client.id,
                "compression.codec": "none",
                "dr_cb": true,
                "log.connection.close": false,
                "message.send.max.retries": maxRetries,
                "metadata.broker.list": this._client.brokers.join(","),
                "queue.buffering.max.messages": 100000,
                "queue.buffering.max.ms": 1000,
                "retry.backoff.ms": 200,
                "socket.keepalive.enable": true,
            });
            try {
                yield this.connect($ctx);
            }
            catch (error) {
                $ctx.logger.error("could not connect");
            }
        });
    }
    /**
     * This function going to connect the underlying producer.
     *
     * @param $ctx - Trace context.
     */
    connect($ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            this._connected = new Promise((res) => {
                this._producer.connect();
                this._producer.on("ready", () => {
                    $ctx.logger.debug("rdkafka producer is ready");
                    this._isConnected = true;
                    res();
                });
                this._producer.on("event.error", (error) => {
                    $ctx.logger.warn("producer error", error);
                });
            });
            return this._connected;
        });
    }
    /**
     * This function going to disconnect this producer.
     *
     * @param $ctx - Trace context.
     */
    disconnect($ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((res) => {
                this._producer.disconnect((error) => {
                    if (error) {
                        $ctx.logger.error("could not disconnect rdkafka producer", error);
                        return;
                    }
                    $ctx.logger.debug("rdkafka producer disconnected");
                    res(null);
                });
            });
        });
    }
    /**
     * This function going to send a new message.
     *
     * @param args - Send arguments.
     * @param $ctx - Trace context.
     */
    send(args, $ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const msgStr = JSON.stringify(args.message);
            const msgBuffer = Buffer.from(msgStr);
            try {
                this._producer.produce(args.topic, args.partition || args.partition, msgBuffer, args.message.type, args.message.timestamp);
                $ctx.logger.debug("message sent using rdkafka producer", args);
            }
            catch (error) {
                $ctx.logger.error("rdkafka producer failed to produce message", error);
            }
        });
    }
}
__decorate([
    (0, tracer_1.trace)()
], KafkaProducer.prototype, "_init", null);
__decorate([
    (0, tracer_1.trace)()
], KafkaProducer.prototype, "connect", null);
__decorate([
    (0, tracer_1.trace)()
], KafkaProducer.prototype, "disconnect", null);
__decorate([
    (0, tracer_1.trace)()
], KafkaProducer.prototype, "send", null);
exports.default = KafkaProducer;
//# sourceMappingURL=producer.js.map