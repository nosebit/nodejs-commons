import { TraceContext } from "@nosebit/tracer";
import { IAdminClient, IKafkaClient } from "../types";
/**
 * This is an admin client that uses RdKafka under the hood.
 */
export default class KafkaAdminClient implements IAdminClient {
    /**
     * Underlying rdkafka admin client.
     */
    private readonly _adminClient;
    /**
     * Constructor.
     *
     * @param client - The kafka client.
     */
    constructor(client: IKafkaClient);
    /**
     * This function creates a single topic in kafka.
     *
     * @param topic - Name of topic to create.
     * @param opts - A set of options to control creation.
     * @param opts.partitions - Number of partitions to use.
     * @param opts.replication - The replication factor.
     * @param opts.timeout - Timeout value.
     * @param $ctx - Trace context.
     */
    createTopic(topic: string, opts?: {
        partitions?: number;
        replication?: number;
        timeout?: number;
    }, $ctx?: TraceContext): Promise<void>;
    /**
     * This function going to disconnect admin client.
     *
     * @param _$ctx - Log context.
     */
    disconnect(_$ctx?: TraceContext): Promise<void>;
}
