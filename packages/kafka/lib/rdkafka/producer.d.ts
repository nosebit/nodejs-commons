import { TraceContext } from "@nosebit/tracer";
import { IKafkaClient, IProducer, IProducerConfig, IProducerSendArgs } from "../types";
/**
 * This is a producer which uses RdKafka under ther hood.
 * We use RdKafka for producer because of performance
 * (high throughput when delivering messages).
 */
export default class KafkaProducer implements IProducer {
    /**
     * Flag indicating if this producer is already connected.
     */
    private _isConnected;
    /**
     * Promise for connected status.
     */
    private _connected;
    /**
     * The kafka client.
     */
    private readonly _client;
    /**
     * Flag indicating if producer is initialized and ready.
     */
    private readonly _initialized;
    /**
     * The underlying rdkafka producer.
     */
    private _producer;
    /**
     * Getter for is isConnected flag.
     */
    get isConnected(): boolean;
    /**
     * Getter for connected promise.
     */
    get connected(): Promise<void>;
    /**
     * Getter for initialized promise.
     */
    get initialized(): Promise<void>;
    /**
     * Constructor.
     *
     * @param client - The kafka client to use.
     * @param config - Producer config options.
     */
    constructor(client: IKafkaClient, config?: IProducerConfig);
    /**
     * This function going to initialize the producer.
     *
     * @param config - Producer config options.
     * @param $ctx - Trace context.
     */
    private _init;
    /**
     * This function going to connect the underlying producer.
     *
     * @param $ctx - Trace context.
     */
    connect($ctx?: TraceContext): Promise<void>;
    /**
     * This function going to disconnect this producer.
     *
     * @param $ctx - Trace context.
     */
    disconnect($ctx?: TraceContext): Promise<void>;
    /**
     * This function going to send a new message.
     *
     * @param args - Send arguments.
     * @param $ctx - Trace context.
     */
    send(args: IProducerSendArgs, $ctx?: TraceContext): Promise<void>;
}
