# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.7.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.7.3...@nosebit/kafka@0.7.4) (2022-04-26)

**Note:** Version bump only for package @nosebit/kafka





## [0.7.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.7.2...@nosebit/kafka@0.7.3) (2022-02-09)

**Note:** Version bump only for package @nosebit/kafka





## [0.7.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.7.1...@nosebit/kafka@0.7.2) (2022-01-09)


### Bug Fixes

* **kafka:** make kafka using node emitter to work exactly the same as kafka consumer/producer ([5bb6f1e](https://github.com/nosebit/nodejs-commons/commit/5bb6f1eb4d90805e782a8062fb2fd1dee634cf88))





## [0.7.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.7.0...@nosebit/kafka@0.7.1) (2022-01-07)

**Note:** Version bump only for package @nosebit/kafka





# [0.7.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.6.0...@nosebit/kafka@0.7.0) (2021-11-16)


### Features

* **kafka:** add off method and return unsubscribe method from on method ([31b71f2](https://github.com/nosebit/nodejs-commons/commit/31b71f21fdffdbd3929b8d03160bc203495f45a0))





# [0.6.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.5.5...@nosebit/kafka@0.6.0) (2021-11-14)


### Features

* **kafka:** add namespace and catch some connect errors ([c165239](https://github.com/nosebit/nodejs-commons/commit/c16523972f3bc4705375f1c1b212fe001e4d4f0d))





## [0.5.5](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.5.4...@nosebit/kafka@0.5.5) (2021-10-09)

**Note:** Version bump only for package @nosebit/kafka





## [0.5.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.5.2...@nosebit/kafka@0.5.4) (2021-10-09)

**Note:** Version bump only for package @nosebit/kafka





## [0.5.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.5.2...@nosebit/kafka@0.5.3) (2021-10-09)

**Note:** Version bump only for package @nosebit/kafka





## [0.5.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.5.1...@nosebit/kafka@0.5.2) (2021-10-07)

**Note:** Version bump only for package @nosebit/kafka





## [0.5.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.5.0...@nosebit/kafka@0.5.1) (2021-10-07)

**Note:** Version bump only for package @nosebit/kafka





# [0.5.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.4.4...@nosebit/kafka@0.5.0) (2021-10-07)


### Bug Fixes

* **kafka:** make trace context optional in createTopic function ([b39a6a8](https://github.com/nosebit/nodejs-commons/commit/b39a6a87cf78dca3bc8387f67febc3cd08903646))


### Features

* **kafka:** use new tracer instead of logger ([43efff4](https://github.com/nosebit/nodejs-commons/commit/43efff45ee2e7b59cee50c2b281483905638acd4))





## [0.4.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.4.3...@nosebit/kafka@0.4.4) (2021-09-12)

**Note:** Version bump only for package @nosebit/kafka





## [0.4.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.4.2...@nosebit/kafka@0.4.3) (2021-09-11)

**Note:** Version bump only for package @nosebit/kafka





## [0.4.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.4.1...@nosebit/kafka@0.4.2) (2021-09-11)

**Note:** Version bump only for package @nosebit/kafka





## [0.4.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.4.0...@nosebit/kafka@0.4.1) (2021-07-14)


### Bug Fixes

* **kafka:** add missing initialized getter to kafkajs consumer ([41d00c8](https://github.com/nosebit/nodejs-commons/commit/41d00c8021149ab94b2dabaa069a7effb2f300ad))
* **kafka:** add missing initialized getter to rdkafka consumer ([1c12702](https://github.com/nosebit/nodejs-commons/commit/1c12702a8bcab66d69ea7a875f75d2db6e24cb50))
* **kafka:** add missing shared getter ([85ae237](https://github.com/nosebit/nodejs-commons/commit/85ae237682413b4ce96fe1468d2c682391631e9f))





# [0.4.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.3.4...@nosebit/kafka@0.4.0) (2021-07-08)


### Bug Fixes

* **kafka:** use kafkajs to consume messages ([8175907](https://github.com/nosebit/nodejs-commons/commit/8175907bbe8e6116dab99ee8e58c5b5ba083a466))
* **thrift:** fix build ([c6e794a](https://github.com/nosebit/nodejs-commons/commit/c6e794a6969fed494bf8e288f5f6379ca7355e1a))


### BREAKING CHANGES

* **kafka:** Config options to kafka client changed.





## [0.3.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.3.3...@nosebit/kafka@0.3.4) (2021-06-12)

**Note:** Version bump only for package @nosebit/kafka





## [0.3.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.3.2...@nosebit/kafka@0.3.3) (2021-06-09)

**Note:** Version bump only for package @nosebit/kafka





## [0.3.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.3.1...@nosebit/kafka@0.3.2) (2021-06-09)

**Note:** Version bump only for package @nosebit/kafka





## [0.3.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.3.0...@nosebit/kafka@0.3.1) (2020-10-26)


### Bug Fixes

* **kafka:** does not create topics by default ([f7d9eaf](https://github.com/nosebit/nodejs-commons/commit/f7d9eaf886a9096e3e3fb016316b1c42638ab235))
* **kafka:** log warn instead of error on consumer error event ([16e40d0](https://github.com/nosebit/nodejs-commons/commit/16e40d01a19e840b3e89183c57aa3d512915d432))





# [0.3.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.2.2...@nosebit/kafka@0.3.0) (2020-10-26)


### Features

* **kafka:** create topics when emitting and subscribe to topics when registering listeners ([c3e47b3](https://github.com/nosebit/nodejs-commons/commit/c3e47b3373d8ae5707e0a7d0dd2dec4d92e55f7c))





## [0.2.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.2.1...@nosebit/kafka@0.2.2) (2020-10-15)

**Note:** Version bump only for package @nosebit/kafka





## [0.2.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.2.0...@nosebit/kafka@0.2.1) (2020-10-15)

**Note:** Version bump only for package @nosebit/kafka





# [0.2.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.1.7...@nosebit/kafka@0.2.0) (2020-10-14)


### Features

* **kafka:** add ability to create topics on the fly ([a7b9725](https://github.com/nosebit/nodejs-commons/commit/a7b9725d2a4129395ab76ef560897b289ed9e4e1))





## [0.1.7](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.1.6...@nosebit/kafka@0.1.7) (2020-10-12)

**Note:** Version bump only for package @nosebit/kafka





## [0.1.6](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.1.5...@nosebit/kafka@0.1.6) (2020-10-12)

**Note:** Version bump only for package @nosebit/kafka





## [0.1.5](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.1.4...@nosebit/kafka@0.1.5) (2020-10-10)

**Note:** Version bump only for package @nosebit/kafka





## [0.1.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.1.3...@nosebit/kafka@0.1.4) (2020-09-24)

**Note:** Version bump only for package @nosebit/kafka





## [0.1.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.1.2...@nosebit/kafka@0.1.3) (2020-09-12)

**Note:** Version bump only for package @nosebit/kafka





## [0.1.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.1.1...@nosebit/kafka@0.1.2) (2020-05-13)

**Note:** Version bump only for package @nosebit/kafka





## [0.1.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/kafka@0.1.0...@nosebit/kafka@0.1.1) (2020-04-19)

**Note:** Version bump only for package @nosebit/kafka





# 0.1.0 (2020-04-19)


### Bug Fixes

* Use kafka ConsumerStreamMessage instead of kafka Message type which was breaking lerna build. ([3840f60](https://github.com/nosebit/nodejs-commons/commit/3840f6050ecd39c0bf24d718cc3d44c3dfc3de7a))


### Features

* New npm package names removing nodejs-* prefix and adding mongo and thrift mock packages. ([461df10](https://github.com/nosebit/nodejs-commons/commit/461df1099b71f838b046a60f2d232e891a65f666))
