import JaegerTracer from "./tracers/jaeger";
/**
 * The unit of tracing.
 */
export interface ITracerSpan {
    finish(finishTime?: number): void;
}
/**
 * The options to create spans.
 */
export interface ITracerSpanConfig {
    childOf?: ITracerSpan | object;
}
/**
 * The tracer interface.
 */
export interface ITracer {
    startSpan(operationName: string, config?: ITracerSpanConfig): ITracerSpan;
    getSpanFromId(id: string): ITracerSpan | object;
    getTraceId(span: ITracerSpan): string;
}
/**
 * Options for trace method.
 */
interface ITraceMethodConfig {
    allowExternalTracing?: boolean;
    args?: any[];
    argsToLog?: string[];
    fileName?: string;
    methodId: string;
    parentCtx: LogContext;
    preventTracing?: boolean;
    resultToLog?: string[] | boolean;
}
/**
 * Logger driver interface
 */
export interface ILoggerDriver {
    log(level: string, message: string, meta?: any): void;
}
/**
 * Log level type.
 */
export declare type TLogLevel = "error" | "warn" | "info" | "debug" | "verbose";
/**
 * The logger config
 */
interface ILoggerConfig {
    driver?: ILoggerDriver;
    fileName?: string;
    level?: TLogLevel;
    tracer?: ITracer;
    traceInfo?: TraceInfo;
}
/**
 * Log method config options.
 */
interface ILogMeta {
    fileName?: string;
}
/**
 * Interface for logger meta info.
 */
interface ILoggerMeta {
    traceId: string;
}
/**
 * This is the main class which is used to log and trace
 * a piece of code.
 */
declare class Logger {
    /**
     * Shared instance.
     */
    private static _shared;
    /**
     * Shared service name.
     */
    private static _sharedServiceName;
    /**
     * Shared service color.
     */
    private static _sharedServiceColor;
    /**
     * Shared tracer.
     */
    private static _sharedTracer;
    /**
     * Available log levels.
     */
    private static readonly _logLevels;
    /**
     * Shared log level.
     */
    private static _sharedLogLevel;
    /**
     * Instance log level.
     */
    private readonly _logLevel;
    /**
     * This function initializes a shared logger.
     *
     * @param config - A set of configs.
     * @param config.serviceName - The service name owning the logger factory.
     * @param config.serviceColor - A color to be used for logging the service name.
     * @param config.level - The logging level for the shared logged itself.
     * @param config.sharedLevel - The logging level for all created loggers.
     * @param config.tracer - The tracer.
     */
    static sharedInit(config?: {
        serviceName: string;
        serviceColor?: string;
        level?: TLogLevel;
        tracer?: ITracer;
    }): Logger;
    /**
     * This function get the shared service name.
     */
    static get serviceName(): string;
    /**
     * This function get the shared service name.
     */
    static get serviceColor(): string;
    /**
     * This function retrieves the global service name.
     */
    static get shared(): Logger;
    /**
     * This function checks if we have a shared logger.
     */
    static sharedExists(): boolean;
    /**
     * This function retrieves the shared tracer.
     */
    static get tracer(): ITracer;
    /**
     * This function decode a trace id string in it's parts.
     *
     * @param  id - The trace id to decode.
     */
    static decodeTraceId(id: string): {
        contextId: string;
        stackId: string;
    } | {
        contextId?: undefined;
        stackId?: undefined;
    };
    /**
     * This function decode a trace id string in it's parts.
     *
     * @param contextId - The trace id to decode.
     * @param stackId - The stack id.
     */
    static encodeTraceIds(contextId?: string, stackId?: string): string;
    /**
     * This is the scope id for this logger.
     */
    readonly scopeId?: string;
    /**
     * The fileName to be used in all logging.
     */
    readonly fileName?: string;
    /**
     * Entity which drives the logging.
     */
    readonly driver: ILoggerDriver;
    /**
     * Entity which drives the tracing.
     */
    readonly tracer: ITracer;
    /**
     * Trace info for this logger.
     */
    readonly traceInfo: TraceInfo;
    /**
     * This is a getter to retrieve correct trace id from this logger trace context.
     */
    get traceId(): string;
    /**
     * Creates an instance of Logger.
     *
     * @param scopeId - Name identifying the scope of logging.
     * @param config - A set of config options.
     */
    constructor(scopeId: string, config?: ILoggerConfig);
    /**
     * This function implements a generic log function which is
     * the base to all other methods.
     *
     * @param level - The log level (info, error, etc).
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of metadata.
     */
    private _log;
    /**
     * This function creates a debug log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    debug(message: string, data?: any, meta?: ILogMeta): void;
    /**
     * This function creates a verbose log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    verbose(message: string, data?: any, meta?: ILogMeta): void;
    /**
     * This function creates an info log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    info(message: string, data?: any, meta?: ILogMeta): void;
    /**
     * This function creates a warning log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    warn(message: string, data?: any, meta?: ILogMeta): void;
    /**
     * This function creates an error log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    error(message: string, data?: any, meta?: ILogMeta): void;
    /**
     * This function emits a critical error log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     */
    critical(message: string, data?: any): void;
    /**
     * This function creates a new logger based on this logger.
     *
     * @param scopeId - The scope of new logger.
     */
    fork(scopeId: string): Logger;
}
/**
 * The trace info.
 */
declare class TraceInfo {
    /**
     * A string identifying the context stack.
     */
    stackId?: string;
    /**
     * A string identifying the tracer context.
     */
    id?: string;
    /**
     * The method name associated with this context.
     */
    method?: string;
    /**
     * The span associated with this context.
     */
    span?: ITracerSpan;
    /**
     * The parent context in the stack.
     */
    parent?: TraceInfo;
    /**
     * Creates an instance of TraceInfo.
     */
    constructor();
}
/**
 * The log context info is what gets passed along functions
 * for logging and tracing.
 */
declare class LogContext<T extends ILoggerMeta = any> {
    /**
     * This function creates a log context from a trace id.
     *
     * @param traceId - The trace id string.
     * @param baseCtx - A base context to be used.
     */
    static fromTraceId(traceId: string, baseCtx?: LogContext): LogContext<any>;
    /**
     * This function creates a log context from a logger meta.
     *
     * @param meta - The trace id string.
     * @param baseCtx - A base context to be used.
     */
    static fromMeta(meta: ILoggerMeta, baseCtx?: LogContext): LogContext<any>;
    /**
     * This functions only return data if we are in envorionments other
     * than production.
     *
     * @param data - The secret data to log if we are allowed to (dev like envs).
     */
    static secret<TT>(data: TT): TT | string;
    /**
     * This function is the main log function and it's used to trace
     * execution of a piece of code.
     *
     * @param config - The trace config.
     * @param methodToTrace - The method to be traced.
     */
    static trace<T1 = any, T2 extends ILoggerMeta = any>(config: ITraceMethodConfig, methodToTrace: (ctx: LogContext) => T1): T1;
    /**
     * The trace info.
     */
    traceInfo: TraceInfo;
    /**
     * The logger for the context.
     */
    logger: Logger;
    /**
     * Meta data.
     */
    meta: T;
    /**
     * This getter retrives the traceId from traceInfo.
     */
    get traceId(): string;
    /**
     * This function going to clone this context mergin it with
     * some provided data.
     */
    clone(): LogContext<any>;
}
/**
 * This function decorates a method so it can be logged.
 *
 * @param config - Config options.
 * @param config.allowExternalTracing - Flag to disable external tracers like jaeger.
 * @param config.argsToLog - A list of received arguments we want to log at function call.
 * @param config.ctxArgIdx - Fix argument index where log context should be passed in (useful when we have optional args).
 * @param config.loggerPrefix - A function to put prefix to log message.
 * @param config.preventTracing - Disable all tracing logs (start/end of function call).
 * @param config.resultToLog - A list of results to log (lodash syntax) or false to disable logging function returned value.
 * @param config.skipArgs - A list of argument index to skip during logging.
 */
declare function log<T extends ILoggerMeta = any>(config?: {
    allowExternalTracing?: boolean;
    argsToLog?: string[];
    ctxArgIdx?: number;
    loggerPrefix?: (tracerBag: object) => string;
    preventTracing?: boolean;
    resultToLog?: string[] | boolean;
    skipArgs?: number[];
}): (...decArgs: any[]) => void;
declare const globalCtx: LogContext<any>;
export { globalCtx, JaegerTracer, log, LogContext, Logger, TraceInfo, };
