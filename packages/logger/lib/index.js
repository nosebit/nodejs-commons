"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TraceInfo = exports.Logger = exports.LogContext = exports.log = exports.JaegerTracer = exports.globalCtx = void 0;
/* eslint-disable max-classes-per-file */
const utils_1 = require("@nosebit/utils");
const node_1 = __importDefault(require("@sentry/node"));
const lodash_1 = __importDefault(require("lodash"));
const moment_1 = __importDefault(require("moment"));
const shortid_1 = __importDefault(require("shortid"));
const uuid_1 = require("uuid");
const winston_1 = __importDefault(require("./drivers/winston"));
const jaeger_1 = __importDefault(require("./tracers/jaeger"));
exports.JaegerTracer = jaeger_1.default;
//#####################################################
// Auxiliary Functions
//#####################################################
/**
 * This function going to build a map to log with function
 * arguments.
 *
 * @param args - The function arguments.
 * @param argsToLog - A list of arg spec which we should use
 * to log the arg.
 */
function argsMapToLog(args = [], argsToLog) {
    const argsMap = {};
    lodash_1.default.forEach(argsToLog, (argSpec, idx) => {
        const argValue = lodash_1.default.get(args, `${idx}`);
        if (argSpec === "_" || lodash_1.default.isFunction(argValue)) {
            return;
        }
        const dotIdx = argSpec.indexOf(".");
        let argName = null;
        let argValPaths = [];
        let argValueToLog = argValue;
        if (dotIdx > 0) {
            argName = argSpec.substr(0, dotIdx);
            const argSpecVal = argSpec.substr(dotIdx + 1);
            const multiValPathsMatch = (/{[^{}]+}/).exec(argSpecVal);
            if (multiValPathsMatch) {
                const valPathsStr = multiValPathsMatch[0].replace(/{|}/g, "");
                argValPaths = valPathsStr.split(/, ?/g);
            }
        }
        else {
            argName = argSpec;
        }
        if (argValPaths.length > 0) {
            argValueToLog = {};
            argValPaths.forEach((valPathWithOpt) => {
                // Check for options
                const valPathParts = valPathWithOpt.split(":");
                const valPath = valPathParts.length ? valPathParts[0] : null;
                const valPathOpt = valPathParts.length ? valPathParts[1] : null;
                if (valPathOpt !== "secret" || process.env.LOG_SECRETS) {
                    lodash_1.default.set(argValueToLog, valPath, lodash_1.default.get(argValue, valPath));
                }
            });
        }
        argsMap[argName] = argValueToLog;
    });
    return argsMap;
}
/**
 * Get caller.
 */
function getCallerFile() {
    const originalFunc = Error.prepareStackTrace;
    let callerfile;
    try {
        const error = new Error();
        Error.prepareStackTrace = function prepareStackTrace(_error, stack) {
            return stack;
        };
        const stack = error.stack; // eslint-disable-line prefer-destructuring
        const currentfile = stack.shift().getFileName();
        while (error.stack.length) {
            callerfile = stack.shift().getFileName();
            if (currentfile !== callerfile) {
                break;
            }
        }
    }
    catch (error) { }
    Error.prepareStackTrace = originalFunc;
    return callerfile;
}
//#####################################################
// Main class
//#####################################################
/**
 * This is the main class which is used to log and trace
 * a piece of code.
 */
class Logger {
    /**
     * Creates an instance of Logger.
     *
     * @param scopeId - Name identifying the scope of logging.
     * @param config - A set of config options.
     */
    constructor(scopeId, config = {}) {
        this.scopeId = scopeId;
        this.fileName = config.fileName;
        this.traceInfo = config.traceInfo;
        this._logLevel = config.level || Logger._sharedLogLevel;
        this.driver = config.driver
            || (0, winston_1.default)();
        this.tracer = config.tracer;
    }
    /**
     * This function initializes a shared logger.
     *
     * @param config - A set of configs.
     * @param config.serviceName - The service name owning the logger factory.
     * @param config.serviceColor - A color to be used for logging the service name.
     * @param config.level - The logging level for the shared logged itself.
     * @param config.sharedLevel - The logging level for all created loggers.
     * @param config.tracer - The tracer.
     */
    static sharedInit(config = {
        serviceName: "",
    }) {
        if (!Logger._shared) {
            Logger._sharedServiceName = config.serviceName;
            Logger._sharedServiceColor = config.serviceColor || (0, utils_1.getRandomHexColor)();
            Logger._sharedTracer = config.tracer;
            Logger._sharedLogLevel = config.level
                || process.env.LOG_LEVEL
                || "error";
            Logger._shared = new Logger("<ROOT>");
            if (process.env.SENTRY_URL) {
                node_1.default.init({
                    dsn: process.env.SENTRY_URL,
                });
            }
        }
        return Logger._shared;
    }
    /**
     * This function get the shared service name.
     */
    static get serviceName() {
        return Logger._sharedServiceName;
    }
    /**
     * This function get the shared service name.
     */
    static get serviceColor() {
        return Logger._sharedServiceColor;
    }
    /**
     * This function retrieves the global service name.
     */
    static get shared() {
        if (!Logger._shared) {
            throw new Error("shared logger not initialized");
        }
        return Logger._shared;
    }
    /**
     * This function checks if we have a shared logger.
     */
    static sharedExists() {
        return Boolean(Logger._shared);
    }
    /**
     * This function retrieves the shared tracer.
     */
    static get tracer() {
        return Logger._sharedTracer;
    }
    /**
     * This function decode a trace id string in it's parts.
     *
     * @param  id - The trace id to decode.
     */
    static decodeTraceId(id) {
        // Check if string is a trace id.
        if (id.startsWith("@TRACE_ID_")) {
            const ids = id.replace(/^@TRACE_ID_/, "").split(/_@STACK_ID_/);
            return {
                contextId: ids.length ? ids[0] : null,
                stackId: ids.length > 1 ? ids[1] : null,
            };
        }
        return {};
    }
    /**
     * This function decode a trace id string in it's parts.
     *
     * @param contextId - The trace id to decode.
     * @param stackId - The stack id.
     */
    static encodeTraceIds(contextId = (0, uuid_1.v4)(), stackId = shortid_1.default.generate()) {
        return `@TRACE_ID_${contextId}_@STACK_ID_${stackId || ""}`;
    }
    /**
     * This is a getter to retrieve correct trace id from this logger trace context.
     */
    get traceId() {
        let traceId;
        if (this.traceInfo) {
            const contextId = this.tracer.getTraceId(this.traceInfo.span);
            const { stackId } = this.traceInfo;
            traceId = Logger.encodeTraceIds(contextId, stackId);
        }
        else {
            // Set random trace id.
            traceId = Logger.encodeTraceIds((0, uuid_1.v4)());
        }
        return traceId;
    }
    /**
     * This function implements a generic log function which is
     * the base to all other methods.
     *
     * @param level - The log level (info, error, etc).
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of metadata.
     */
    _log(level, message, data, meta = {}) {
        // Check if we going to log.
        const currentLevelIdx = Logger._logLevels.indexOf(this._logLevel);
        const levelIdx = Logger._logLevels.indexOf(level);
        if (levelIdx < 0 || levelIdx > currentLevelIdx) {
            return;
        }
        // Get caller filepath.
        let { fileName } = meta;
        // Prepend service name to filepath.
        if (fileName) {
            fileName = `${fileName.replace(/^\//, "")}`;
        }
        // Parse message
        const scopedMessage = this.scopeId
            ? `${this.scopeId} : ${message}`
            : message;
        // Try to convert data to plain object.
        let plainData;
        try {
            plainData = JSON.parse(JSON.stringify(data));
        }
        catch (error) {
            // Could not convert data to plain object.
        }
        this.driver.log(level, scopedMessage, {
            data: plainData,
            serviceColor: Logger.serviceColor,
            serviceName: Logger.serviceName,
            trace: {
                fileName,
                stackId: this.traceInfo ? this.traceInfo.stackId : null,
            },
        });
    }
    /**
     * This function creates a debug log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    debug(message, data, meta) {
        this._log("debug", message, data, meta);
    }
    /**
     * This function creates a verbose log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    verbose(message, data, meta) {
        this._log("verbose", message, data, meta);
    }
    /**
     * This function creates an info log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    info(message, data, meta) {
        this._log("info", message, data, meta);
    }
    /**
     * This function creates a warning log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    warn(message, data, meta) {
        this._log("warn", message, data, meta);
    }
    /**
     * This function creates an error log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    error(message, data, meta) {
        this._log("error", message, data, meta);
    }
    /**
     * This function emits a critical error log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     */
    critical(message, data) {
        this._log("error", message, data);
        if (node_1.default) {
            node_1.default.captureException({ data, message });
        }
    }
    /**
     * This function creates a new logger based on this logger.
     *
     * @param scopeId - The scope of new logger.
     */
    fork(scopeId) {
        return new Logger(`${this.scopeId}.${scopeId}`);
    }
}
exports.Logger = Logger;
/**
 * Available log levels.
 */
Logger._logLevels = [
    "error",
    "warn",
    "info",
    "debug",
    "verbose",
];
//#####################################################
// Auxiliary Classes
//#####################################################
/**
 * The trace info.
 */
class TraceInfo {
    /**
     * Creates an instance of TraceInfo.
     */
    constructor() {
        this.id = `@TRACE_ID_${(0, uuid_1.v4)()}`;
        this.stackId = (0, shortid_1.default)();
    }
}
exports.TraceInfo = TraceInfo;
/**
 * The log context info is what gets passed along functions
 * for logging and tracing.
 */
class LogContext {
    /**
     * This function creates a log context from a trace id.
     *
     * @param traceId - The trace id string.
     * @param baseCtx - A base context to be used.
     */
    static fromTraceId(traceId, baseCtx) {
        let ctx;
        if (baseCtx) {
            ctx = baseCtx.clone();
        }
        else {
            ctx = new LogContext();
            ctx.traceInfo = new TraceInfo();
        }
        ctx.traceInfo.id = traceId;
        return ctx;
    }
    /**
     * This function creates a log context from a logger meta.
     *
     * @param meta - The trace id string.
     * @param baseCtx - A base context to be used.
     */
    static fromMeta(meta, baseCtx) {
        let ctx;
        if (baseCtx) {
            ctx = baseCtx.clone();
        }
        else {
            ctx = new LogContext();
            ctx.traceInfo = new TraceInfo();
        }
        ctx.traceInfo.id = meta.traceId;
        return ctx;
    }
    /**
     * This functions only return data if we are in envorionments other
     * than production.
     *
     * @param data - The secret data to log if we are allowed to (dev like envs).
     */
    static secret(data) {
        return process.env.LOG_SECRETS
            ? "<SECRET>"
            : data;
    }
    /**
     * This function is the main log function and it's used to trace
     * execution of a piece of code.
     *
     * @param config - The trace config.
     * @param methodToTrace - The method to be traced.
     */
    static trace(config, methodToTrace) {
        /**
         * If we don't want any tracing functionality then we can just create
         * a simple context which contains just a logger.
         */
        if (config.preventTracing) {
            const simpleCtx = new LogContext();
            simpleCtx.traceInfo = new TraceInfo();
            simpleCtx.traceInfo.id = `@TRACE_ID_${(0, uuid_1.v4)()}`;
            simpleCtx.traceInfo.stackId = shortid_1.default.generate();
            simpleCtx.logger = new Logger(config.methodId); // eslint-disable-line no-use-before-define
            return methodToTrace(simpleCtx);
        }
        const spanLabel = config.fileName
            ? `${config.fileName}:${config.methodId}`
            : config.methodId;
        // Create a new context info.
        const ctx = new LogContext();
        // Create new trace info
        const info = new TraceInfo();
        ctx.traceInfo = info;
        // Set context meta from parent
        if (config.parentCtx && config.parentCtx.meta) {
            ctx.meta = config.parentCtx.meta;
        }
        // Set parent context
        if (config.parentCtx && config.parentCtx.traceInfo) {
            info.parent = config.parentCtx.traceInfo;
            info.stackId = `${info.parent.stackId}/${shortid_1.default.generate()}`;
        }
        else {
            info.stackId = shortid_1.default.generate();
        }
        // Set trace method name
        info.method = config.methodId;
        // Set parent method
        const parentMethod = info.parent && info.parent.method
            ? info.parent.method
            : "<ROOT>";
        // Start an internal timer.
        const start = (0, moment_1.default)();
        // Log start
        const fromStr = parentMethod
            ? `(from: ${parentMethod})`
            : null;
        // Start the trace span.
        if (config.allowExternalTracing && Logger.tracer) {
            info.span = Logger.tracer.startSpan(spanLabel, {
                childOf: info.parent
                    ? info.parent.span
                    : Logger.tracer.getSpanFromId(config.parentCtx && config.parentCtx.traceId),
            });
            // Set trace info id.
            info.id = `@TRACE_ID_${Logger.tracer.getTraceId(info.span)}`;
        }
        else {
            // Set random trace id.
            info.id = `@TRACE_ID_${(0, uuid_1.v4)()}`;
        }
        // Creates a logger for the method context.
        const logger = new Logger(info.method, { traceInfo: info }); // eslint-disable-line no-use-before-define
        ctx.logger = logger;
        /**
         * Get args that should be logged and create log map. Suppose we have a
         * function with signature myFun(arg1, ..., argN). Then this trace function
         * can receive the list of arguments as args=[arg1, ..., argN] and a list
         * argsToLog mapping which arguments we want to log. For example:
         *
         * - `argsToLog=[arg1Name]` : Going to log just the first argument using
         * arg1Name as name (prints an object with arg1Name:arg1 kv pair);
         * - `argsToLog=["_", arg2Name]` : Going to log just the second argument.
         * - `argsToLog=[arg1Name, "arg2.subarg1"]` : Going to log just the subarg1 key
         * of arg2 object.
         * - `argsToLog=[arg1Name, "arg2.{subarg1,subarg2}"]` : Going to log just
         * subarg1 and subarg 2 of arg2 object.
         */
        const argsMap = argsMapToLog(config.args, config.argsToLog);
        const meta = {};
        if (config.fileName) {
            meta.fileName = config.fileName;
        }
        // Log start of method
        logger.verbose(`start ${fromStr}`, argsMap, meta);
        // Method response
        const response = methodToTrace(ctx);
        /**
         * Encapsulate finishignin trace into a function to deal with promises.
         *
         * @param result - The results recieved.
         */
        const finish = (result) => {
            // Finish the span
            if (info.span) {
                info.span.finish();
            }
            // Stop the internal timer
            const end = (0, moment_1.default)();
            const duration = moment_1.default.duration(end.diff(start)).as("milliseconds");
            // Build result to log.
            let parsedResult;
            let resultToLog;
            // Try to convert result to a plain object.
            try {
                parsedResult = JSON.parse(JSON.stringify(result));
            }
            catch (error) {
                parsedResult = result;
            }
            if (lodash_1.default.isArray(config.resultToLog)) {
                resultToLog = argsMapToLog(parsedResult, config.resultToLog);
            }
            // Log success
            if (lodash_1.default.isBoolean(config.resultToLog) && !config.resultToLog) {
                logger.verbose(`success (duration: ${duration}ms)`, null, meta);
            }
            else {
                logger.verbose(`success (duration: ${duration}ms)`, resultToLog, meta);
            }
            return result;
        };
        // Method returns a promise.
        if (response instanceof Promise) {
            response
                .then(finish)
                .catch((error) => {
                // Stop the internal timer
                const end = (0, moment_1.default)();
                const duration = moment_1.default.duration(end.diff(start)).as("milliseconds");
                // Log error
                logger.error(`fail (duration: ${duration}ms)`, error.message, { fileName: config.fileName });
                return error;
            });
        }
        else {
            finish(response);
        }
        return response;
    }
    /**
     * This getter retrives the traceId from traceInfo.
     */
    get traceId() {
        return this.traceInfo && this.traceInfo.id;
    }
    /**
     * This function going to clone this context mergin it with
     * some provided data.
     */
    clone() {
        const cloneCtx = new LogContext();
        cloneCtx.logger = this.logger;
        cloneCtx.traceInfo = new TraceInfo();
        Object.assign(cloneCtx.traceInfo, this.traceInfo);
        return cloneCtx;
    }
}
exports.LogContext = LogContext;
//#####################################################
// Decorators
//#####################################################
/**
 * This function decorates a method so it can be logged.
 *
 * @param config - Config options.
 * @param config.allowExternalTracing - Flag to disable external tracers like jaeger.
 * @param config.argsToLog - A list of received arguments we want to log at function call.
 * @param config.ctxArgIdx - Fix argument index where log context should be passed in (useful when we have optional args).
 * @param config.loggerPrefix - A function to put prefix to log message.
 * @param config.preventTracing - Disable all tracing logs (start/end of function call).
 * @param config.resultToLog - A list of results to log (lodash syntax) or false to disable logging function returned value.
 * @param config.skipArgs - A list of argument index to skip during logging.
 */
function log(config = {}) {
    return function decorator(...decArgs) {
        if (decArgs.length !== 3 || typeof decArgs[2] === "number") {
            throw new Error("Method does not allow decoration");
        }
        /* eslint-disable prefer-destructuring */
        const target = decArgs[0];
        const descriptor = decArgs[2];
        /* eslint-enable prefer-destructuring */
        const originalMethod = descriptor.value;
        const methodName = originalMethod.name;
        descriptor.value = function value(...args) {
            if (!Logger.sharedExists()) {
                return originalMethod.apply(this, args);
            }
            let fileName = getCallerFile();
            if (fileName) {
                fileName = fileName.replace(`${process.cwd()}/`, "");
            }
            const className = target.constructor.name;
            // Try to get parent log context passed through.
            const lastArg = lodash_1.default.last(args);
            let parentCtx;
            if (lastArg instanceof LogContext) {
                parentCtx = args.pop();
            }
            return LogContext.trace({
                allowExternalTracing: config.allowExternalTracing,
                args,
                argsToLog: config.argsToLog,
                fileName,
                methodId: `${className}.${methodName}`,
                parentCtx,
                preventTracing: config.preventTracing,
            }, (ctx) => {
                let childArgs = args;
                /**
                 * If user provided a specific position for context, then
                 * let's adjust args to be sure ctx going to be in right
                 * index. This is important when method has optional args
                 * with default values.
                 */
                if (config.ctxArgIdx > 0 && args.length < config.ctxArgIdx) {
                    childArgs = [
                        ...args,
                        ...(new Array(config.ctxArgIdx - args.length)), // eslint-disable-line @typescript-eslint/no-extra-parens
                    ];
                }
                // Push log context into the args.
                childArgs.push(ctx);
                return originalMethod.apply(this, childArgs);
            });
        };
    };
}
exports.log = log;
//#####################################################
// Global default context
//#####################################################
const globalCtx = new LogContext();
exports.globalCtx = globalCtx;
globalCtx.logger = new Logger("global");
//# sourceMappingURL=index.js.map