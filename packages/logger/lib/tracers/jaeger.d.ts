import { ITracer, ITracerSpan, ITracerSpanConfig } from "..";
interface IJaegerTracerConfig {
    host?: string;
    serviceName: string;
}
/**
 * This class implements a jaeger tracer.
 */
declare class JaegerTracer implements ITracer {
    /**
     * The jaeger driver (or client).
     */
    private readonly _driver;
    /**
     * This function creates a new jaeger tracer instance.
     *
     * @param config - The config options.
     */
    constructor(config: IJaegerTracerConfig);
    /**
     * This function starts a new span.
     *
     * @param operationName - Name of the operation to be traced.
     * @param config - A set of config options.
     */
    startSpan(operationName: string, config?: ITracerSpanConfig): ITracerSpan;
    /**
     * This function build a span object from context id.
     *
     * @param id - The context id.
     */
    getSpanFromId(id: string): ITracerSpan | object;
    /**
     * This function extract the trace id from the span object.
     *
     * @param span - The span object.
     */
    getTraceId(span: ITracerSpan): string;
}
export { JaegerTracer as default, };
