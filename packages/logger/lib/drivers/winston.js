"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = void 0;
//#####################################################
// Imports
//#####################################################
const utils_1 = require("@nosebit/utils");
const chalk_1 = __importDefault(require("chalk"));
const moment_1 = __importDefault(require("moment"));
const winston = __importStar(require("winston"));
const winston_logstash_transport_1 = require("winston-logstash-transport");
//#####################################################
// Local variables
//#####################################################
// The global winston driver
let driver;
//#####################################################
// Main function
//#####################################################
/**
 * This function retreives a global winston driver.
 *
 * @param opts - A set of config options.
 */
function winstonDriver(opts = {}) {
    if (driver) {
        return driver;
    }
    // Ignore log messages if the have { private: true }
    const ignorePrivate = winston.format((info) => {
        if (info.private) {
            return false;
        }
        return info;
    });
    const { combine, timestamp, printf, colorize, } = winston.format;
    // Create winston logger
    driver = winston.createLogger({
        format: winston.format.json(),
        /**
         * We going to handle level in our logger instead of in
         * winston so that we can have a winston singleton driver.
         */
        level: opts.level || "silly",
    });
    if (!process.env.LOG_SILENT) {
        driver.add(new winston.transports.Console({
            format: combine(colorize(), ignorePrivate(), timestamp(), printf((info) => {
                let dataStr = "";
                let traceStr = "";
                if (info.data) {
                    try {
                        dataStr = JSON.stringify(info.data);
                    }
                    catch (error) {
                        dataStr = info.data;
                    }
                    if (dataStr && dataStr.length) {
                        dataStr = ` - ${dataStr}`;
                    }
                }
                if (info.trace && info.trace.stackId) {
                    try {
                        traceStr = JSON.stringify(info.trace);
                    }
                    catch (error) {
                        traceStr = info.trace;
                    }
                    if (traceStr && traceStr.length) {
                        traceStr = ` - ${traceStr}`;
                    }
                }
                const suffix = info.suffix
                    ? ` ${info.suffix}`
                    : "";
                const serviceName = info.serviceName
                    ? chalk_1.default.bold.hex(info.serviceColor)(` [${info.serviceName}]`)
                    : "";
                const time = (0, moment_1.default)(info.timestamp).toISOString();
                const callerName = info.callerName ? ` ${info.callerName}` : "";
                const message = info.message ? ` ${info.message}` : "";
                return `${time}${serviceName} ${info.level} ${callerName}${message}${suffix}${dataStr}${traceStr}`; // eslint-disable-line max-len
            })),
        }));
    }
    if (process.env.LOG_TO_FILE) {
        driver.add(new winston.transports.File({
            filename: "error.log",
            level: "error",
        }));
        driver.add(new winston.transports.File({
            filename: "combined.log",
        }));
    }
    if (process.env.LOGSTASH_URL) {
        const urlParts = (0, utils_1.getUrlParts)(process.env.LOGSTASH_URL);
        const logstashOpts = {
            host: urlParts.hostname,
            node_name: "logstash",
            port: urlParts.port,
        };
        driver.add(new winston_logstash_transport_1.LogstashTransport(logstashOpts));
    }
    return driver;
}
exports.default = winstonDriver;
//# sourceMappingURL=winston.js.map