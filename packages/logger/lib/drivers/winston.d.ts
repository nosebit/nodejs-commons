import { ILoggerDriver } from "..";
/**
 * Interface for winston driver options.
 */
interface IWinstonDriverOpts {
    level?: string;
}
/**
 * This function retreives a global winston driver.
 *
 * @param opts - A set of config options.
 */
declare function winstonDriver(opts?: IWinstonDriverOpts): ILoggerDriver;
export { winstonDriver as default, };
