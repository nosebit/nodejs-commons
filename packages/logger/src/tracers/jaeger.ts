//#####################################################
// Imports
//#####################################################
import { initTracer, Tracer as JaegerDriver } from "jaeger-client";
import { v4 as uuid } from "uuid";

import {
  Logger,
  ITracer,
  ITracerSpan,
  ITracerSpanConfig,
} from "..";

//#####################################################
// Types
//#####################################################
interface IJaegerTracerConfig {
  host?: string;
  serviceName: string;
}

//#####################################################
// Constants
//#####################################################
// Tracer key id
const TRACE_ID = "uber-trace-id";

//#####################################################
// Main class
//#####################################################
/**
 * This class implements a jaeger tracer.
 */
class JaegerTracer implements ITracer {
  /**
   * The jaeger driver (or client).
   */
  private readonly _driver: JaegerDriver;

  /**
   * This function creates a new jaeger tracer instance.
   *
   * @param config - The config options.
   */
  constructor(config: IJaegerTracerConfig) {
    this._driver = initTracer({
      reporter: {
        agentHost: config.host || "localhost",
      },
      sampler: {
        host: config.host || "localhost",
        param: 1,
        type: "const",
      },
      serviceName: config.serviceName,
    }, {
      logger: Logger.sharedExists()
        ? Logger.shared
        : null,
    });
  }

  /**
   * This function starts a new span.
   *
   * @param operationName - Name of the operation to be traced.
   * @param config - A set of config options.
   */
  public startSpan(
    operationName: string,
    config?: ITracerSpanConfig,
  ): ITracerSpan {
    return this._driver.startSpan(operationName, config as object);
  }

  /**
   * This function build a span object from context id.
   *
   * @param id - The context id.
   */
  public getSpanFromId(id: string): ITracerSpan | object {
    const ctx = id
      ? this._driver.extract("http_headers", { [TRACE_ID]: id })
      : null;

    return ctx;
  }

  /**
   * This function extract the trace id from the span object.
   *
   * @param span - The span object.
   */
  public getTraceId(span: ITracerSpan): string {
    if (!span) {
      return uuid();
    }

    const headers = {
      [TRACE_ID]: "",
    };

    this._driver.inject(span, "http_headers", headers);

    return headers[TRACE_ID];
  }
}

//#####################################################
// Export
//#####################################################
export {
  JaegerTracer as default,
};
