//#####################################################
// Imports
//#####################################################
import { getUrlParts } from "@nosebit/utils";
import chalk from "chalk";
import moment from "moment";
import * as winston from "winston";
import { LogstashTransport } from "winston-logstash-transport";

import { ILoggerDriver } from "..";

//#####################################################
// Types
//#####################################################
/**
 * Interface for winston driver options.
 */
interface IWinstonDriverOpts {
  level?: string;
}

//#####################################################
// Local variables
//#####################################################
// The global winston driver
let driver: winston.Logger;

//#####################################################
// Main function
//#####################################################
/**
 * This function retreives a global winston driver.
 *
 * @param opts - A set of config options.
 */
function winstonDriver(
  opts: IWinstonDriverOpts = {},
): ILoggerDriver {
  if (driver) {
    return driver;
  }

  // Ignore log messages if the have { private: true }
  const ignorePrivate = winston.format((info) => {
    if (info.private) {
      return false;
    }

    return info;
  });

  const {
    combine,
    timestamp,
    printf,
    colorize,
  } = winston.format;

  // Create winston logger
  driver = winston.createLogger({
    format: winston.format.json(),

    /**
     * We going to handle level in our logger instead of in
     * winston so that we can have a winston singleton driver.
     */
    level: opts.level || "silly",
  });

  if (!process.env.LOG_SILENT) {
    driver.add(new winston.transports.Console({
      format: combine(
        colorize(),
        ignorePrivate(),
        timestamp(),
        printf((info) => {
          let dataStr = "";
          let traceStr = "";

          if (info.data) {
            try {
              dataStr = JSON.stringify(info.data);
            } catch (error) {
              dataStr = info.data;
            }

            if (dataStr && dataStr.length) {
              dataStr = ` - ${dataStr}`;
            }
          }

          if (info.trace && info.trace.stackId) {
            try {
              traceStr = JSON.stringify(info.trace);
            } catch (error) {
              traceStr = info.trace;
            }

            if (traceStr && traceStr.length) {
              traceStr = ` - ${traceStr}`;
            }
          }

          const suffix = info.suffix
            ? ` ${info.suffix}`
            : "";

          const serviceName = info.serviceName
            ? chalk.bold.hex(info.serviceColor)(` [${info.serviceName}]`)
            : "";
          const time = moment(info.timestamp).toISOString();
          const callerName = info.callerName ? ` ${info.callerName}` : "";
          const message = info.message ? ` ${info.message}` : "";

          return `${time}${serviceName} ${info.level} ${callerName}${message}${suffix}${dataStr}${traceStr}`; // eslint-disable-line max-len
        }),
      ),
    }));
  }

  if (process.env.LOG_TO_FILE) {
    driver.add(
      new winston.transports.File({
        filename: "error.log",
        level: "error",
      }),
    );

    driver.add(
      new winston.transports.File({
        filename: "combined.log",
      }),
    );
  }

  if (process.env.LOGSTASH_URL) {
    const urlParts = getUrlParts(process.env.LOGSTASH_URL);
    const logstashOpts: any = {
      host: urlParts.hostname,
      node_name: "logstash",
      port: urlParts.port,
    };

    driver.add(
      new LogstashTransport(logstashOpts),
    );
  }

  return driver as ILoggerDriver;
}

//#####################################################
// Export
//#####################################################
export {
  winstonDriver as default,
};
