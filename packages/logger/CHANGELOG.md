# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.5.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.5.3...@nosebit/logger@0.5.4) (2022-04-26)

**Note:** Version bump only for package @nosebit/logger





## [0.5.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.5.2...@nosebit/logger@0.5.3) (2022-01-07)

**Note:** Version bump only for package @nosebit/logger





## [0.5.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.5.1...@nosebit/logger@0.5.2) (2021-11-14)

**Note:** Version bump only for package @nosebit/logger





## [0.5.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.5.0...@nosebit/logger@0.5.1) (2021-10-07)

**Note:** Version bump only for package @nosebit/logger





# [0.5.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.4.1...@nosebit/logger@0.5.0) (2021-09-12)


### Features

* **logger:** log service name to console with custom color ([54fcf62](https://github.com/nosebit/nodejs-commons/commit/54fcf62f9777287aba135d66c5fde1d8a884b900))





## [0.4.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.4.0...@nosebit/logger@0.4.1) (2021-09-11)

**Note:** Version bump only for package @nosebit/logger





# [0.4.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.3.4...@nosebit/logger@0.4.0) (2021-07-08)


### Bug Fixes

* **kafka:** use kafkajs to consume messages ([8175907](https://github.com/nosebit/nodejs-commons/commit/8175907bbe8e6116dab99ee8e58c5b5ba083a466))


### BREAKING CHANGES

* **kafka:** Config options to kafka client changed.





## [0.3.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.3.3...@nosebit/logger@0.3.4) (2021-06-12)

**Note:** Version bump only for package @nosebit/logger





## [0.3.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.3.2...@nosebit/logger@0.3.3) (2021-06-09)

**Note:** Version bump only for package @nosebit/logger





## [0.3.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.3.1...@nosebit/logger@0.3.2) (2020-10-26)


### Bug Fixes

* **logger:** use correct types to Logger.trace method ([6faabf2](https://github.com/nosebit/nodejs-commons/commit/6faabf200b3c3e80f22bf16385f309ab2b191c2d))





## [0.3.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.3.0...@nosebit/logger@0.3.1) (2020-10-15)


### Bug Fixes

* **logger:** make lint happy ([903fdbb](https://github.com/nosebit/nodejs-commons/commit/903fdbbda6169477879b819554b126b5dd41ca6d))
* **logger:** set winston to log anything so we can use singleton winston driver ([7df4d0d](https://github.com/nosebit/nodejs-commons/commit/7df4d0def6ec9f0b49652a781f2df1fb09386dda))





# [0.3.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.2.0...@nosebit/logger@0.3.0) (2020-10-15)


### Features

* **logger:** don't use NODE_ENV to control logs and use custom env vars instead ([7e1aed6](https://github.com/nosebit/nodejs-commons/commit/7e1aed6d1f4725bba39c0065f669aaea2e2c199e))





# [0.2.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.1.6...@nosebit/logger@0.2.0) (2020-10-14)


### Features

* **logger:** allow LOG_LEVEL environment variable ([36ecbfe](https://github.com/nosebit/nodejs-commons/commit/36ecbfeb9cc3a306d898be2c75db83cb4086411e))





## [0.1.6](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.1.5...@nosebit/logger@0.1.6) (2020-10-12)


### Bug Fixes

* **logger:** fix way prevent log based on level for logger instances without shared one ([358486b](https://github.com/nosebit/nodejs-commons/commit/358486b9dba8ccbd8893d76fb3c11265134c255f))





## [0.1.5](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.1.4...@nosebit/logger@0.1.5) (2020-10-12)


### Bug Fixes

* **logger:** print date in iso format and prevent null stackId log ([96a6889](https://github.com/nosebit/nodejs-commons/commit/96a688914499481504ef165fdcf51429cadc2808))





## [0.1.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.1.3...@nosebit/logger@0.1.4) (2020-10-10)

**Note:** Version bump only for package @nosebit/logger





## [0.1.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.1.2...@nosebit/logger@0.1.3) (2020-09-24)

**Note:** Version bump only for package @nosebit/logger





## [0.1.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.1.1...@nosebit/logger@0.1.2) (2020-09-12)

**Note:** Version bump only for package @nosebit/logger





## [0.1.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/logger@0.1.0...@nosebit/logger@0.1.1) (2020-05-13)

**Note:** Version bump only for package @nosebit/logger





# 0.1.0 (2020-04-19)


### Bug Fixes

* Change remaining references to nodejs-* packages. ([c6ef440](https://github.com/nosebit/nodejs-commons/commit/c6ef440d93d30af6f4c6d5c97026abe07f9f0982))


### Features

* New npm package names removing nodejs-* prefix and adding mongo and thrift mock packages. ([461df10](https://github.com/nosebit/nodejs-commons/commit/461df1099b71f838b046a60f2d232e891a65f666))
