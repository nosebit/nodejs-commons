const path = require("path");

const tsConfig = path.resolve(__dirname, "tsconfig.json");

module.exports = {
  extends: [
    "nosebit-typescript",
  ],
  parserOptions: {
    project: tsConfig,
  },
  rules: {
    "@typescript-eslint/no-redeclare": [
      "error",
    ],
    "@typescript-eslint/no-shadow": [
      "error",
      {
        builtinGlobals: true,
        hoist: "functions",
      },
    ],
    "no-redeclare": [
      "off",
    ],
    "no-shadow": [
      "off",
    ],
  },
  settings: {
    "import/external-module-folders": [
      "@nosebit",
      "node_modules",
    ],
    "import/resolver": {
      typescript: {
        project: tsConfig,
      },
    },
  },
};
