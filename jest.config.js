/* eslint-disable import/no-commonjs */

const config = {
  collectCoverage: true,
  collectCoverageFrom: [
    "src/**/*.ts",
    "!**/*.d.ts",
    "!**/*.test.ts",
  ],
  coverageDirectory: "coverage",
  forceExit: true,
  moduleDirectories: [
    "<rootDir>",
    "<rootDir>/packages",
    "node_modules",
  ],
  moduleFileExtensions: [
    "ts",
    "tsx",
    "js",
    "jsx",
    "json",
    "node",
  ],
  moduleNameMapper: {
    "^@nosebit/(.*)/lib/(.*)": "<rootDir>/packages/$1/src/$2",
    "^@nosebit/([^/]*)$": "<rootDir>/packages/$1/src",
  },
  setupFilesAfterEnv: [
    "./jest.setup.ts",
  ],
  testRegex: "(?<!\\.integ)\\.(test|spec)\\.tsx?$",
  transform: {
    "^.+\\.tsx?$": "ts-jest",
  },
};

// Setup config for local integration test.
if ((process.env.NODE_ENV || "").startsWith("test-integ")) {
  config.collectCoverage = false;
  config.testRegex = "\\.integ\\.(test|spec)\\.tsx?$";
}

module.exports = config;
